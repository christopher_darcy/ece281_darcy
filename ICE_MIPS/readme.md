# MIPS In Class Exercise 
## C3C Christopher Darcy

###Part 1

a. What does it do? Write a single sentence specifically describing its function.

The program initializes three variables t0=5, t1=20, and t2=0, then uses a loop to add t2 to t1 and 4 to t2; the loops ends when t2 = t1.


b. Add a comment to each line explaining its role in the program in plain English.

![Comments](images/part1_comments.PNG)
#####Figure 1: Commented version of part1, viewed in Bitbucket's built in text editor.

c. How many times does the loop run?

5 times

d. What is the final value of register $t0?
0x2d, or 45 in base 10.

e. Commit your commented version of this program.

f. Modify this program so that x (found in $t0) counts up by 2 from 0 to 10, and once $t0 reaches 10, it counts back down to 0 by 2. This counting behavior should go on forever (e.g. counting up and down and up and down and up and down and up…). Commit your modified program.



###Documentation


I used this website to understand syscall: https://courses.missouristate.edu/KenVollmar/mars/Help/SyscallHelp.html. It helped me by allowing me to have input and output.