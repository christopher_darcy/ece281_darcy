# Lab 2

## By C3C Christopher Darcy

## Table of Contents 
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
 * [Code](#code)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
 * [Pseudocode](#pseudocode)
4. [Hardware schematic](#hardware-schematic)
5. [Well-formatted code](#well-formatted-code)
6. [Debugging](#debugging)
7. [Testing methodology or results](#testing-methodology-or-results)
8. [Answers to Lab Questions](#answers-to-lab-questions)
9. [Observations and Conclusions](#observations-and-conclusions)
10. [Feedback](#feedback)
11. [Documentation](#documentation)
 
### Objectives or Purpose 
The objective of this lab is to design, write, and implement a finite state machine that will simulate the taillights of a 1965 Ford Thunderbird. There are several parts to this lab: the prelab where the design is made, the VHDL where the finite state machine (FSM) is coded, the simulation to verify the functionality of the VDHL, and lastly the actual implementation onto the Nexys2 Board. The Purpose of this lab is to learn how to create a FSM in VHDL and see how it can be done very simply using processes. Also, this lab shows us that VDHL will be efficient in creating the design, so the schematic will be different then the schematic I created in the prelab.


### Preliminary design
The Thunderbird taillights have a very unique design where the blinkers are three separate lights that flash outwards. Additionally, for extra credit, all six taillights will blink when the hazards are on.

There are several inputs for this FSM: left blinker, right blinker, and reset. The outputs are the 6 lights: LA, LB, LC, RA, RB, and RC.

![Thunderbird Taillights](images/taillights.PNG)
#####Figure 1: Flashing sequence for the Thunderbird taillights (light blue indicates that they are illuminated).

There are 8 total states: left1, left2, left3, right1, right2, right3, reset, and hazards. Below is a diagram showing all these states and the specified output.

![State Transition Diagram](images/diagram.PNG)
#####Figure 2: State transition diagram for a Moore machine that shows the states for the Thunderbird tail lights.


From the state transition diagram, I created tables for the next state and outputs.

![Next State Table](images/next_state_table.PNG)
#####Figure 3: Next State Table.


From this table I was able to determine the following next state equations.

	--------------------------------------------------------------------
	Next State Equations:

	**A*** = A'B'C'LD' + AB'D'

	**B*** = A'B'C'LRD' + B'CD' + A'BC'D'

	**C*** = A'B'C'RD' + A'BC'D' + AB'C'D'

	--------------------------------------------------------------------

![Output Table](images/output_table.PNG)
#####Figure 4: Output Table.
		
Output equations were much simpler and were again determined using the output table.

	--------------------------------------------------------------------
	Output Equations:
	**LA** = A

	**LB** = AB + AC

	**LC** = AB

	**RA** = A'C + A'B + BC

	**RB** = BC + A'B

	**RC** = BC
	--------------------------------------------------------------------
#####Code
All code for this project is included in this repository under the **code/ file**. Key points are also included in the well formatted code section of this markdown file.





#####VHDL Header:
	--------------------------------------------------------------------
	-- Name:C3C Christopher Darcy
	-- Date: 27 Feb 2017
	-- Course: ECE 281
	-- File:Lab2/readme.md
	-- HW:	Lab2
	--
	-- Purp: Simulate the taillights of a 1965 Ford Thunderbird
	-- 
	--
	-- Doc:	None.
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	------------------------------------------------------------------------- 

	
### Software flow chart or algorithms

All the material for this section was a part of the prelab. As such, the state transition diagram and next state and output equations are all listed under the Preliminary design part of the markdown file.



#### Pseudocode:

The logic for this FSM is very simple.

If the reset is on:
	go to the reset state

else if the hazards are on:
	go to the hazards state

else if the left blinker is on:
	go to the left1 blinker state

else if the right blinker is on:
	go to the right1 blinker state

for any other cases:
	go to reset case



### Hardware schematic
Before starting the lab, we were required to create a hardware schematic of our FSM. Below is prelab schematic.

![schematic](images/schematic.PNG)
#####Figure 5: Hand drawn hardware schematic for the Thunderbird tail lights.

After we coded the FSM in VHDL, we were instructed to draw a diagram of the top level architecture. First is pretty much the top view of the schematic, without drilling down all the way into the thunderbird_fsm.

![RTL Short](images/rtl_short.PNG)
#####Figure 6: A shortened picture of the RTL schematic where the internal components and signals are easily visible.

Also, I have included the following fully drilled down schematic.

![RTL Full](images/rtl_full.PNG)
#####Figure 7: A RTL schematic with all internal block contents shown.



### Well-formatted code

First, I created the thunderbird_fsm and coded the processes for the clock and next state.

![Next State Process](images/next_state.PNG)
#####Figure 8: Next State Process, the code repeats for the right and also includes states for hazards and a catch all.

![Clock Process](images/clk_process.PNG)
#####Figure 9: Clock process that drives the next state.

Next, I created the testbench, which is included in the code folder of this repository. The test cases can be viewed inside that code file. 
After the testbench, I coded the toplevel file. First thing to do in the toplevel file is add the two components: the thunderbird_fsm and clk_divider.

![Components](images/components.PNG)
#####Figure 10: The two components of the top level file.

Once the components were added, I had to write the code to connect everything, which was accomplished through port maps. 

![Port Maps](images/port_maps.PNG)
#####Figure 11: The port maps that connect the inputs and outputs of the top level and each component.



### Debugging
Overall, I did not run into a lot of trouble during this lab. The only major hang up I had was a few syntax errors, one of which is pictured below. However, all these issues were easily corrected.

![Syntax Error](images/syntax_error.PNG)
#####Figure 12: Syntax error that I had not encountered before, solution was fixing a simple typo. 

After the syntax error, I had trouble in the simulation because my output lights were undefined. This was simple a minor coding error and was again easily fixed to ensure the output process was correct.




### Testing methodology or results 
First, I had to use the testbench simulation to ensure that my FSM worked. I used test cases for each blinker, the hazards, the reset on the hazards, and the reset on a blinker. I feel that these 5 cases adequately tested each condition and special case. Below is the simulation waveform results. Notice how the the outputs for each light look like stairs, this is how we know the blinkers are correct. Also, in this simulation it appears that the lights are backwards and both flash in the same direction (up). However, this issue was fixed in the constraints file, where the led outputs were mapped to LEDs on the Nexys2 Board that corrected this.

![Waveform](images/waveform.PNG)
#####Figure 13: Simulation waveform results.

For this waveform, I start by resetting the board for the first 10 ns so that the board is in a known state. The next test starts at 10 ns and is only a left input, which starts the left lights on the next rising edge of the clock (15 ns). This test ends 3 clock cycles later at 45 ns. Following that is a test of just right input, and again the right lights begin on the next rising edge (from 55 ns to 85 ns). Last I check the hazards, where both left and right input are on, which is at 95 ns. Immediately following that, I test the reset with both left and right inputs still on, the reset works correctly and turns all the lights off regardless of the current state.


After running the waveform simulation, I was able to actually generate and program the file onto my Nexys2 Board. The proof of functionality can be seen in the video at this link: [Proof of functionality*](https://drive.google.com/file/d/0B2_imC2sGkjBNktHVW1MX2tsWWs/view?usp=sharing). A copy of the video is also provided in this repository at **Lab2/images/ece281_lab2.mp4**.

 


*Or copy/paste this URL: https://drive.google.com/file/d/0B2_imC2sGkjBNktHVW1MX2tsWWs/view?usp=sharing


### Answers to Lab Questions
2a)	Are you designing a Moore or a Mealy machine?

I am designing a Moore machine, the output is only dependent on the current states.

View the RTL schematic of the synthesized FSM and include a picture in your README.  Does it look as expected?  Do you find anything surprising?

It looks as I expected it to, its very similar to the schematic I drew. I am not surprised by how the RTL schematic looked.


### Observations and Conclusions
I was able to correctly simulate the 1965 Ford Thunderbird's taillights by relying on my past knowledge, the word document guide, and examples from previous lectures. Starting with the prelab, I created a state transition diagram and used that to create next state and output tables. From the tables, I was able to create the next state and output equations. With that design created, I could then use VHDL to code the actually FSM. I then ran it through a simulation to verify that it was functioning as designed. Finally, I was able to implement the design on the Nexys2 Board and demonstrate its actual functionality. For extra credit, I was able to add an extra state to simulate the Thunderbird's hazard lights. Ultimately, I was able to design and program all the desired functionality, perform a simulation, and lastly implement the code. 


### Feedback

Number of hours spent on Lab 2: ______8 hrs______ (no points associated with this unless you leave it blank)


Suggestions to improve Lab 2 in future years: (use blank space below)

I liked this lab a lot more then the past labs and CEs. This was due to the fact that we had created FSMs in class before using VHDL so I was not as lost as I was in the previous Labs and CEs. In the future, definitely continue to go over creating and simulating an FSM before this lab, that way we understand the machine and have example code to look back on.

### Documentation

C3C Ryer and I compared our next state and output tables and equations to verify their correctness before created the components in VHDL.