# Computer Exercise #3

## By C3C Christopher Darcy

## Table of Contents 
1. [Test Vectors](#test-vectors)
 * [Rationale for Test Cases](#rationale-for-test-cases)
2. [Simulation Waveform](#software-flow-chart-or-algorithms)
 * [Waveform Explanation](#waveform-explanation)
3. [Synthesis Results](#synthesis-results)
 * [Interpretation of Results](#interpretation-of-results)
4. [Feedback](#feedback)
5. [Documentation](#documentation)
 
### Test Vectors
Before beginning this CE, I started by the filling out the following table to better understand the functionality of the ALU, as well as create some verifiable test cases. 

![test vectors](images/test_input_vectors.PNG)
######Figure 1: Filled out table showing the test cases. The bottom 3 test cases are my own to test for special boundary cases.

#### Rationale for Test Cases
The test cases ensure that the behavioral modeling in VHDL is correct. They test boundary cases where interesting things happen and we want to ensure that the boundary cases are producing the correct output. For example, test 23 attempts to add two numbers that overflow the 32 bit system. The output is 0 for a non-zero value, demonstrating a limit that this ALU has. 

	
### Simulation Waveform
After a few minor syntax errors, I was able to get the alu.vhd and associated testbench file to produce the following waveform simulation. Below are pictures of the waveform, each picture shows a separate process

![Log](images/waveform_log.PNG)
##### Figure 2: Console output from Isim showing no failures on any of the verification tests.

![Add](images/add.PNG)
##### Figure 3: Waveform form output for ADD command. This picture also includes the layout of the inputs and outputs on the waveform.

![sub](images/sub.PNG)
##### Figure 4: Waveform output for the SUB command.

![Add](images/slt.PNG)
##### Figure 5: Waveform output for the set less than (SLT) command.

![sub](images/and.PNG)
##### Figure 6: Waveform output for the AND command.

![Add](images/or.PNG)
##### Figure 7: Waveform output for the OR command.

![sub](images/my_tests.PNG)
##### Figure 8: Waveform output for my test cases.


#### Waveform Explanation
The waveform exactly matches the predictions I made in the test vector table. Originally, I had issues with the SLT command, but it turned out that I was using the unsigned version of std_logic. So I changed that to use the signed version and the waveform passed all the tests sand matched the predictions I made. 




### Synthesis Results
After running the simulation, I looked at the synthesis report and hardware schematics to analyze my design. Below are screenshots of those items, followed by interpretation of these results. 

![Report](images/synthesis_report.PNG)
##### Figure 9: Screenshot of the Synthesis Report that shows my design uses 1 Adder/Subtractor and 1 Comparator. 

The RTL schematic is extremely large and had to be split up to show the top and bottom. Between the following two pictures are 32 of the c_y<#>_imp boxes, which I excluded for sizing purposes.

![RTL Top](images/rtl_top.PNG)
##### Figure 10: Screenshot of the top of the RTL schematic.

![RTL Bottom](images/rtl_bottom.PNG)
##### Figure 11: Screenshot of the bottom of the RTL schematic showing the Add/Sub and Comp. 

Finally, the Technology Schematic which optimizes this design in terms of the FPGA elements.

![Tech](images/tech.PNG)
##### Figure 12: Screenshot of the entire Technology schematic.

Again, this schematic is extremely large and capturing all of it in this medium is not effective. Below is a zoomed in screenshot of the tech schematic which clearly shows its differences from the RTL schematic. 

![Tech_closer](images/tech_closer.PNG)
##### Figure 13: Zoomed in picture of the Technology schematic showing its use of buffers, xors, and LUTs. 


#### Interpretation of Results
The Synthesis report says that there is only one Adder/Subtractor and one Comparator being used in this design. In the RTL schematic this is very apparent, as they clearly show those two elements at the bottom. However, the technology schematic does not really show those elements. I was also very surprised at all the other smaller gates included in these schematics. For example, the technology schematic has numerous buffers, xors, and LUTs; where are these reported? 



### Feedback

Number of hours spent on CE3: 9:00 (no points associated with this unless you leave it blank)

What did you learn?

I learned how to code from the ground up in VHDL with no given code. I still think I need a lot of work on this, but I feel a lot better with VHDL now. I also learned how to generate the schematics for these entities that we create in VHDL. 

Suggestions to improve CE3 in future years: 

More guidance. There was a big jump between this CE and the last CE, so it took a lot of time to figure out all the nuances and new things. I spent most of my time lost, trying to troubleshoot my different problems.



### Documentation
C3C Ryer and I talked about the limits on the ALU and he helped me see how I can create a test case to overflow the 32-bit system. I included this in test 23. 