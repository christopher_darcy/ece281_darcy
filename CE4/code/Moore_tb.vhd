--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : Moore_tb.vhd
--| AUTHOR(S)     : C3C Chris Darcy
--| CREATED       : 03/07/2017
--| DESCRIPTION   : Control an elevator through inputs.
--|
--| DOCUMENTATION : 
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES : MooreElevatorController_Shell.vhd
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : none
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Moore_tb IS
END Moore_tb;
 
ARCHITECTURE behavior OF Moore_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT MooreElevatorController_Shell
    PORT(
         i_clk : IN  std_logic;
         i_reset : IN  std_logic;
         i_stop : IN  std_logic;
         i_up_down : IN  std_logic;
         o_floor : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal i_clk : std_logic := '0';
   signal i_reset : std_logic := '0';
   signal i_stop : std_logic := '0';
   signal i_up_down : std_logic := '0';

 	--Outputs
   signal o_floor : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: MooreElevatorController_Shell PORT MAP (
          i_clk => i_clk,
          i_reset => i_reset,
          i_stop => i_stop,
          i_up_down => i_up_down,
          o_floor => o_floor
        );

   -- Clock process definitions
   clk_process :process
   begin
		i_clk <= '0';
		wait for clk_period/2;
		i_clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      i_reset <= '1';
			wait for 100 ns;
		--go up to floor 4, stopping for 2 clock cycles on each floor
		--floor 2
		i_up_down <= '1'; i_stop <= '0'; i_reset <= '0';
			wait for clk_period;
		i_up_down <= '1'; i_stop <= '1';
			wait for clk_period * 2;
			
		--floor 3
		i_up_down <= '1'; i_stop <= '0';
			wait for clk_period;
		i_up_down <= '1'; i_stop <= '1';
			wait for clk_period * 2;
		
		--floor 4
		i_up_down <= '1'; i_stop <= '0';
			wait for clk_period;
		i_up_down <= '1'; i_stop <= '1';
			wait for clk_period * 2;
		
		--return to first floor
		i_up_down <= '0'; i_stop <= '0';
			wait for clk_period * 2;
		

      wait for clk_period*10;

      wait;
   end process;

END;
