--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   21:06:02 04/05/2017
-- Design Name:   
-- Module Name:   C:/Users/C19Christopher.Darcy/Documents/Spring2017/ECE281/Lab3/code/REQUIRED_ELEVATOR_tb.vhd
-- Project Name:  Lab3
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: top_nexys2
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY REQUIRED_ELEVATOR_tb IS
END REQUIRED_ELEVATOR_tb;
 
ARCHITECTURE behavior OF REQUIRED_ELEVATOR_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT top_nexys2
    PORT(
         i_clk_50MHz : IN  std_logic;
         i_BTN : IN  std_logic_vector(3 downto 0);
         i_SW : IN  std_logic_vector(7 downto 0);
         o_LED : OUT  std_logic_vector(7 downto 0);
         o_AN_n : OUT  std_logic_vector(3 downto 0);
         o_SSEG_n : OUT  std_logic_vector(6 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal i_BTN : std_logic_vector(3 downto 0) := (others => '0');
   signal i_SW : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal o_LED : std_logic_vector(7 downto 0);
   signal o_AN_n : std_logic_vector(3 downto 0);
   signal o_SSEG_n : std_logic_vector(6 downto 0);
   -- No clocks detected in port list. Replace clk below with 
   -- appropriate port name 
 
   constant clk_period : time := 10 ns;
	signal clk : std_logic;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: top_nexys2 PORT MAP (
          i_clk_50MHz => clk,
          i_BTN => i_BTN,
          i_SW => i_SW,
          o_LED => o_LED,
          o_AN_n => o_AN_n,
          o_SSEG_n => o_SSEG_n
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      i_SW <= "00001111";
		i_BTN(0) <= '1';
		wait for clk_period;
		i_BTN(0) <= '0';
		
		wait for 100 ns;
      

      -- insert stimulus here 

      wait;
   end process;

END;
