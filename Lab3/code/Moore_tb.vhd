--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   21:19:58 04/06/2017
-- Design Name:   
-- Module Name:   C:/Users/C19Christopher.Darcy/Documents/Spring2017/ECE281/Lab3/code/Moore_tb.vhd
-- Project Name:  Lab3
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: MooreElevatorController_Shell
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Moore_tb IS
END Moore_tb;
 
ARCHITECTURE behavior OF Moore_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT MooreElevatorController_Shell
    PORT(
         i_clk : IN  std_logic;
         i_reset : IN  std_logic;
         i_go : IN  std_logic;
         i_starting : IN  std_logic_vector(3 downto 0);
         i_desired : IN  std_logic_vector(3 downto 0);
         o_floor_ones : OUT  std_logic_vector(3 downto 0);
         o_floor_tens : OUT  std_logic_vector(3 downto 0);
         o_left : OUT  std_logic;
         o_right : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal i_clk : std_logic := '0';
   signal i_reset : std_logic := '0';
   signal i_go : std_logic := '0';
   signal i_starting : std_logic_vector(3 downto 0) := (others => '0');
   signal i_desired : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal o_floor_ones : std_logic_vector(3 downto 0);
   signal o_floor_tens : std_logic_vector(3 downto 0);
   signal o_left : std_logic;
   signal o_right : std_logic;

   -- Clock period definitions
   constant i_clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: MooreElevatorController_Shell PORT MAP (
          i_clk => i_clk,
          i_reset => i_reset,
          i_go => i_go,
          i_starting => i_starting,
          i_desired => i_desired,
          o_floor_ones => o_floor_ones,
          o_floor_tens => o_floor_tens,
          o_left => o_left,
          o_right => o_right
        );

   -- Clock process definitions
   i_clk_process :process
   begin
		i_clk <= '0';
		wait for i_clk_period/2;
		i_clk <= '1';
		wait for i_clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		i_reset <= '1';
		wait for i_clk_period;
		
      i_starting <= "1111"; i_desired <= "0000"; i_reset <= '0'; i_go <= '1';
		wait for i_clk_period;
		i_go <= '0';
		wait for i_clk_period * 32;

      -- insert stimulus here 

      wait;
   end process;

END;
