# Computer Exercise #4

## By C3C Christopher Darcy

## Table of Contents 
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
 * [Code](#code)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
 * [Pseudocode](#pseudocode)
4. [Hardware schematic](#hardware-schematic)
5. [Well-formatted code](#well-formatted-code)
6. [Debugging](#debugging)
7. [Testing methodology or results](#testing-methodology-or-results)
8. [Answers to Lab Questions](#answers-to-lab-questions)
9. [Observations and Conclusions](#observations-and-conclusions)
10. [Feedback](#feedback)
11. [Documentation](#documentation)
 
### Objectives or Purpose 
The objective of Computer Exercise 4 is to create and simulate a synchronous sequential logic system. This exercise will not actually implement the design, that will come in a later lab. The synchronous system will be built in VHDL and will simulate an elevator controller that traverses four floors. Also, this exercise will create an elevator controller in two ways: one using a Moore machine and the other using a Mealy machine. 


### Preliminary design
The design for this synchronous system is rather simple. There are two inputs: up_down, which controls whether the elevator is going up (1) or down (0), and stop, which determines if the elevator will stop (1) at a floor or keep going (0). The output of the system is a four-bit binary number, 1-4, representing the floor the elevator is on.

Below are state transition diagrams for each of the machines.

![Moore State Diagram](images/moore_sd.PNG)
#####Figure 1: The state diagram for the Moore machine, provided in the CE guide.

![Mealy State Diagram](images/mealy_sd.PNG)

#####Code
All code for this project is included in this repository under the **code/ file**. Key points are also included in the well formatted code section of this markdown file.





#####VHDL Header:
	--------------------------------------------------------------------
	-- Name:C3C Christopher Darcy
	-- Date: 12 Mar 2017
	-- Course: ECE 281
	-- File:CE4/readme.md
	-- HW:	CE4
	--
	-- Purp: Create and simulate an elevator controller.
	-- 
	--
	-- Doc:	None.
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	------------------------------------------------------------------------- 

	
### Software flow chart or algorithms
Please see the above Preliminary Design section for the state transition diagram for each machine. Other than those, there was not any flow charts or algorithms used in this Computer Exercise.



#### Pseudocode:
None.



### Hardware schematic
These elevator controller state machines were only used in simulations so far. They were not actually implemented onto a board or anything. Thus, there was no need for a hardware schematic. 
 




### Well-formatted code
For the Moore machine, a lot of the code was given in the template. The first coding I had to do was to complete the next state logic in within the process. This was accomplished by relying on the example code and applying it to the rest of the floor states.
![Moore Next State 1](images/moore_next_state_code.PNG)	
#####Figure 3: Moore machine code for the next state from floor 2.

![Moore Next State 2](images/moore_next_state_code2.PNG)
#####Figure 4: Moore machine code for the next state from floors 3 and 4, also includes a catch for any ghost states.

The Mealy machine next state code was very similar and I completed that code by copy and pasting this code over. One difference was the state memory, I created a separate clock process to change the state.

![Mealy State Memory](images/mealy_state_memory.PNG)
#####Figure 5: Mealy machine code for the changing the state based off the clock cycle.

Also, the output logic for each machine was the exact same, which was a late change from the course director the limited the outputs from the Mealy machine. Thus, both FSMs have the output logic below.

![Output Logic](images/output_logic.PNG)
#####Figure 6: Output logic for both machines. Note: variable names differ between the machines.


### Debugging
The only real issue I ran into with this lab were a few syntax errors and some trouble with the Mealy state transition diagram. The syntax errors were easily solved as there were simple typos and didn't require much work. However, when I began drawing the state transition diagram for the Mealy machine, I was very confused by the number of states and how exactly to draw that machine. I overcame this issue by relying on the examples from Lesson 18 and the FSM for the stoplight. Using that example code, I was able to finish this Mealy machine.




### Testing methodology or results 
Overall, I completed this assignment with little difficulty. First, I completed the elevator controller using the Moore machine. Based off the requirements of this exercise, I had the simulation start with a reset, then went up to floor 4, stopping at every floor for 3 clock cycles, and lastly had it return to the first floor without stopping. Below are screen shots of the simulation demonstrating this functionality.

![Moore Start](images/moore_sim_first.PNG)
#####Figure 7: Moore machine simulation starting with reset state.

![Moore Up](images/moore_sim_up.PNG)
#####Figure 8: Moore machine going up. The floor radix is in decimal and the elevator stops are each floor for 3 clock cycles. 

![Moore Down](images/moore_sim_down.PNG)
#####Figure 9: Moore machine going down. Starting from floor 4, the elevator goes down through each floor without stopping. 

After I completed the Moore machine, I coded and ran the simulation for the Mealy machine. These two machines are very similar in code, and have the exact same functionality, so the test cases for the two are the exact same. The Mealy machine simulation is below. 

![Mealy Up](images/mealy_up.PNG)
#####Figure 10: Mealy machine, starting with a reset, going up from floor 1 to floor 4, stopping for 3 clock cycles at each floor. 

![Mealy Down](images/mealy_down.PNG)
#####Figure 11: Mealy machine going down. Starting at floor 4 and going down to floor 1 without stopping.

I ran the same simulation tests for both machines because they have the same functionality. These test cases work because they verify the functionality of all elevator. For example, starting with a reset verifies that the reset button works, and puts the FSM in a known state for the following tests. Next, going up to each floor verifies that the up-down button functions. Also, stopping for a few clock cycles on each floor verifies that the stop button functions. Then, going straight down verifies that the down part of the up-down button works, and that elevator is works without the stop. These test cases adequately verify the functionality of both elevator controllers.

### Answers to Lab Questions
•	What is the clock frequency? What value would we set to simulate a 50MHz clock?

Clock frequency is how often the clock cycles, or goes high and low. The clock frequency is equivalent to 1 / T, where T is the clock period. To simulate a 50 MHz clock, we solve 50 MHz = 1 / T, (50 * 10^6) ^ -1 = T, so T = 20 * 10^-9, or 20 ns. 


### Observations and Conclusions
I was very successful in designing, coding, and testing both the Moore and Mealy version of the elevator controller. This CE demonstrates the similarities and differences in using both machines. One thing I was surprised about was that both the Moore and Mealy designs had the same number of states. Normally Mealy machines have less states than their Moore counterparts. However, this makes sense given that each state is a floor and the elevator may have to stop on each floor. Also, I think the elevator FSM functionality is great. However, one observation I have is that the output immediately changes with the clock, yet an actual elevator would spend time traveling between floors (or between states), so I'm not sure if an FSM is the best representation for this problem. 


### Feedback
Number of hours spent on CE 4: ____7 hrs____ (no points associated with this unless you leave it blank)

Suggestions to improve CE 4 in future years: (use blank space below)

I thought this was the best CE to date. I really liked the example code that was provided in from class as well as the outline that was given. The combination of those two resources enabled me to actually understand what was happening and replicate in these machines.


### Documentation
None.