--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : thunderbird_fsm.vhd
--| AUTHOR(S)     : C3C Chris Darcy
--| CREATED       : 03/01/2017
--| DESCRIPTION   : Simulate the taillights of a 1965 Ford Thunderbird.
--|
--| DOCUMENTATION : 
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES : none.
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : LIST ANY DEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library unisim;
  use UNISIM.Vcomponents.ALL;

-- entity 
entity thunderbird_fsm is 
  port(
			i_clk, i_reset : in std_logic;
			i_left, i_right : in std_logic;
			o_lights_L : out std_logic_vector(2 downto 0);
			o_lights_R : out std_logic_vector(2 downto 0)
  );
end thunderbird_fsm;

architecture thunderbird_fsm_arch of thunderbird_fsm is 
	-- include components declarations and signals
	
	-- createsthe state signals 
	type sm_thunderbird is (s_reset, s_left1, s_left2, s_left3, s_right1, s_right2, s_right3, s_hazards);
	signal next_state, current_state : sm_thunderbird;
  
begin

	
	-- PROCESSES ----------------------------------------
	
	-- Provide a comment that describes each process
	-- block them off like the modules above and separate with SPACE
	-- Note, the example below is a local oscillator address counter 
	--	not related to other code in this file
	
	
	-- next state process, contains logic of the next state equations
	next_state_process : process (current_state, i_left, i_right)
	begin
		case current_state is
			
			-- reset case, next state depends on the inputs
			when s_reset => 
				if (i_left ='1' and i_right = '1') then
					next_state <= s_hazards;
				elsif i_left = '1' then
					next_state <= s_left1;
				elsif i_right = '1' then
					next_state <= s_right1;
				else
					next_state <= current_state;
				end if;
				
			-- left and right always proceed to the next one and then back to reset
			-- regardless of inputs
			when s_left1 =>
				next_state <= s_left2;
			when s_left2 => 
				next_state <= s_left3;
			when s_left3 =>
				next_state <= s_reset;
				
			when s_right1 =>
				next_state <= s_right2;
			when s_right2 => 
				next_state <= s_right3;
			when s_right3 =>
				next_state <= s_reset;
			
			when s_hazards =>
				next_state <= s_reset;
			-- catch any other strange/ghost states and return to reset state
			when others =>
				next_state <= s_reset;
			
		end case;
	end process;
		
	
	
	-- clk process, keeps track of the clk
	clk_process : process(i_clk, i_reset)
	begin
		if i_reset = '1' then
			current_state <= s_reset;
		-- next state occurs on rising edge of clk
		elsif (rising_edge(i_clk)) then
			current_state <= next_state;
		end if;
	end process;
	
	
	-- output process gives the proper outputs 
	output_process : process(current_state)
	begin
		case current_state is
		
		-- 1 indicates that light is on, 0 for off
			when s_hazards =>
				o_lights_L <= "111";
				o_lights_R <= "111";
				
			when s_left1 =>
				o_lights_L <= "001";
				o_lights_R <= "000";
			when s_left2 =>
				o_lights_L <= "011";
				o_lights_R <= "000";
			when s_left3 =>
				o_lights_L <= "111";
				o_lights_R <= "000";
				
			when s_right1 =>
				o_lights_L <= "000";
				o_lights_R <= "001";
			when s_right2 =>
				o_lights_L <= "000";
				o_lights_R <= "011";
			when s_right3 =>
				o_lights_L <= "000";
				o_lights_R <= "111";
			
			when others =>
				o_lights_L <= "000";
				o_lights_R <= "000";
		end case;
	end process;
				
	
end thunderbird_fsm_arch;
