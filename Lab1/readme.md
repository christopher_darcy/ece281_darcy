# Lab 1

## By C3C Christopher Darcy

## Table of Contents 
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
 * [Code](#code)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
 * [Pseudocode](#pseudocode)
4. [Hardware schematic](#hardware-schematic)
5. [Well-formatted code](#well-formatted-code)
6. [Debugging](#debugging)
7. [Testing methodology or results](#testing-methodology-or-results)
8. [Answers to Lab Questions](#answers-to-lab-questions)
9. [Observations and Conclusions](#observations-and-conclusions)
10. [Feedback](#feedback)
11. [Documentation](#documentation)
 
### Objectives or Purpose 
The objective of Lab 1 is to design, test, and implement a seven-segment display decoder on the Nexys2 Board. Input will be a 4-bit value from switches, there will be a select of one or more using buttons, and the output will be a seven-segment display of the correct hex digit. This decoder will take the 4-bit binary input and produce 7-bits to indicate whether each segment is on ('0') or off ('1'). 

The purpose of this lab is to become more familiar with the design process for digital design projects. This includes: creating the design in software, testing the design using software, and implementing the design onto the actual Nexys2 Board. This lab also serves the purpose of becoming familiar with how decoders work and how to create and use one. Lastly, this project will enable further experience with VHDL in the Xilinx ISE, creating a lab write-up in Markdown, and using Git to publish the final project. 


### Preliminary design
The design is really quite simply: four binary inputs (consequentially the number of bits needed to represent a hex value) will be decoding into 7-bits which represent the seven segments of the display.

![Decoder and Seven-Segment Display](images/decoder.PNG)
#####Figure 1: This depcits the decoder taking a 4-bit input and will output a 7-bit value. These 7 bits correspond to the seven-segment display (a-g), which will be turned on or off accordingly to display the correct value.

The first step in creating the design is filling out the truth table. This will allow us to map each possible input to an output. This design has an active low, so the segment is only illuminated when the output is '0'. Below is the Truth Table for this design.

![Truth Table](images/truth_table.PNG)
#####Figure 2: Truth Table mapping all possible inputs to output values for Sa-Sg.

For each output, if you had to implement the full SOP or POS equations in hardware, which one would you choose and why?

I would choose to use the Sum of Products, because the SOP essentially is a chain of **OR** statements that combine all the maxterms. Since these outputs have few maxterms, because its active low and there are few instances of turning the segments off, it is much simpler to use the Sum of Products equation in the hardware. 

After creating the Truth table, K-Maps can be used to simplify the output equations. K-Maps work by creating a table (pictured below), that allows an easy, visual way to simplify logic equations. It works by circling prime implicants, according to some rules, that then become the parts of the equations.

![Output Sa](images/output_sa.PNG)
#####Figure 3: Above is the K-Map for the output Sa with the prime implicants circled in red. This K-map can be used to get the logic equation: Sa = D3'D2'D1'D0 + D3D2'D1D0 + D2D1'D0' + D3D2D1'

The following lines are the simplified equations for each output; all ascertained using K-Maps. 

	Sa = D3'D2'D1'D0 + D3D2'D1D0 + D2D1'D0' + D3D2D1'
	Sb = D3'D2D1'D0 + D3D2D0' + D2D1D0' + D3D1D0
	Sc = D3'D2'D1D0' + D3D2D0' + D3D2D1
	Sd = D3'D2D1'D0' + D2'D1'D0 + D2D1D0 + D3D2'D1D0'
	Se = D3'D0 + D3'D2D1' + D2'D1'D0
	Sf = D3D2D1' + D3'D2'D0 + D3'D2'D1 + D3'D1D0
	Sg = D3'D2'D1' + D3'D2D1D0

#####Code
All code for this project is included in this repository under the **code/ file**. Key points are also included in the well formatted code section of this markdown file.





#####VHDL Header:
	--------------------------------------------------------------------
	-- Name:C3C Christopher Darcy
	-- Date: 27 Jan 2017
	-- Course: ECE 281
	-- File:Lab1/readme.md
	-- HW:	Lab1
	--
	-- Purp: Create, test, and implement a design that illuminates a hex value from a 
	-- binary input.
	-- 
	--
	-- Doc:	None.
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	------------------------------------------------------------------------- 

	
### Software flow chart or algorithms
The only real algorithms are the logic equations pictured below. 



#### Pseudocode:
The object of the code is to create a circuit that implements the following equations to turn on a seven segment display. The main logic of the program will follow these equations.

	Sa = D3'D2'D1'D0 + D3D2'D1D0 + D2D1'D0' + D3D2D1'
	Sb = D3'D2D1'D0 + D3D2D0' + D2D1D0' + D3D1D0
	Sc = D3'D2'D1D0' + D3D2D0' + D3D2D1
	Sd = D3'D2D1'D0' + D2'D1'D0 + D2D1D0 + D3D2'D1D0'
	Se = D3'D0 + D3'D2D1' + D2'D1'D0
	Sf = D3D2D1' + D3'D2'D0 + D3'D2'D1 + D3'D1D0
	Sg = D3'D2'D1' + D3'D2D1D0




### Hardware schematic

We coded this logic circuit using VHDL, thus no schematic was actually created. 




### Well-formatted code
A lot of the code in this Lab was repetitive from CE 1 and 2. An important section of the code is to declare all the signals. These have to follow a specific naming convention, where prefixes **i** and **o** stand for input and output signals respectively. Prefix **c** stands for the internal combination signals. 

![Declarations](images/signals.PNG)
#####Figure 4: Above shows the declarations of all the signals, or wires. This code is included in pretty much all VHDL files in this lab.

The first new code in Lab 1 was to actually model the behavior of the logic. Essentially, each output signal has to be coded for when it should be high and when it should be low. 

![Behavioral Logic](images/behav_logic.PNG)
#####Figure 5: Shows two different models for behavior. Signals A and B use gates to directly implement the logic, while all the others implement a sort of lookup table.

Lastly, I had to actually connect all the signals with their respective ports and inputs/outputs. The best way to think about this is as actually wiring up the logic. 

![Connections](images/signal_connection.PNG)
#####Figure 6: Connecting signals to their ports. Note how hardware "goes into" the signal and on the other end the signal "goes into" the output hardware.




### Debugging
The beginning of this project was relatively easy; whenever I got stuck I simply referred to CE2 and was able to proceed. My first error came when I tried to run the Behavioral  Check Syntax, which failed because of a syntax error.

![Syntax Error](images/errors_sim.PNG)
#####Figure 4: The console output resulting from running Behavioral Check Syntax.

There was an easy solution to this error, as the error pointed directly to the line with a syntax error. It was a simple typo that coded a "-" instead of a "_".

![Syntax Error Cause](images/sim_error_cause.PNG)
#####Figure 5: Shows the typo that caused the syntax error towards the end of the third line of this code.

This was an easy fix, which allowed me to finally run the testbench simulation, and ultimately complete the project. 

My progress was slowed again when I needed to code the top level file. It felt as if all the structure that I was relying to code the other VHDL files was dropped and I had to sink or swim. I asked a few questions in class to clarify what certain comments were asking for in the top level file. I was able to solve this by using code from the testbench and trial and error method for syntax on other parts. Looking back, it was pretty intuitive code and I think I just got stuck up on the introduction of a new file without any structure like the previous files. 




### Testing methodology or results 
I began by filling out the truth table and creating all of the K-Maps for each output. This really enabled me to understand the problem and what exactly needed to be done. From there, I used my experience with Computer Exercise 2, and hints from the instructions, to code the sevenSegDecoder in VHDL. Once I had included and mapped all the components to their signals and behaviorally modeled the digital architecture, I was able to run the testbench simulation.

![Simulation results](images/sim_results.PNG) 
#####Figure 6: The simulation output, with grouped inputs/outputs and hexadecimal values in the radix. The hex radixes can be compared with the hex values in the truth table to verify correctness. 

After running the simulation, it was time to implement the code onto the Nexys2 Board. I used the same process as CE 1 and 2 to generate a .bit file and program it to the FPGA. I was able to demonstrate the functionality during class on Lesson 9. A video of the functionality check can be found at the following link: [https://drive.google.com/file/d/0B2_imC2sGkjBVmpqaktMdGJpT3M/view?usp=sharing](https://drive.google.com/file/d/0B2_imC2sGkjBVmpqaktMdGJpT3M/view?usp=sharing "Proof of Functionality"). This video is also included in this repository under **images/function_check.mp4**.


### Answers to Lab Questions
•	For each output, if you had to implement the full SOP or POS equations in hardware, which one would you choose and why?

I would use the Sum of Products because the SOP uses the maxterms to create the equation. In this case, there are few maxterms due to this being an active low and not having many display segments turned off. So the SOP is easier to write and understand because its simpler. 



•	Which model method do you think is easier to use for implementing your outputs?

The behavioral model that is essentially a lookup table (LUT) is much easier to use. Using the model with the gates requires a lot of code and parenthesis which can cause typos and errors. The LUT is easier to read and with easy access to the truth table, this is definitely the easier implementation for the outputs. 


•	Compare the entity interface to the provided constraints (UCF) file.  Do you notice any similarities?

The port entities in the top level file are the same as in the constraints file. The constraints file assigns actual hardware on the FPGA to these ports (i.e.  *NET "SW0" LOC = "G18"* assigns the input switch SW0 to location G18 on the board). The top level file declares these entities so that we can implement the logic with them.

•	When connecting a button to the 7SD enable pin, why is a NOT gate required?  What would happen if the button were wired directly to the 7SD enable pin?

Because the button is an active high, but we want an active low. So we fix this by **NOT**-ing one side. Otherwise, the enable pins would put all the segments on instead of off. 



### Observations and Conclusions
Overall, this lab accomplished its goal of helping me become more familiar with the design process for digital design projects. I was successfully able to implement the seven segment display decoder and test it on the FPGA. Aside from running into confusion with the top level VHDL file, I was very successful in applying my past experiences with VHDL to code the seven segment display decoder in this lab. Additionally, I am beginning to feel pretty comfortable with the process; from creating a truth table to actually programming and testing it on the FPGA.


### Feedback

Number of hours spent on Lab 1: 4:30 (no points associated with this unless you leave it blank)

Suggestions to improve Lab 1 in future years: (use blank space below)

I think there should be a little more guidance in programming the top level file. It felt like all the training wheels came of all of a sudden and I don't think any one was really ready to code at that level. 


### Documentation
C3C Stenger and I discussed how the logic worked in the truth table and how the hex values were calculated using the seven outputs as binary. 

C3C Stenger, C3C Ryer, and I compared our K-map values with each other and all agreed on the answers. 