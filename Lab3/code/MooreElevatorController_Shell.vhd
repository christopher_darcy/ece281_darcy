--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : MooreElevatorController_Shell.vhd
--| AUTHOR(S)     : C3C Chris Darcy
--| CREATED       : 03/07/2017
--| DESCRIPTION   : Control an elevator through inputs.
--|
--| DOCUMENTATION : None.
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES : None.
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : None.
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MooreElevatorController_Shell is
    Port ( i_clk : in  STD_LOGIC;
           i_reset : in  STD_LOGIC;
			  i_go : in STD_LOGIC;
           i_starting : in  STD_LOGIC_VECTOR (3 downto 0);
           i_desired : in  STD_LOGIC_VECTOR (3 downto 0);
			  o_floor_ones : out STD_LOGIC_VECTOR (3 downto 0);
           o_floor_tens : out  STD_LOGIC_VECTOR (3 downto 0);
			  o_left : out std_logic;
			  o_right : out std_logic);
end MooreElevatorController_Shell;

architecture Behavioral of MooreElevatorController_Shell is

--Below you create a new variable type! You also define what values that 
--variable type can take on. Now you can assign a signal as 
--"floor_state_type" the same way you'd assign a signal as std_logic 
type sm_floor_state_type is (floor1, floor2, floor3, floor4, floor5, floor6, floor7, floor8, floor9, floor10, floor11,
									 floor12, floor13, floor14, floor15, floor16);

--Here you create a variable "floor_state" that can take on the values
--defined above. Neat-o!
signal floor_state : sm_floor_state_type;

signal starting_floor : std_logic_vector(3 downto 0);
signal desired_floor : std_logic_vector(3 downto 0);
signal move_to : std_logic_vector(3 downto 0);
signal up : std_logic;
signal down : std_logic;

begin
---------------------------------------------
--Below you will code your next-state process
---------------------------------------------

--This line will set up a process that is sensitive to the clock
floor_state_machine: process(i_clk, i_go)
begin
	--i_clk'event and i_clk='1' is VHDL-speak for a rising edge
	--if i_clk'event and i_clk='1' then
	if i_reset='1' then
		floor_state <= floor1;
		--i_reset is active high and will return the elevator to floor1
		--Question: is i_reset synchronous or asynchronous?
	
	elsif i_go = '1' then
			starting_floor <= i_starting;
			desired_floor <= i_desired;
			move_to <= starting_floor;
	
	elsif i_clk'event and i_clk='1' then	
	
		-- wait for go to begin movin
		
		case floor_state is
				--when our current state is floor1
				when floor1 =>
					if move_to > "0000" then
						floor_state <= floor2;
						up <= '1';
						down <= '0';
					else
						move_to <= desired_floor;
					end if;
						
				--when our current state is floor2
--COMPLETE THE NEXT STATE LOGIC FOR FLOOR 2.  THE COMMENTS SHOULD HELP.				
				when floor2 => 
					--if i_up_down is set to "go up" and i_stop is set to 
					--"don't i_stop" which floor do we want to go to?
					if move_to > "0001" then 
							floor_state <= floor3;
							up <= '1';
							down <= '0';
					--if i_up_down is set to "go down" and i_stop is set to 
					--"don't i_stop" which floor do we want to go to?
					elsif move_to < "0001" then 
						floor_state <= floor1;
						up <= '0';
						down <= '1';
					--otherwise we're going to stay at floor2
					else
						move_to <= desired_floor;
						up <= '0';
						down <= '0';
					end if;
				
--COMPLETE THE NEXT STATE LOGIC ASSIGNMENTS FOR FLOORS 3 AND 4
				when floor3 =>
					if move_to > "0010" then 
						floor_state <= floor4;
						up <= '1';
						down <= '0';
					elsif move_to < "0010" then 
						floor_state <= floor2;	
						up <= '0';
						down <= '1';
					else
						move_to <= desired_floor;
						up <= '0';
						down <= '0';
					end if;
					
				when floor4 =>
					if move_to > "0011" then 
						floor_state <= floor5;
						up <= '1';
						down <= '0';
					elsif move_to < "0011" then 
						floor_state <= floor3;
						up <= '0';
						down <= '1';						
					else
						move_to <= desired_floor;
						up <= '0';
						down <= '0';
					end if;
				
				when floor5 =>
					if move_to > "0100" then 
						floor_state <= floor6;
						up <= '1';
						down <= '0';
					elsif move_to < "0100" then 
						floor_state <= floor4;
						up <= '0';
						down <= '1';
					else
						move_to <= desired_floor;
						up <= '0';
						down <= '0';
					end if;
				
				when floor6 =>
					if move_to > "0101" then 
						floor_state <= floor7;
						up <= '1';
						down <= '0';
					elsif move_to < "0101" then 
						floor_state <= floor5;
						up <= '0';
						down <= '1';
					else
						move_to <= desired_floor;
						up <= '0';
						down <= '0';
					end if;
				
				when floor7 =>
					if move_to > "0110" then 
						floor_state <= floor8;
						up <= '1';
						down <= '0';
					elsif move_to < "0110" then 
						floor_state <= floor6;
						up <= '0';
						down <= '1';
					else
						move_to <= desired_floor;
						up <= '0';
						down <= '0';
					end if;
					
					when floor8 =>
					if move_to > "0111" then 
						floor_state <= floor9;
						up <= '1';
						down <= '0';
					elsif move_to < "0111" then 
						floor_state <= floor7;
						up <= '0';
						down <= '1';
					else
						move_to <= desired_floor;
						up <= '0';
						down <= '0';
					end if;
					
					when floor9 =>
					if move_to > "1000" then 
						floor_state <= floor10;
						up <= '1';
						down <= '0';
					elsif move_to < "1000" then 
						floor_state <= floor8;
						up <= '0';
						down <= '1';						
					else
						move_to <= desired_floor;
						up <= '0';
						down <= '0';
					end if;
					
					when floor10 =>
					if move_to > "1001" then 
						floor_state <= floor11;
						up <= '1';
						down <= '0';
					elsif move_to < "1001" then 
						floor_state <= floor9;
						up <= '0';
						down <= '1';
					else
						move_to <= desired_floor;
						up <= '0';
						down <= '0';
					end if;
					
					when floor11 =>
					if move_to > "1010" then 
						floor_state <= floor12;
						up <= '1';
						down <= '0';
					elsif move_to < "1010" then 
						floor_state <= floor10;	
						up <= '0';
						down <= '1';
					else
						move_to <= desired_floor;
						up <= '0';
						down <= '0';
					end if;
					
					when floor12 =>
					if move_to > "1011" then 
						floor_state <= floor13;
						up <= '1';
						down <= '0';
					elsif move_to < "1011" then 
						floor_state <= floor11;	
						up <= '0';
						down <= '1';
					else
						move_to <= desired_floor;
						up <= '0';
						down <= '0';
					end if;
					
					when floor13 =>
					if move_to > "1100" then 
						floor_state <= floor14;
						up <= '1';
						down <= '0';
					elsif move_to < "1100" then 
						floor_state <= floor12;
						up <= '0';
						down <= '1';
					else
						move_to <= desired_floor;
						up <= '0';
						down <= '0';
					end if;
					
					when floor14 =>
					if move_to > "1101" then 
						floor_state <= floor15;
						up <= '1';
						down <= '0';
					elsif move_to < "1101" then 
						floor_state <= floor13;
						up <= '0';
						down <= '1';
					else
						move_to <= desired_floor;
						up <= '0';
						down <= '0';
					end if;
					
					when floor15 =>
					if move_to > "1110" then 
						floor_state <= floor16;
						up <= '1';
						down <= '0';
					elsif move_to < "1110" then 
						floor_state <= floor14;	
						up <= '0';
						down <= '1';
					else
						move_to <= desired_floor;
						up <= '0';
						down <= '0';
					end if;
					
					when floor16 =>
					if move_to < "1111" then 
						floor_state <= floor15;
						up <= '0';
						down <= '1';
					else
						move_to <= desired_floor;
						up <= '0';
						down <= '0';
					end if;
				
				--This line accounts for phantom states
				when others =>
					floor_state <= floor1;
			end case;
	end if;
end process;

-- Here you define your output logic. Finish the statements below
o_floor_ones <= "0001" when (floor_state = floor1 or floor_state = floor11) else
			"0010" when (floor_state = floor2 or floor_state = floor12) else
			"0011" when (floor_state = floor3 or floor_state = floor13) else
			"0100" when (floor_state = floor4 or floor_state = floor14) else
			"0101" when (floor_state = floor5 or floor_state = floor15) else
			"0110" when (floor_state = floor6 or floor_state = floor16) else
			"0111" when (floor_state = floor7) else
			"1000" when (floor_state = floor8) else
			"1001" when (floor_state = floor9) else
			"0000" when (floor_state = floor10) else
			"0001";
			
o_floor_tens <= "0001" when (floor_state = floor10 or floor_state = floor11 or floor_state = floor12
								  or floor_state = floor13 or floor_state = floor14 or floor_state = floor15 
								  or floor_state = floor16) else "0000";

o_right <= down;
o_left <= up;

end Behavioral;

