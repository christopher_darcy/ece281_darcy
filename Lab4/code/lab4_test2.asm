# lab4_test2.asm
# Test MIPS instructions.

#Assembly Code 				Comments			pc	               	          	
main:	ori	$t0, $0,  0x8000	# initialize $t0 = 0x8000	00
	addi	$t1, $0,  -32768	# initialize $t1 = 0xFFFF8000	04
	ori	$t2, $t0, 0x8001	# initialize $t2 = 0x8001	08
	beq	$t0, $t1, there		# shouldn't be taken		0C
	slt	$t3, $t1, $t0		# $t3 = -32768 < 0x8000 = 1	10
	bne	$t3, $0,  here		# should be taken		14
	j	there			# skipped over			18
here:	sub	$t2, $t2, $t0		# $t2 = 0x8001 - 0x8000 = 1	1C
	ori	$t0, $t0, 0xFF		# $t0 = 0x8000 or 0xFF = 0x80FF	20
there:	add	$t3, $t3, $t2		# $t3 = 1+1 = 2			24
	sub	$t0, $t2, $t0		# $t0 = 1-0x80FF = 0xFFFF7F02	28
	sw	$t3, 82($t0)		# [0xFFFF7F02 + 0x52] = [$t3]	2C
