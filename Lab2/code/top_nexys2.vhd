--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : top_nexys2.vhd
--| AUTHOR(S)     : C3C Chris Darcy
--| CREATED       : 03/01/2017
--| DESCRIPTION   : This file implements the top level module for a NEXYS2 in 
--|					order to drive a Thunderbird taillight controller.
--|
--|					Inputs:  sw(3:0)  --> clk reset, FSM reset, left, and right turn signals
--|							 clk 	  --> 50 MHz clock from FPGA
--|					Output:  led(5:0) --> left and right turn signal lights (three per side)
--|
--| DOCUMENTATION : None
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES : clock_divider.vhd, thunderbird_fsm.vhd
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : thunderbird_fsm.vhd, clock_divider.vhd
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  
entity top_nexys2 is
	port(
		i_clk_50MHz : in std_logic; -- native 50MHz FPGA clock
		i_sw  		: in std_logic_vector(3 downto 0); -- clk_reset, fsm_reset, 
													   -- left, right
		o_led 		: out std_logic_vector(5 downto 0) -- taillights 
													   -- (LC, LB, LA, RA, RB, RC)
	);
end top_nexys2;

architecture top_nexys2_arch of top_nexys2 is 
	
  -- declare components
  component thunderbird_fsm is
	port(
		i_clk, i_reset : in std_logic;
		i_left, i_right : in std_logic;
		o_lights_L : out std_logic_vector(2 downto 0);
		o_lights_R : out std_logic_vector(2 downto 0)
	);
	end component;
	
	component clock_divider is
	 port(
		i_clk    : in std_logic;
		i_reset  : in std_logic;
		o_clk    : out std_logic
	 );
	 end component;


  -- create wires to connect components
	signal c_clk_divider : std_logic;  -- carries clk signal from clk_divider to thunderbird_fsm
	signal c_left_out : std_logic_vector(2 downto 0);  -- carries left blinker output
	signal c_right_out : std_logic_vector(2 downto 0);  -- carries right blinker output
  

  
begin
	-- PORT MAPS / COMPONENT INSTANTIATION --------------
	
	-- wires the components to each other and top level inputs/outputs
	clk_divider_inst : clock_divider port map(
		i_clk => i_clk_50MHz,
		i_reset => i_sw(3),
		o_clk => c_clk_divider
	);
	
	tbird_inst : thunderbird_fsm port map(
		i_clk => c_clk_divider,
		i_reset => i_sw(2),
		i_left => i_sw(1),
		i_right => i_sw(0),
		o_lights_L => c_left_out,
		o_lights_R => c_right_out
	);
	
	
	
	-- CONCURRENT STATEMENTS ----------------------------
	-- concatenate the left and right blinker outputs
	o_led <= c_left_out & c_right_out;
	

	
end top_nexys2_arch;
