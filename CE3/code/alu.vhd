--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : alu.vhd
--| AUTHOR(S)     : C3C Christopher Darcy
--| CREATED       : 02/12/2017
--| DESCRIPTION   : This file simply provides a template for all VHDL assignments
--| 				- Be sure to include your Documentation Statement below!
--|
--| DOCUMENTATION : 
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : LIST ANY DEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use ieee.std_logic_signed.all;

library unisim;
  use UNISIM.Vcomponents.ALL;

-- entity name should match filename  
entity alu is 
	port(
		a, b : in std_logic_vector(31 downto 0);
		f    : in std_logic_vector(2 downto 0);
		y    : out std_logic_vector(31 downto 0);
		zero : out std_logic
	);

end alu;

architecture alu_arch of alu is 
	-- include components declarations and signals
	
	-- intermediate signals with initial value
	-- typically you would use names that relate to signal (e.g. c_mux_2)
	signal c_y : std_logic_vector(31 downto 0);

  
begin
	-- PORT MAPS ----------------------------------------

	-- map ports for any component instantiations (port mapping is like wiring hardware)
	-- concurrent signal assignment
	y <= c_y;

	-- CONCURRENT STATEMENTS "MODULES" ------------------

	-- Provide a comment that describes each "module" as appropriate
	-- think of "modules" in this sense as groups of related statements
		

	
	-- PROCESSES ----------------------------------------
	
	c_y <= (a and b) when f = "000" else
			 (a or b) when f = "001" else
			 (a + b) when f = "010" else
			 (a and not b ) when f = "100" else
			 (a or not b ) when f = "101" else
			 (a - b ) when f = "110" else
			 x"00000001" when ((f = "111") and (a < b)) else
			 x"00000000";
						
	zero <= '1' when (c_y = x"00000000") else '0';
	
	-- Provide a comment that describes each process
	-- block them off like the modules above and separate with SPACE
	-- Note, the example below is a local oscillator address counter 
	--	not related to other code in this file
	

	-----------------------------------------------------	

	-- Another process module ---------------------------
	-- . . .
	-----------------------------------------------------
	
end alu_arch;
