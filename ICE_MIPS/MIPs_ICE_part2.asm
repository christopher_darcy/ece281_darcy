#-------------------------------------------------------------------------------
# MIPS ICE - Part 2
# C3C Christopher Darcy, USAF / 10 Apr 2017 (Start Date) / 10 Apr 2017 (Completion Date)
#
# Purpose:  This file implements a program that divides a user input 
#	    by 8 using only bit shift operations.
# 
# Documentation: I used this website to understand syscall: https://courses.missouristate.edu/KenVollmar/mars/Help/SyscallHelp.html
#		It helped me by allowing me to have input and output.
#-------------------------------------------------------------------------------

.data
load_string: .asciiz "Enter a number > "
print_string: .asciiz "\nNew value is: "


.text

# print message
	li $v0, 4
	la $a0, load_string
	syscall


# load int
	li $v0, 5
	syscall
	move $t0, $v0

# divide by 8
	srl $t1, $t0, 3 # divides by eight by shifting right 3 bits

# print new value
	li $v0, 4
	la $a0, print_string
	syscall

	li $v0, 1
	la $a0, ($t1)
	syscall

end:
	j end
