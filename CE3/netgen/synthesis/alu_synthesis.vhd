--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: alu_synthesis.vhd
-- /___/   /\     Timestamp: Tue Feb 14 16:07:51 2017
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm alu -w -dir netgen/synthesis -ofmt vhdl -sim alu.ngc alu_synthesis.vhd 
-- Device	: xc3s500e-4-fg320
-- Input file	: alu.ngc
-- Output file	: C:\Users\C19Christopher.Darcy\Documents\Spring2017\ECE281\CE3\netgen\synthesis\alu_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: alu
-- Xilinx	: C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity alu is
  port (
    zero : out STD_LOGIC; 
    y : out STD_LOGIC_VECTOR ( 31 downto 0 ); 
    a : in STD_LOGIC_VECTOR ( 31 downto 0 ); 
    b : in STD_LOGIC_VECTOR ( 31 downto 0 ); 
    f : in STD_LOGIC_VECTOR ( 2 downto 0 ) 
  );
end alu;

architecture Structure of alu is
  signal N0 : STD_LOGIC; 
  signal N1 : STD_LOGIC; 
  signal a_0_IBUF_161 : STD_LOGIC; 
  signal a_10_IBUF_162 : STD_LOGIC; 
  signal a_11_IBUF_163 : STD_LOGIC; 
  signal a_12_IBUF_164 : STD_LOGIC; 
  signal a_13_IBUF_165 : STD_LOGIC; 
  signal a_14_IBUF_166 : STD_LOGIC; 
  signal a_15_IBUF_167 : STD_LOGIC; 
  signal a_16_IBUF_168 : STD_LOGIC; 
  signal a_17_IBUF_169 : STD_LOGIC; 
  signal a_18_IBUF_170 : STD_LOGIC; 
  signal a_19_IBUF_171 : STD_LOGIC; 
  signal a_1_IBUF_172 : STD_LOGIC; 
  signal a_20_IBUF_173 : STD_LOGIC; 
  signal a_21_IBUF_174 : STD_LOGIC; 
  signal a_22_IBUF_175 : STD_LOGIC; 
  signal a_23_IBUF_176 : STD_LOGIC; 
  signal a_24_IBUF_177 : STD_LOGIC; 
  signal a_25_IBUF_178 : STD_LOGIC; 
  signal a_26_IBUF_179 : STD_LOGIC; 
  signal a_27_IBUF_180 : STD_LOGIC; 
  signal a_28_IBUF_181 : STD_LOGIC; 
  signal a_29_IBUF_182 : STD_LOGIC; 
  signal a_2_IBUF_183 : STD_LOGIC; 
  signal a_30_IBUF_184 : STD_LOGIC; 
  signal a_31_IBUF_185 : STD_LOGIC; 
  signal a_3_IBUF_186 : STD_LOGIC; 
  signal a_4_IBUF_187 : STD_LOGIC; 
  signal a_5_IBUF_188 : STD_LOGIC; 
  signal a_6_IBUF_189 : STD_LOGIC; 
  signal a_7_IBUF_190 : STD_LOGIC; 
  signal a_8_IBUF_191 : STD_LOGIC; 
  signal a_9_IBUF_192 : STD_LOGIC; 
  signal b_0_IBUF_225 : STD_LOGIC; 
  signal b_10_IBUF_226 : STD_LOGIC; 
  signal b_11_IBUF_227 : STD_LOGIC; 
  signal b_12_IBUF_228 : STD_LOGIC; 
  signal b_13_IBUF_229 : STD_LOGIC; 
  signal b_14_IBUF_230 : STD_LOGIC; 
  signal b_15_IBUF_231 : STD_LOGIC; 
  signal b_16_IBUF_232 : STD_LOGIC; 
  signal b_17_IBUF_233 : STD_LOGIC; 
  signal b_18_IBUF_234 : STD_LOGIC; 
  signal b_19_IBUF_235 : STD_LOGIC; 
  signal b_1_IBUF_236 : STD_LOGIC; 
  signal b_20_IBUF_237 : STD_LOGIC; 
  signal b_21_IBUF_238 : STD_LOGIC; 
  signal b_22_IBUF_239 : STD_LOGIC; 
  signal b_23_IBUF_240 : STD_LOGIC; 
  signal b_24_IBUF_241 : STD_LOGIC; 
  signal b_25_IBUF_242 : STD_LOGIC; 
  signal b_26_IBUF_243 : STD_LOGIC; 
  signal b_27_IBUF_244 : STD_LOGIC; 
  signal b_28_IBUF_245 : STD_LOGIC; 
  signal b_29_IBUF_246 : STD_LOGIC; 
  signal b_2_IBUF_247 : STD_LOGIC; 
  signal b_30_IBUF_248 : STD_LOGIC; 
  signal b_31_IBUF_249 : STD_LOGIC; 
  signal b_3_IBUF_250 : STD_LOGIC; 
  signal b_4_IBUF_251 : STD_LOGIC; 
  signal b_5_IBUF_252 : STD_LOGIC; 
  signal b_6_IBUF_253 : STD_LOGIC; 
  signal b_7_IBUF_254 : STD_LOGIC; 
  signal b_8_IBUF_255 : STD_LOGIC; 
  signal b_9_IBUF_256 : STD_LOGIC; 
  signal c_y_0_1_258 : STD_LOGIC; 
  signal c_y_10_1_260 : STD_LOGIC; 
  signal c_y_11_1_262 : STD_LOGIC; 
  signal c_y_12_1_264 : STD_LOGIC; 
  signal c_y_13_1_266 : STD_LOGIC; 
  signal c_y_14_1_268 : STD_LOGIC; 
  signal c_y_15_1_270 : STD_LOGIC; 
  signal c_y_16_1_272 : STD_LOGIC; 
  signal c_y_17_1_274 : STD_LOGIC; 
  signal c_y_18_1_276 : STD_LOGIC; 
  signal c_y_19_1_278 : STD_LOGIC; 
  signal c_y_1_1_280 : STD_LOGIC; 
  signal c_y_20_1_282 : STD_LOGIC; 
  signal c_y_21_1_284 : STD_LOGIC; 
  signal c_y_22_1_286 : STD_LOGIC; 
  signal c_y_23_1_288 : STD_LOGIC; 
  signal c_y_24_1_290 : STD_LOGIC; 
  signal c_y_25_1_292 : STD_LOGIC; 
  signal c_y_26_1_294 : STD_LOGIC; 
  signal c_y_27_1_296 : STD_LOGIC; 
  signal c_y_28_1_298 : STD_LOGIC; 
  signal c_y_29_1_300 : STD_LOGIC; 
  signal c_y_2_1_302 : STD_LOGIC; 
  signal c_y_30_1_304 : STD_LOGIC; 
  signal c_y_31_1_306 : STD_LOGIC; 
  signal c_y_3_1_308 : STD_LOGIC; 
  signal c_y_4_1_310 : STD_LOGIC; 
  signal c_y_5_1_312 : STD_LOGIC; 
  signal c_y_6_1_314 : STD_LOGIC; 
  signal c_y_7_1_316 : STD_LOGIC; 
  signal c_y_8_1_318 : STD_LOGIC; 
  signal c_y_9_1_320 : STD_LOGIC; 
  signal c_y_mux0000 : STD_LOGIC; 
  signal f_0_IBUF_357 : STD_LOGIC; 
  signal f_1_IBUF_358 : STD_LOGIC; 
  signal f_2_IBUF_359 : STD_LOGIC; 
  signal y_0_OBUF_392 : STD_LOGIC; 
  signal y_10_OBUF_393 : STD_LOGIC; 
  signal y_11_OBUF_394 : STD_LOGIC; 
  signal y_12_OBUF_395 : STD_LOGIC; 
  signal y_13_OBUF_396 : STD_LOGIC; 
  signal y_14_OBUF_397 : STD_LOGIC; 
  signal y_15_OBUF_398 : STD_LOGIC; 
  signal y_16_OBUF_399 : STD_LOGIC; 
  signal y_17_OBUF_400 : STD_LOGIC; 
  signal y_18_OBUF_401 : STD_LOGIC; 
  signal y_19_OBUF_402 : STD_LOGIC; 
  signal y_1_OBUF_403 : STD_LOGIC; 
  signal y_20_OBUF_404 : STD_LOGIC; 
  signal y_21_OBUF_405 : STD_LOGIC; 
  signal y_22_OBUF_406 : STD_LOGIC; 
  signal y_23_OBUF_407 : STD_LOGIC; 
  signal y_24_OBUF_408 : STD_LOGIC; 
  signal y_25_OBUF_409 : STD_LOGIC; 
  signal y_26_OBUF_410 : STD_LOGIC; 
  signal y_27_OBUF_411 : STD_LOGIC; 
  signal y_28_OBUF_412 : STD_LOGIC; 
  signal y_29_OBUF_413 : STD_LOGIC; 
  signal y_2_OBUF_414 : STD_LOGIC; 
  signal y_30_OBUF_415 : STD_LOGIC; 
  signal y_31_OBUF_416 : STD_LOGIC; 
  signal y_3_OBUF_417 : STD_LOGIC; 
  signal y_4_OBUF_418 : STD_LOGIC; 
  signal y_5_OBUF_419 : STD_LOGIC; 
  signal y_6_OBUF_420 : STD_LOGIC; 
  signal y_7_OBUF_421 : STD_LOGIC; 
  signal y_8_OBUF_422 : STD_LOGIC; 
  signal y_9_OBUF_423 : STD_LOGIC; 
  signal zero_OBUF_425 : STD_LOGIC; 
  signal Maddsub_c_y_share0000_cy : STD_LOGIC_VECTOR ( 30 downto 0 ); 
  signal Maddsub_c_y_share0000_lut : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal Mcompar_c_y_cmp_lt0000_cy : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal Mcompar_c_y_cmp_lt0000_lut : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal c_y : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal c_y_share0000 : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal zero_cmp_eq0000_wg_cy : STD_LOGIC_VECTOR ( 6 downto 0 ); 
  signal zero_cmp_eq0000_wg_lut : STD_LOGIC_VECTOR ( 7 downto 0 ); 
begin
  XST_GND : GND
    port map (
      G => N0
    );
  XST_VCC : VCC
    port map (
      P => N1
    );
  Maddsub_c_y_share0000_lut_0_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_0_IBUF_161,
      I1 => b_0_IBUF_225,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(0)
    );
  Maddsub_c_y_share0000_cy_0_Q : MUXCY
    port map (
      CI => c_y_mux0000,
      DI => a_0_IBUF_161,
      S => Maddsub_c_y_share0000_lut(0),
      O => Maddsub_c_y_share0000_cy(0)
    );
  Maddsub_c_y_share0000_xor_0_Q : XORCY
    port map (
      CI => c_y_mux0000,
      LI => Maddsub_c_y_share0000_lut(0),
      O => c_y_share0000(0)
    );
  Maddsub_c_y_share0000_lut_1_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_1_IBUF_172,
      I1 => b_1_IBUF_236,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(1)
    );
  Maddsub_c_y_share0000_cy_1_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(0),
      DI => a_1_IBUF_172,
      S => Maddsub_c_y_share0000_lut(1),
      O => Maddsub_c_y_share0000_cy(1)
    );
  Maddsub_c_y_share0000_xor_1_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(0),
      LI => Maddsub_c_y_share0000_lut(1),
      O => c_y_share0000(1)
    );
  Maddsub_c_y_share0000_lut_2_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_2_IBUF_183,
      I1 => b_2_IBUF_247,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(2)
    );
  Maddsub_c_y_share0000_cy_2_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(1),
      DI => a_2_IBUF_183,
      S => Maddsub_c_y_share0000_lut(2),
      O => Maddsub_c_y_share0000_cy(2)
    );
  Maddsub_c_y_share0000_xor_2_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(1),
      LI => Maddsub_c_y_share0000_lut(2),
      O => c_y_share0000(2)
    );
  Maddsub_c_y_share0000_lut_3_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_3_IBUF_186,
      I1 => b_3_IBUF_250,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(3)
    );
  Maddsub_c_y_share0000_cy_3_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(2),
      DI => a_3_IBUF_186,
      S => Maddsub_c_y_share0000_lut(3),
      O => Maddsub_c_y_share0000_cy(3)
    );
  Maddsub_c_y_share0000_xor_3_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(2),
      LI => Maddsub_c_y_share0000_lut(3),
      O => c_y_share0000(3)
    );
  Maddsub_c_y_share0000_lut_4_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_4_IBUF_187,
      I1 => b_4_IBUF_251,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(4)
    );
  Maddsub_c_y_share0000_cy_4_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(3),
      DI => a_4_IBUF_187,
      S => Maddsub_c_y_share0000_lut(4),
      O => Maddsub_c_y_share0000_cy(4)
    );
  Maddsub_c_y_share0000_xor_4_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(3),
      LI => Maddsub_c_y_share0000_lut(4),
      O => c_y_share0000(4)
    );
  Maddsub_c_y_share0000_lut_5_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_5_IBUF_188,
      I1 => b_5_IBUF_252,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(5)
    );
  Maddsub_c_y_share0000_cy_5_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(4),
      DI => a_5_IBUF_188,
      S => Maddsub_c_y_share0000_lut(5),
      O => Maddsub_c_y_share0000_cy(5)
    );
  Maddsub_c_y_share0000_xor_5_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(4),
      LI => Maddsub_c_y_share0000_lut(5),
      O => c_y_share0000(5)
    );
  Maddsub_c_y_share0000_lut_6_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_6_IBUF_189,
      I1 => b_6_IBUF_253,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(6)
    );
  Maddsub_c_y_share0000_cy_6_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(5),
      DI => a_6_IBUF_189,
      S => Maddsub_c_y_share0000_lut(6),
      O => Maddsub_c_y_share0000_cy(6)
    );
  Maddsub_c_y_share0000_xor_6_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(5),
      LI => Maddsub_c_y_share0000_lut(6),
      O => c_y_share0000(6)
    );
  Maddsub_c_y_share0000_lut_7_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_7_IBUF_190,
      I1 => b_7_IBUF_254,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(7)
    );
  Maddsub_c_y_share0000_cy_7_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(6),
      DI => a_7_IBUF_190,
      S => Maddsub_c_y_share0000_lut(7),
      O => Maddsub_c_y_share0000_cy(7)
    );
  Maddsub_c_y_share0000_xor_7_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(6),
      LI => Maddsub_c_y_share0000_lut(7),
      O => c_y_share0000(7)
    );
  Maddsub_c_y_share0000_lut_8_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_8_IBUF_191,
      I1 => b_8_IBUF_255,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(8)
    );
  Maddsub_c_y_share0000_cy_8_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(7),
      DI => a_8_IBUF_191,
      S => Maddsub_c_y_share0000_lut(8),
      O => Maddsub_c_y_share0000_cy(8)
    );
  Maddsub_c_y_share0000_xor_8_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(7),
      LI => Maddsub_c_y_share0000_lut(8),
      O => c_y_share0000(8)
    );
  Maddsub_c_y_share0000_lut_9_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_9_IBUF_192,
      I1 => b_9_IBUF_256,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(9)
    );
  Maddsub_c_y_share0000_cy_9_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(8),
      DI => a_9_IBUF_192,
      S => Maddsub_c_y_share0000_lut(9),
      O => Maddsub_c_y_share0000_cy(9)
    );
  Maddsub_c_y_share0000_xor_9_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(8),
      LI => Maddsub_c_y_share0000_lut(9),
      O => c_y_share0000(9)
    );
  Maddsub_c_y_share0000_lut_10_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_10_IBUF_162,
      I1 => b_10_IBUF_226,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(10)
    );
  Maddsub_c_y_share0000_cy_10_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(9),
      DI => a_10_IBUF_162,
      S => Maddsub_c_y_share0000_lut(10),
      O => Maddsub_c_y_share0000_cy(10)
    );
  Maddsub_c_y_share0000_xor_10_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(9),
      LI => Maddsub_c_y_share0000_lut(10),
      O => c_y_share0000(10)
    );
  Maddsub_c_y_share0000_lut_11_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_11_IBUF_163,
      I1 => b_11_IBUF_227,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(11)
    );
  Maddsub_c_y_share0000_cy_11_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(10),
      DI => a_11_IBUF_163,
      S => Maddsub_c_y_share0000_lut(11),
      O => Maddsub_c_y_share0000_cy(11)
    );
  Maddsub_c_y_share0000_xor_11_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(10),
      LI => Maddsub_c_y_share0000_lut(11),
      O => c_y_share0000(11)
    );
  Maddsub_c_y_share0000_lut_12_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_12_IBUF_164,
      I1 => b_12_IBUF_228,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(12)
    );
  Maddsub_c_y_share0000_cy_12_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(11),
      DI => a_12_IBUF_164,
      S => Maddsub_c_y_share0000_lut(12),
      O => Maddsub_c_y_share0000_cy(12)
    );
  Maddsub_c_y_share0000_xor_12_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(11),
      LI => Maddsub_c_y_share0000_lut(12),
      O => c_y_share0000(12)
    );
  Maddsub_c_y_share0000_lut_13_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_13_IBUF_165,
      I1 => b_13_IBUF_229,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(13)
    );
  Maddsub_c_y_share0000_cy_13_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(12),
      DI => a_13_IBUF_165,
      S => Maddsub_c_y_share0000_lut(13),
      O => Maddsub_c_y_share0000_cy(13)
    );
  Maddsub_c_y_share0000_xor_13_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(12),
      LI => Maddsub_c_y_share0000_lut(13),
      O => c_y_share0000(13)
    );
  Maddsub_c_y_share0000_lut_14_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_14_IBUF_166,
      I1 => b_14_IBUF_230,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(14)
    );
  Maddsub_c_y_share0000_cy_14_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(13),
      DI => a_14_IBUF_166,
      S => Maddsub_c_y_share0000_lut(14),
      O => Maddsub_c_y_share0000_cy(14)
    );
  Maddsub_c_y_share0000_xor_14_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(13),
      LI => Maddsub_c_y_share0000_lut(14),
      O => c_y_share0000(14)
    );
  Maddsub_c_y_share0000_lut_15_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_15_IBUF_167,
      I1 => b_15_IBUF_231,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(15)
    );
  Maddsub_c_y_share0000_cy_15_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(14),
      DI => a_15_IBUF_167,
      S => Maddsub_c_y_share0000_lut(15),
      O => Maddsub_c_y_share0000_cy(15)
    );
  Maddsub_c_y_share0000_xor_15_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(14),
      LI => Maddsub_c_y_share0000_lut(15),
      O => c_y_share0000(15)
    );
  Maddsub_c_y_share0000_lut_16_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_16_IBUF_168,
      I1 => b_16_IBUF_232,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(16)
    );
  Maddsub_c_y_share0000_cy_16_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(15),
      DI => a_16_IBUF_168,
      S => Maddsub_c_y_share0000_lut(16),
      O => Maddsub_c_y_share0000_cy(16)
    );
  Maddsub_c_y_share0000_xor_16_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(15),
      LI => Maddsub_c_y_share0000_lut(16),
      O => c_y_share0000(16)
    );
  Maddsub_c_y_share0000_lut_17_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_17_IBUF_169,
      I1 => b_17_IBUF_233,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(17)
    );
  Maddsub_c_y_share0000_cy_17_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(16),
      DI => a_17_IBUF_169,
      S => Maddsub_c_y_share0000_lut(17),
      O => Maddsub_c_y_share0000_cy(17)
    );
  Maddsub_c_y_share0000_xor_17_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(16),
      LI => Maddsub_c_y_share0000_lut(17),
      O => c_y_share0000(17)
    );
  Maddsub_c_y_share0000_lut_18_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_18_IBUF_170,
      I1 => b_18_IBUF_234,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(18)
    );
  Maddsub_c_y_share0000_cy_18_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(17),
      DI => a_18_IBUF_170,
      S => Maddsub_c_y_share0000_lut(18),
      O => Maddsub_c_y_share0000_cy(18)
    );
  Maddsub_c_y_share0000_xor_18_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(17),
      LI => Maddsub_c_y_share0000_lut(18),
      O => c_y_share0000(18)
    );
  Maddsub_c_y_share0000_lut_19_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_19_IBUF_171,
      I1 => b_19_IBUF_235,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(19)
    );
  Maddsub_c_y_share0000_cy_19_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(18),
      DI => a_19_IBUF_171,
      S => Maddsub_c_y_share0000_lut(19),
      O => Maddsub_c_y_share0000_cy(19)
    );
  Maddsub_c_y_share0000_xor_19_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(18),
      LI => Maddsub_c_y_share0000_lut(19),
      O => c_y_share0000(19)
    );
  Maddsub_c_y_share0000_lut_20_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_20_IBUF_173,
      I1 => b_20_IBUF_237,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(20)
    );
  Maddsub_c_y_share0000_cy_20_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(19),
      DI => a_20_IBUF_173,
      S => Maddsub_c_y_share0000_lut(20),
      O => Maddsub_c_y_share0000_cy(20)
    );
  Maddsub_c_y_share0000_xor_20_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(19),
      LI => Maddsub_c_y_share0000_lut(20),
      O => c_y_share0000(20)
    );
  Maddsub_c_y_share0000_lut_21_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_21_IBUF_174,
      I1 => b_21_IBUF_238,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(21)
    );
  Maddsub_c_y_share0000_cy_21_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(20),
      DI => a_21_IBUF_174,
      S => Maddsub_c_y_share0000_lut(21),
      O => Maddsub_c_y_share0000_cy(21)
    );
  Maddsub_c_y_share0000_xor_21_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(20),
      LI => Maddsub_c_y_share0000_lut(21),
      O => c_y_share0000(21)
    );
  Maddsub_c_y_share0000_lut_22_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_22_IBUF_175,
      I1 => b_22_IBUF_239,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(22)
    );
  Maddsub_c_y_share0000_cy_22_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(21),
      DI => a_22_IBUF_175,
      S => Maddsub_c_y_share0000_lut(22),
      O => Maddsub_c_y_share0000_cy(22)
    );
  Maddsub_c_y_share0000_xor_22_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(21),
      LI => Maddsub_c_y_share0000_lut(22),
      O => c_y_share0000(22)
    );
  Maddsub_c_y_share0000_lut_23_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_23_IBUF_176,
      I1 => b_23_IBUF_240,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(23)
    );
  Maddsub_c_y_share0000_cy_23_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(22),
      DI => a_23_IBUF_176,
      S => Maddsub_c_y_share0000_lut(23),
      O => Maddsub_c_y_share0000_cy(23)
    );
  Maddsub_c_y_share0000_xor_23_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(22),
      LI => Maddsub_c_y_share0000_lut(23),
      O => c_y_share0000(23)
    );
  Maddsub_c_y_share0000_lut_24_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_24_IBUF_177,
      I1 => b_24_IBUF_241,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(24)
    );
  Maddsub_c_y_share0000_cy_24_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(23),
      DI => a_24_IBUF_177,
      S => Maddsub_c_y_share0000_lut(24),
      O => Maddsub_c_y_share0000_cy(24)
    );
  Maddsub_c_y_share0000_xor_24_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(23),
      LI => Maddsub_c_y_share0000_lut(24),
      O => c_y_share0000(24)
    );
  Maddsub_c_y_share0000_lut_25_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_25_IBUF_178,
      I1 => b_25_IBUF_242,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(25)
    );
  Maddsub_c_y_share0000_cy_25_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(24),
      DI => a_25_IBUF_178,
      S => Maddsub_c_y_share0000_lut(25),
      O => Maddsub_c_y_share0000_cy(25)
    );
  Maddsub_c_y_share0000_xor_25_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(24),
      LI => Maddsub_c_y_share0000_lut(25),
      O => c_y_share0000(25)
    );
  Maddsub_c_y_share0000_lut_26_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_26_IBUF_179,
      I1 => b_26_IBUF_243,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(26)
    );
  Maddsub_c_y_share0000_cy_26_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(25),
      DI => a_26_IBUF_179,
      S => Maddsub_c_y_share0000_lut(26),
      O => Maddsub_c_y_share0000_cy(26)
    );
  Maddsub_c_y_share0000_xor_26_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(25),
      LI => Maddsub_c_y_share0000_lut(26),
      O => c_y_share0000(26)
    );
  Maddsub_c_y_share0000_lut_27_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_27_IBUF_180,
      I1 => b_27_IBUF_244,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(27)
    );
  Maddsub_c_y_share0000_cy_27_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(26),
      DI => a_27_IBUF_180,
      S => Maddsub_c_y_share0000_lut(27),
      O => Maddsub_c_y_share0000_cy(27)
    );
  Maddsub_c_y_share0000_xor_27_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(26),
      LI => Maddsub_c_y_share0000_lut(27),
      O => c_y_share0000(27)
    );
  Maddsub_c_y_share0000_lut_28_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_28_IBUF_181,
      I1 => b_28_IBUF_245,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(28)
    );
  Maddsub_c_y_share0000_cy_28_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(27),
      DI => a_28_IBUF_181,
      S => Maddsub_c_y_share0000_lut(28),
      O => Maddsub_c_y_share0000_cy(28)
    );
  Maddsub_c_y_share0000_xor_28_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(27),
      LI => Maddsub_c_y_share0000_lut(28),
      O => c_y_share0000(28)
    );
  Maddsub_c_y_share0000_lut_29_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_29_IBUF_182,
      I1 => b_29_IBUF_246,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(29)
    );
  Maddsub_c_y_share0000_cy_29_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(28),
      DI => a_29_IBUF_182,
      S => Maddsub_c_y_share0000_lut(29),
      O => Maddsub_c_y_share0000_cy(29)
    );
  Maddsub_c_y_share0000_xor_29_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(28),
      LI => Maddsub_c_y_share0000_lut(29),
      O => c_y_share0000(29)
    );
  Maddsub_c_y_share0000_lut_30_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_30_IBUF_184,
      I1 => b_30_IBUF_248,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(30)
    );
  Maddsub_c_y_share0000_cy_30_Q : MUXCY
    port map (
      CI => Maddsub_c_y_share0000_cy(29),
      DI => a_30_IBUF_184,
      S => Maddsub_c_y_share0000_lut(30),
      O => Maddsub_c_y_share0000_cy(30)
    );
  Maddsub_c_y_share0000_xor_30_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(29),
      LI => Maddsub_c_y_share0000_lut(30),
      O => c_y_share0000(30)
    );
  Maddsub_c_y_share0000_lut_31_Q : LUT3
    generic map(
      INIT => X"96"
    )
    port map (
      I0 => a_31_IBUF_185,
      I1 => b_31_IBUF_249,
      I2 => c_y_mux0000,
      O => Maddsub_c_y_share0000_lut(31)
    );
  Maddsub_c_y_share0000_xor_31_Q : XORCY
    port map (
      CI => Maddsub_c_y_share0000_cy(30),
      LI => Maddsub_c_y_share0000_lut(31),
      O => c_y_share0000(31)
    );
  Mcompar_c_y_cmp_lt0000_lut_0_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_0_IBUF_161,
      I1 => b_0_IBUF_225,
      O => Mcompar_c_y_cmp_lt0000_lut(0)
    );
  Mcompar_c_y_cmp_lt0000_cy_0_Q : MUXCY
    port map (
      CI => N1,
      DI => a_0_IBUF_161,
      S => Mcompar_c_y_cmp_lt0000_lut(0),
      O => Mcompar_c_y_cmp_lt0000_cy(0)
    );
  Mcompar_c_y_cmp_lt0000_lut_1_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_1_IBUF_172,
      I1 => b_1_IBUF_236,
      O => Mcompar_c_y_cmp_lt0000_lut(1)
    );
  Mcompar_c_y_cmp_lt0000_cy_1_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(0),
      DI => a_1_IBUF_172,
      S => Mcompar_c_y_cmp_lt0000_lut(1),
      O => Mcompar_c_y_cmp_lt0000_cy(1)
    );
  Mcompar_c_y_cmp_lt0000_lut_2_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_2_IBUF_183,
      I1 => b_2_IBUF_247,
      O => Mcompar_c_y_cmp_lt0000_lut(2)
    );
  Mcompar_c_y_cmp_lt0000_cy_2_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(1),
      DI => a_2_IBUF_183,
      S => Mcompar_c_y_cmp_lt0000_lut(2),
      O => Mcompar_c_y_cmp_lt0000_cy(2)
    );
  Mcompar_c_y_cmp_lt0000_lut_3_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_3_IBUF_186,
      I1 => b_3_IBUF_250,
      O => Mcompar_c_y_cmp_lt0000_lut(3)
    );
  Mcompar_c_y_cmp_lt0000_cy_3_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(2),
      DI => a_3_IBUF_186,
      S => Mcompar_c_y_cmp_lt0000_lut(3),
      O => Mcompar_c_y_cmp_lt0000_cy(3)
    );
  Mcompar_c_y_cmp_lt0000_lut_4_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_4_IBUF_187,
      I1 => b_4_IBUF_251,
      O => Mcompar_c_y_cmp_lt0000_lut(4)
    );
  Mcompar_c_y_cmp_lt0000_cy_4_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(3),
      DI => a_4_IBUF_187,
      S => Mcompar_c_y_cmp_lt0000_lut(4),
      O => Mcompar_c_y_cmp_lt0000_cy(4)
    );
  Mcompar_c_y_cmp_lt0000_lut_5_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_5_IBUF_188,
      I1 => b_5_IBUF_252,
      O => Mcompar_c_y_cmp_lt0000_lut(5)
    );
  Mcompar_c_y_cmp_lt0000_cy_5_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(4),
      DI => a_5_IBUF_188,
      S => Mcompar_c_y_cmp_lt0000_lut(5),
      O => Mcompar_c_y_cmp_lt0000_cy(5)
    );
  Mcompar_c_y_cmp_lt0000_lut_6_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_6_IBUF_189,
      I1 => b_6_IBUF_253,
      O => Mcompar_c_y_cmp_lt0000_lut(6)
    );
  Mcompar_c_y_cmp_lt0000_cy_6_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(5),
      DI => a_6_IBUF_189,
      S => Mcompar_c_y_cmp_lt0000_lut(6),
      O => Mcompar_c_y_cmp_lt0000_cy(6)
    );
  Mcompar_c_y_cmp_lt0000_lut_7_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_7_IBUF_190,
      I1 => b_7_IBUF_254,
      O => Mcompar_c_y_cmp_lt0000_lut(7)
    );
  Mcompar_c_y_cmp_lt0000_cy_7_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(6),
      DI => a_7_IBUF_190,
      S => Mcompar_c_y_cmp_lt0000_lut(7),
      O => Mcompar_c_y_cmp_lt0000_cy(7)
    );
  Mcompar_c_y_cmp_lt0000_lut_8_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_8_IBUF_191,
      I1 => b_8_IBUF_255,
      O => Mcompar_c_y_cmp_lt0000_lut(8)
    );
  Mcompar_c_y_cmp_lt0000_cy_8_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(7),
      DI => a_8_IBUF_191,
      S => Mcompar_c_y_cmp_lt0000_lut(8),
      O => Mcompar_c_y_cmp_lt0000_cy(8)
    );
  Mcompar_c_y_cmp_lt0000_lut_9_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_9_IBUF_192,
      I1 => b_9_IBUF_256,
      O => Mcompar_c_y_cmp_lt0000_lut(9)
    );
  Mcompar_c_y_cmp_lt0000_cy_9_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(8),
      DI => a_9_IBUF_192,
      S => Mcompar_c_y_cmp_lt0000_lut(9),
      O => Mcompar_c_y_cmp_lt0000_cy(9)
    );
  Mcompar_c_y_cmp_lt0000_lut_10_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_10_IBUF_162,
      I1 => b_10_IBUF_226,
      O => Mcompar_c_y_cmp_lt0000_lut(10)
    );
  Mcompar_c_y_cmp_lt0000_cy_10_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(9),
      DI => a_10_IBUF_162,
      S => Mcompar_c_y_cmp_lt0000_lut(10),
      O => Mcompar_c_y_cmp_lt0000_cy(10)
    );
  Mcompar_c_y_cmp_lt0000_lut_11_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_11_IBUF_163,
      I1 => b_11_IBUF_227,
      O => Mcompar_c_y_cmp_lt0000_lut(11)
    );
  Mcompar_c_y_cmp_lt0000_cy_11_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(10),
      DI => a_11_IBUF_163,
      S => Mcompar_c_y_cmp_lt0000_lut(11),
      O => Mcompar_c_y_cmp_lt0000_cy(11)
    );
  Mcompar_c_y_cmp_lt0000_lut_12_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_12_IBUF_164,
      I1 => b_12_IBUF_228,
      O => Mcompar_c_y_cmp_lt0000_lut(12)
    );
  Mcompar_c_y_cmp_lt0000_cy_12_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(11),
      DI => a_12_IBUF_164,
      S => Mcompar_c_y_cmp_lt0000_lut(12),
      O => Mcompar_c_y_cmp_lt0000_cy(12)
    );
  Mcompar_c_y_cmp_lt0000_lut_13_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_13_IBUF_165,
      I1 => b_13_IBUF_229,
      O => Mcompar_c_y_cmp_lt0000_lut(13)
    );
  Mcompar_c_y_cmp_lt0000_cy_13_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(12),
      DI => a_13_IBUF_165,
      S => Mcompar_c_y_cmp_lt0000_lut(13),
      O => Mcompar_c_y_cmp_lt0000_cy(13)
    );
  Mcompar_c_y_cmp_lt0000_lut_14_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_14_IBUF_166,
      I1 => b_14_IBUF_230,
      O => Mcompar_c_y_cmp_lt0000_lut(14)
    );
  Mcompar_c_y_cmp_lt0000_cy_14_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(13),
      DI => a_14_IBUF_166,
      S => Mcompar_c_y_cmp_lt0000_lut(14),
      O => Mcompar_c_y_cmp_lt0000_cy(14)
    );
  Mcompar_c_y_cmp_lt0000_lut_15_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_15_IBUF_167,
      I1 => b_15_IBUF_231,
      O => Mcompar_c_y_cmp_lt0000_lut(15)
    );
  Mcompar_c_y_cmp_lt0000_cy_15_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(14),
      DI => a_15_IBUF_167,
      S => Mcompar_c_y_cmp_lt0000_lut(15),
      O => Mcompar_c_y_cmp_lt0000_cy(15)
    );
  Mcompar_c_y_cmp_lt0000_lut_16_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_16_IBUF_168,
      I1 => b_16_IBUF_232,
      O => Mcompar_c_y_cmp_lt0000_lut(16)
    );
  Mcompar_c_y_cmp_lt0000_cy_16_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(15),
      DI => a_16_IBUF_168,
      S => Mcompar_c_y_cmp_lt0000_lut(16),
      O => Mcompar_c_y_cmp_lt0000_cy(16)
    );
  Mcompar_c_y_cmp_lt0000_lut_17_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_17_IBUF_169,
      I1 => b_17_IBUF_233,
      O => Mcompar_c_y_cmp_lt0000_lut(17)
    );
  Mcompar_c_y_cmp_lt0000_cy_17_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(16),
      DI => a_17_IBUF_169,
      S => Mcompar_c_y_cmp_lt0000_lut(17),
      O => Mcompar_c_y_cmp_lt0000_cy(17)
    );
  Mcompar_c_y_cmp_lt0000_lut_18_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_18_IBUF_170,
      I1 => b_18_IBUF_234,
      O => Mcompar_c_y_cmp_lt0000_lut(18)
    );
  Mcompar_c_y_cmp_lt0000_cy_18_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(17),
      DI => a_18_IBUF_170,
      S => Mcompar_c_y_cmp_lt0000_lut(18),
      O => Mcompar_c_y_cmp_lt0000_cy(18)
    );
  Mcompar_c_y_cmp_lt0000_lut_19_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_19_IBUF_171,
      I1 => b_19_IBUF_235,
      O => Mcompar_c_y_cmp_lt0000_lut(19)
    );
  Mcompar_c_y_cmp_lt0000_cy_19_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(18),
      DI => a_19_IBUF_171,
      S => Mcompar_c_y_cmp_lt0000_lut(19),
      O => Mcompar_c_y_cmp_lt0000_cy(19)
    );
  Mcompar_c_y_cmp_lt0000_lut_20_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_20_IBUF_173,
      I1 => b_20_IBUF_237,
      O => Mcompar_c_y_cmp_lt0000_lut(20)
    );
  Mcompar_c_y_cmp_lt0000_cy_20_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(19),
      DI => a_20_IBUF_173,
      S => Mcompar_c_y_cmp_lt0000_lut(20),
      O => Mcompar_c_y_cmp_lt0000_cy(20)
    );
  Mcompar_c_y_cmp_lt0000_lut_21_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_21_IBUF_174,
      I1 => b_21_IBUF_238,
      O => Mcompar_c_y_cmp_lt0000_lut(21)
    );
  Mcompar_c_y_cmp_lt0000_cy_21_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(20),
      DI => a_21_IBUF_174,
      S => Mcompar_c_y_cmp_lt0000_lut(21),
      O => Mcompar_c_y_cmp_lt0000_cy(21)
    );
  Mcompar_c_y_cmp_lt0000_lut_22_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_22_IBUF_175,
      I1 => b_22_IBUF_239,
      O => Mcompar_c_y_cmp_lt0000_lut(22)
    );
  Mcompar_c_y_cmp_lt0000_cy_22_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(21),
      DI => a_22_IBUF_175,
      S => Mcompar_c_y_cmp_lt0000_lut(22),
      O => Mcompar_c_y_cmp_lt0000_cy(22)
    );
  Mcompar_c_y_cmp_lt0000_lut_23_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_23_IBUF_176,
      I1 => b_23_IBUF_240,
      O => Mcompar_c_y_cmp_lt0000_lut(23)
    );
  Mcompar_c_y_cmp_lt0000_cy_23_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(22),
      DI => a_23_IBUF_176,
      S => Mcompar_c_y_cmp_lt0000_lut(23),
      O => Mcompar_c_y_cmp_lt0000_cy(23)
    );
  Mcompar_c_y_cmp_lt0000_lut_24_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_24_IBUF_177,
      I1 => b_24_IBUF_241,
      O => Mcompar_c_y_cmp_lt0000_lut(24)
    );
  Mcompar_c_y_cmp_lt0000_cy_24_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(23),
      DI => a_24_IBUF_177,
      S => Mcompar_c_y_cmp_lt0000_lut(24),
      O => Mcompar_c_y_cmp_lt0000_cy(24)
    );
  Mcompar_c_y_cmp_lt0000_lut_25_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_25_IBUF_178,
      I1 => b_25_IBUF_242,
      O => Mcompar_c_y_cmp_lt0000_lut(25)
    );
  Mcompar_c_y_cmp_lt0000_cy_25_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(24),
      DI => a_25_IBUF_178,
      S => Mcompar_c_y_cmp_lt0000_lut(25),
      O => Mcompar_c_y_cmp_lt0000_cy(25)
    );
  Mcompar_c_y_cmp_lt0000_lut_26_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_26_IBUF_179,
      I1 => b_26_IBUF_243,
      O => Mcompar_c_y_cmp_lt0000_lut(26)
    );
  Mcompar_c_y_cmp_lt0000_cy_26_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(25),
      DI => a_26_IBUF_179,
      S => Mcompar_c_y_cmp_lt0000_lut(26),
      O => Mcompar_c_y_cmp_lt0000_cy(26)
    );
  Mcompar_c_y_cmp_lt0000_lut_27_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_27_IBUF_180,
      I1 => b_27_IBUF_244,
      O => Mcompar_c_y_cmp_lt0000_lut(27)
    );
  Mcompar_c_y_cmp_lt0000_cy_27_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(26),
      DI => a_27_IBUF_180,
      S => Mcompar_c_y_cmp_lt0000_lut(27),
      O => Mcompar_c_y_cmp_lt0000_cy(27)
    );
  Mcompar_c_y_cmp_lt0000_lut_28_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_28_IBUF_181,
      I1 => b_28_IBUF_245,
      O => Mcompar_c_y_cmp_lt0000_lut(28)
    );
  Mcompar_c_y_cmp_lt0000_cy_28_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(27),
      DI => a_28_IBUF_181,
      S => Mcompar_c_y_cmp_lt0000_lut(28),
      O => Mcompar_c_y_cmp_lt0000_cy(28)
    );
  Mcompar_c_y_cmp_lt0000_lut_29_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_29_IBUF_182,
      I1 => b_29_IBUF_246,
      O => Mcompar_c_y_cmp_lt0000_lut(29)
    );
  Mcompar_c_y_cmp_lt0000_cy_29_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(28),
      DI => a_29_IBUF_182,
      S => Mcompar_c_y_cmp_lt0000_lut(29),
      O => Mcompar_c_y_cmp_lt0000_cy(29)
    );
  Mcompar_c_y_cmp_lt0000_lut_30_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_30_IBUF_184,
      I1 => b_30_IBUF_248,
      O => Mcompar_c_y_cmp_lt0000_lut(30)
    );
  Mcompar_c_y_cmp_lt0000_cy_30_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(29),
      DI => a_30_IBUF_184,
      S => Mcompar_c_y_cmp_lt0000_lut(30),
      O => Mcompar_c_y_cmp_lt0000_cy(30)
    );
  Mcompar_c_y_cmp_lt0000_lut_31_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => a_31_IBUF_185,
      I1 => b_31_IBUF_249,
      O => Mcompar_c_y_cmp_lt0000_lut(31)
    );
  Mcompar_c_y_cmp_lt0000_cy_31_Q : MUXCY
    port map (
      CI => Mcompar_c_y_cmp_lt0000_cy(30),
      DI => b_31_IBUF_249,
      S => Mcompar_c_y_cmp_lt0000_lut(31),
      O => Mcompar_c_y_cmp_lt0000_cy(31)
    );
  zero_cmp_eq0000_wg_lut_0_Q : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => y_24_OBUF_408,
      I1 => y_23_OBUF_407,
      I2 => y_25_OBUF_409,
      I3 => y_22_OBUF_406,
      O => zero_cmp_eq0000_wg_lut(0)
    );
  zero_cmp_eq0000_wg_cy_0_Q : MUXCY
    port map (
      CI => N1,
      DI => N0,
      S => zero_cmp_eq0000_wg_lut(0),
      O => zero_cmp_eq0000_wg_cy(0)
    );
  zero_cmp_eq0000_wg_lut_1_Q : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => y_21_OBUF_405,
      I1 => y_20_OBUF_404,
      I2 => y_26_OBUF_410,
      I3 => y_19_OBUF_402,
      O => zero_cmp_eq0000_wg_lut(1)
    );
  zero_cmp_eq0000_wg_cy_1_Q : MUXCY
    port map (
      CI => zero_cmp_eq0000_wg_cy(0),
      DI => N0,
      S => zero_cmp_eq0000_wg_lut(1),
      O => zero_cmp_eq0000_wg_cy(1)
    );
  zero_cmp_eq0000_wg_lut_2_Q : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => y_18_OBUF_401,
      I1 => y_17_OBUF_400,
      I2 => y_27_OBUF_411,
      I3 => y_16_OBUF_399,
      O => zero_cmp_eq0000_wg_lut(2)
    );
  zero_cmp_eq0000_wg_cy_2_Q : MUXCY
    port map (
      CI => zero_cmp_eq0000_wg_cy(1),
      DI => N0,
      S => zero_cmp_eq0000_wg_lut(2),
      O => zero_cmp_eq0000_wg_cy(2)
    );
  zero_cmp_eq0000_wg_lut_3_Q : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => y_15_OBUF_398,
      I1 => y_14_OBUF_397,
      I2 => y_28_OBUF_412,
      I3 => y_13_OBUF_396,
      O => zero_cmp_eq0000_wg_lut(3)
    );
  zero_cmp_eq0000_wg_cy_3_Q : MUXCY
    port map (
      CI => zero_cmp_eq0000_wg_cy(2),
      DI => N0,
      S => zero_cmp_eq0000_wg_lut(3),
      O => zero_cmp_eq0000_wg_cy(3)
    );
  zero_cmp_eq0000_wg_lut_4_Q : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => y_12_OBUF_395,
      I1 => y_11_OBUF_394,
      I2 => y_29_OBUF_413,
      I3 => y_10_OBUF_393,
      O => zero_cmp_eq0000_wg_lut(4)
    );
  zero_cmp_eq0000_wg_cy_4_Q : MUXCY
    port map (
      CI => zero_cmp_eq0000_wg_cy(3),
      DI => N0,
      S => zero_cmp_eq0000_wg_lut(4),
      O => zero_cmp_eq0000_wg_cy(4)
    );
  zero_cmp_eq0000_wg_lut_5_Q : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => y_9_OBUF_423,
      I1 => y_8_OBUF_422,
      I2 => y_30_OBUF_415,
      I3 => y_7_OBUF_421,
      O => zero_cmp_eq0000_wg_lut(5)
    );
  zero_cmp_eq0000_wg_cy_5_Q : MUXCY
    port map (
      CI => zero_cmp_eq0000_wg_cy(4),
      DI => N0,
      S => zero_cmp_eq0000_wg_lut(5),
      O => zero_cmp_eq0000_wg_cy(5)
    );
  zero_cmp_eq0000_wg_lut_6_Q : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => y_6_OBUF_420,
      I1 => y_5_OBUF_419,
      I2 => y_31_OBUF_416,
      I3 => y_4_OBUF_418,
      O => zero_cmp_eq0000_wg_lut(6)
    );
  zero_cmp_eq0000_wg_cy_6_Q : MUXCY
    port map (
      CI => zero_cmp_eq0000_wg_cy(5),
      DI => N0,
      S => zero_cmp_eq0000_wg_lut(6),
      O => zero_cmp_eq0000_wg_cy(6)
    );
  zero_cmp_eq0000_wg_lut_7_Q : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => y_3_OBUF_417,
      I1 => y_2_OBUF_414,
      I2 => y_0_OBUF_392,
      I3 => y_1_OBUF_403,
      O => zero_cmp_eq0000_wg_lut(7)
    );
  zero_cmp_eq0000_wg_cy_7_Q : MUXCY
    port map (
      CI => zero_cmp_eq0000_wg_cy(6),
      DI => N0,
      S => zero_cmp_eq0000_wg_lut(7),
      O => zero_OBUF_425
    );
  c_y_mux00002 : LUT3
    generic map(
      INIT => X"FD"
    )
    port map (
      I0 => f_1_IBUF_358,
      I1 => f_0_IBUF_357,
      I2 => f_2_IBUF_359,
      O => c_y_mux0000
    );
  c_y_0_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_0_IBUF_225,
      I2 => f_2_IBUF_359,
      I3 => a_0_IBUF_161,
      O => c_y_0_1_258
    );
  c_y_0_f5 : MUXF5
    port map (
      I0 => c_y_0_1_258,
      I1 => c_y(0),
      S => f_1_IBUF_358,
      O => y_0_OBUF_392
    );
  a_31_IBUF : IBUF
    port map (
      I => a(31),
      O => a_31_IBUF_185
    );
  a_30_IBUF : IBUF
    port map (
      I => a(30),
      O => a_30_IBUF_184
    );
  a_29_IBUF : IBUF
    port map (
      I => a(29),
      O => a_29_IBUF_182
    );
  a_28_IBUF : IBUF
    port map (
      I => a(28),
      O => a_28_IBUF_181
    );
  a_27_IBUF : IBUF
    port map (
      I => a(27),
      O => a_27_IBUF_180
    );
  a_26_IBUF : IBUF
    port map (
      I => a(26),
      O => a_26_IBUF_179
    );
  a_25_IBUF : IBUF
    port map (
      I => a(25),
      O => a_25_IBUF_178
    );
  a_24_IBUF : IBUF
    port map (
      I => a(24),
      O => a_24_IBUF_177
    );
  a_23_IBUF : IBUF
    port map (
      I => a(23),
      O => a_23_IBUF_176
    );
  a_22_IBUF : IBUF
    port map (
      I => a(22),
      O => a_22_IBUF_175
    );
  a_21_IBUF : IBUF
    port map (
      I => a(21),
      O => a_21_IBUF_174
    );
  a_20_IBUF : IBUF
    port map (
      I => a(20),
      O => a_20_IBUF_173
    );
  a_19_IBUF : IBUF
    port map (
      I => a(19),
      O => a_19_IBUF_171
    );
  a_18_IBUF : IBUF
    port map (
      I => a(18),
      O => a_18_IBUF_170
    );
  a_17_IBUF : IBUF
    port map (
      I => a(17),
      O => a_17_IBUF_169
    );
  a_16_IBUF : IBUF
    port map (
      I => a(16),
      O => a_16_IBUF_168
    );
  a_15_IBUF : IBUF
    port map (
      I => a(15),
      O => a_15_IBUF_167
    );
  a_14_IBUF : IBUF
    port map (
      I => a(14),
      O => a_14_IBUF_166
    );
  a_13_IBUF : IBUF
    port map (
      I => a(13),
      O => a_13_IBUF_165
    );
  a_12_IBUF : IBUF
    port map (
      I => a(12),
      O => a_12_IBUF_164
    );
  a_11_IBUF : IBUF
    port map (
      I => a(11),
      O => a_11_IBUF_163
    );
  a_10_IBUF : IBUF
    port map (
      I => a(10),
      O => a_10_IBUF_162
    );
  a_9_IBUF : IBUF
    port map (
      I => a(9),
      O => a_9_IBUF_192
    );
  a_8_IBUF : IBUF
    port map (
      I => a(8),
      O => a_8_IBUF_191
    );
  a_7_IBUF : IBUF
    port map (
      I => a(7),
      O => a_7_IBUF_190
    );
  a_6_IBUF : IBUF
    port map (
      I => a(6),
      O => a_6_IBUF_189
    );
  a_5_IBUF : IBUF
    port map (
      I => a(5),
      O => a_5_IBUF_188
    );
  a_4_IBUF : IBUF
    port map (
      I => a(4),
      O => a_4_IBUF_187
    );
  a_3_IBUF : IBUF
    port map (
      I => a(3),
      O => a_3_IBUF_186
    );
  a_2_IBUF : IBUF
    port map (
      I => a(2),
      O => a_2_IBUF_183
    );
  a_1_IBUF : IBUF
    port map (
      I => a(1),
      O => a_1_IBUF_172
    );
  a_0_IBUF : IBUF
    port map (
      I => a(0),
      O => a_0_IBUF_161
    );
  b_31_IBUF : IBUF
    port map (
      I => b(31),
      O => b_31_IBUF_249
    );
  b_30_IBUF : IBUF
    port map (
      I => b(30),
      O => b_30_IBUF_248
    );
  b_29_IBUF : IBUF
    port map (
      I => b(29),
      O => b_29_IBUF_246
    );
  b_28_IBUF : IBUF
    port map (
      I => b(28),
      O => b_28_IBUF_245
    );
  b_27_IBUF : IBUF
    port map (
      I => b(27),
      O => b_27_IBUF_244
    );
  b_26_IBUF : IBUF
    port map (
      I => b(26),
      O => b_26_IBUF_243
    );
  b_25_IBUF : IBUF
    port map (
      I => b(25),
      O => b_25_IBUF_242
    );
  b_24_IBUF : IBUF
    port map (
      I => b(24),
      O => b_24_IBUF_241
    );
  b_23_IBUF : IBUF
    port map (
      I => b(23),
      O => b_23_IBUF_240
    );
  b_22_IBUF : IBUF
    port map (
      I => b(22),
      O => b_22_IBUF_239
    );
  b_21_IBUF : IBUF
    port map (
      I => b(21),
      O => b_21_IBUF_238
    );
  b_20_IBUF : IBUF
    port map (
      I => b(20),
      O => b_20_IBUF_237
    );
  b_19_IBUF : IBUF
    port map (
      I => b(19),
      O => b_19_IBUF_235
    );
  b_18_IBUF : IBUF
    port map (
      I => b(18),
      O => b_18_IBUF_234
    );
  b_17_IBUF : IBUF
    port map (
      I => b(17),
      O => b_17_IBUF_233
    );
  b_16_IBUF : IBUF
    port map (
      I => b(16),
      O => b_16_IBUF_232
    );
  b_15_IBUF : IBUF
    port map (
      I => b(15),
      O => b_15_IBUF_231
    );
  b_14_IBUF : IBUF
    port map (
      I => b(14),
      O => b_14_IBUF_230
    );
  b_13_IBUF : IBUF
    port map (
      I => b(13),
      O => b_13_IBUF_229
    );
  b_12_IBUF : IBUF
    port map (
      I => b(12),
      O => b_12_IBUF_228
    );
  b_11_IBUF : IBUF
    port map (
      I => b(11),
      O => b_11_IBUF_227
    );
  b_10_IBUF : IBUF
    port map (
      I => b(10),
      O => b_10_IBUF_226
    );
  b_9_IBUF : IBUF
    port map (
      I => b(9),
      O => b_9_IBUF_256
    );
  b_8_IBUF : IBUF
    port map (
      I => b(8),
      O => b_8_IBUF_255
    );
  b_7_IBUF : IBUF
    port map (
      I => b(7),
      O => b_7_IBUF_254
    );
  b_6_IBUF : IBUF
    port map (
      I => b(6),
      O => b_6_IBUF_253
    );
  b_5_IBUF : IBUF
    port map (
      I => b(5),
      O => b_5_IBUF_252
    );
  b_4_IBUF : IBUF
    port map (
      I => b(4),
      O => b_4_IBUF_251
    );
  b_3_IBUF : IBUF
    port map (
      I => b(3),
      O => b_3_IBUF_250
    );
  b_2_IBUF : IBUF
    port map (
      I => b(2),
      O => b_2_IBUF_247
    );
  b_1_IBUF : IBUF
    port map (
      I => b(1),
      O => b_1_IBUF_236
    );
  b_0_IBUF : IBUF
    port map (
      I => b(0),
      O => b_0_IBUF_225
    );
  f_2_IBUF : IBUF
    port map (
      I => f(2),
      O => f_2_IBUF_359
    );
  f_1_IBUF : IBUF
    port map (
      I => f(1),
      O => f_1_IBUF_358
    );
  f_0_IBUF : IBUF
    port map (
      I => f(0),
      O => f_0_IBUF_357
    );
  zero_OBUF : OBUF
    port map (
      I => zero_OBUF_425,
      O => zero
    );
  y_31_OBUF : OBUF
    port map (
      I => y_31_OBUF_416,
      O => y(31)
    );
  y_30_OBUF : OBUF
    port map (
      I => y_30_OBUF_415,
      O => y(30)
    );
  y_29_OBUF : OBUF
    port map (
      I => y_29_OBUF_413,
      O => y(29)
    );
  y_28_OBUF : OBUF
    port map (
      I => y_28_OBUF_412,
      O => y(28)
    );
  y_27_OBUF : OBUF
    port map (
      I => y_27_OBUF_411,
      O => y(27)
    );
  y_26_OBUF : OBUF
    port map (
      I => y_26_OBUF_410,
      O => y(26)
    );
  y_25_OBUF : OBUF
    port map (
      I => y_25_OBUF_409,
      O => y(25)
    );
  y_24_OBUF : OBUF
    port map (
      I => y_24_OBUF_408,
      O => y(24)
    );
  y_23_OBUF : OBUF
    port map (
      I => y_23_OBUF_407,
      O => y(23)
    );
  y_22_OBUF : OBUF
    port map (
      I => y_22_OBUF_406,
      O => y(22)
    );
  y_21_OBUF : OBUF
    port map (
      I => y_21_OBUF_405,
      O => y(21)
    );
  y_20_OBUF : OBUF
    port map (
      I => y_20_OBUF_404,
      O => y(20)
    );
  y_19_OBUF : OBUF
    port map (
      I => y_19_OBUF_402,
      O => y(19)
    );
  y_18_OBUF : OBUF
    port map (
      I => y_18_OBUF_401,
      O => y(18)
    );
  y_17_OBUF : OBUF
    port map (
      I => y_17_OBUF_400,
      O => y(17)
    );
  y_16_OBUF : OBUF
    port map (
      I => y_16_OBUF_399,
      O => y(16)
    );
  y_15_OBUF : OBUF
    port map (
      I => y_15_OBUF_398,
      O => y(15)
    );
  y_14_OBUF : OBUF
    port map (
      I => y_14_OBUF_397,
      O => y(14)
    );
  y_13_OBUF : OBUF
    port map (
      I => y_13_OBUF_396,
      O => y(13)
    );
  y_12_OBUF : OBUF
    port map (
      I => y_12_OBUF_395,
      O => y(12)
    );
  y_11_OBUF : OBUF
    port map (
      I => y_11_OBUF_394,
      O => y(11)
    );
  y_10_OBUF : OBUF
    port map (
      I => y_10_OBUF_393,
      O => y(10)
    );
  y_9_OBUF : OBUF
    port map (
      I => y_9_OBUF_423,
      O => y(9)
    );
  y_8_OBUF : OBUF
    port map (
      I => y_8_OBUF_422,
      O => y(8)
    );
  y_7_OBUF : OBUF
    port map (
      I => y_7_OBUF_421,
      O => y(7)
    );
  y_6_OBUF : OBUF
    port map (
      I => y_6_OBUF_420,
      O => y(6)
    );
  y_5_OBUF : OBUF
    port map (
      I => y_5_OBUF_419,
      O => y(5)
    );
  y_4_OBUF : OBUF
    port map (
      I => y_4_OBUF_418,
      O => y(4)
    );
  y_3_OBUF : OBUF
    port map (
      I => y_3_OBUF_417,
      O => y(3)
    );
  y_2_OBUF : OBUF
    port map (
      I => y_2_OBUF_414,
      O => y(2)
    );
  y_1_OBUF : OBUF
    port map (
      I => y_1_OBUF_403,
      O => y(1)
    );
  y_0_OBUF : OBUF
    port map (
      I => y_0_OBUF_392,
      O => y(0)
    );
  c_y_0_1 : LUT4
    generic map(
      INIT => X"7520"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => Mcompar_c_y_cmp_lt0000_cy(31),
      I2 => f_2_IBUF_359,
      I3 => c_y_share0000(0),
      O => c_y(0)
    );
  c_y_1_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(1),
      I1 => f_0_IBUF_357,
      O => c_y(1)
    );
  c_y_1_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_1_IBUF_236,
      I2 => f_2_IBUF_359,
      I3 => a_1_IBUF_172,
      O => c_y_1_1_280
    );
  c_y_1_f5 : MUXF5
    port map (
      I0 => c_y_1_1_280,
      I1 => c_y(1),
      S => f_1_IBUF_358,
      O => y_1_OBUF_403
    );
  c_y_2_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(2),
      I1 => f_0_IBUF_357,
      O => c_y(2)
    );
  c_y_2_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_2_IBUF_247,
      I2 => f_2_IBUF_359,
      I3 => a_2_IBUF_183,
      O => c_y_2_1_302
    );
  c_y_2_f5 : MUXF5
    port map (
      I0 => c_y_2_1_302,
      I1 => c_y(2),
      S => f_1_IBUF_358,
      O => y_2_OBUF_414
    );
  c_y_4_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(4),
      I1 => f_0_IBUF_357,
      O => c_y(4)
    );
  c_y_4_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_4_IBUF_251,
      I2 => f_2_IBUF_359,
      I3 => a_4_IBUF_187,
      O => c_y_4_1_310
    );
  c_y_4_f5 : MUXF5
    port map (
      I0 => c_y_4_1_310,
      I1 => c_y(4),
      S => f_1_IBUF_358,
      O => y_4_OBUF_418
    );
  c_y_3_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(3),
      I1 => f_0_IBUF_357,
      O => c_y(3)
    );
  c_y_3_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_3_IBUF_250,
      I2 => f_2_IBUF_359,
      I3 => a_3_IBUF_186,
      O => c_y_3_1_308
    );
  c_y_3_f5 : MUXF5
    port map (
      I0 => c_y_3_1_308,
      I1 => c_y(3),
      S => f_1_IBUF_358,
      O => y_3_OBUF_417
    );
  c_y_5_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(5),
      I1 => f_0_IBUF_357,
      O => c_y(5)
    );
  c_y_5_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_5_IBUF_252,
      I2 => f_2_IBUF_359,
      I3 => a_5_IBUF_188,
      O => c_y_5_1_312
    );
  c_y_5_f5 : MUXF5
    port map (
      I0 => c_y_5_1_312,
      I1 => c_y(5),
      S => f_1_IBUF_358,
      O => y_5_OBUF_419
    );
  c_y_7_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(7),
      I1 => f_0_IBUF_357,
      O => c_y(7)
    );
  c_y_7_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_7_IBUF_254,
      I2 => f_2_IBUF_359,
      I3 => a_7_IBUF_190,
      O => c_y_7_1_316
    );
  c_y_7_f5 : MUXF5
    port map (
      I0 => c_y_7_1_316,
      I1 => c_y(7),
      S => f_1_IBUF_358,
      O => y_7_OBUF_421
    );
  c_y_6_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(6),
      I1 => f_0_IBUF_357,
      O => c_y(6)
    );
  c_y_6_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_6_IBUF_253,
      I2 => f_2_IBUF_359,
      I3 => a_6_IBUF_189,
      O => c_y_6_1_314
    );
  c_y_6_f5 : MUXF5
    port map (
      I0 => c_y_6_1_314,
      I1 => c_y(6),
      S => f_1_IBUF_358,
      O => y_6_OBUF_420
    );
  c_y_8_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(8),
      I1 => f_0_IBUF_357,
      O => c_y(8)
    );
  c_y_8_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_8_IBUF_255,
      I2 => f_2_IBUF_359,
      I3 => a_8_IBUF_191,
      O => c_y_8_1_318
    );
  c_y_8_f5 : MUXF5
    port map (
      I0 => c_y_8_1_318,
      I1 => c_y(8),
      S => f_1_IBUF_358,
      O => y_8_OBUF_422
    );
  c_y_10_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(10),
      I1 => f_0_IBUF_357,
      O => c_y(10)
    );
  c_y_10_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_10_IBUF_226,
      I2 => f_2_IBUF_359,
      I3 => a_10_IBUF_162,
      O => c_y_10_1_260
    );
  c_y_10_f5 : MUXF5
    port map (
      I0 => c_y_10_1_260,
      I1 => c_y(10),
      S => f_1_IBUF_358,
      O => y_10_OBUF_393
    );
  c_y_9_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(9),
      I1 => f_0_IBUF_357,
      O => c_y(9)
    );
  c_y_9_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_9_IBUF_256,
      I2 => f_2_IBUF_359,
      I3 => a_9_IBUF_192,
      O => c_y_9_1_320
    );
  c_y_9_f5 : MUXF5
    port map (
      I0 => c_y_9_1_320,
      I1 => c_y(9),
      S => f_1_IBUF_358,
      O => y_9_OBUF_423
    );
  c_y_11_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(11),
      I1 => f_0_IBUF_357,
      O => c_y(11)
    );
  c_y_11_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_11_IBUF_227,
      I2 => f_2_IBUF_359,
      I3 => a_11_IBUF_163,
      O => c_y_11_1_262
    );
  c_y_11_f5 : MUXF5
    port map (
      I0 => c_y_11_1_262,
      I1 => c_y(11),
      S => f_1_IBUF_358,
      O => y_11_OBUF_394
    );
  c_y_13_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(13),
      I1 => f_0_IBUF_357,
      O => c_y(13)
    );
  c_y_13_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_13_IBUF_229,
      I2 => f_2_IBUF_359,
      I3 => a_13_IBUF_165,
      O => c_y_13_1_266
    );
  c_y_13_f5 : MUXF5
    port map (
      I0 => c_y_13_1_266,
      I1 => c_y(13),
      S => f_1_IBUF_358,
      O => y_13_OBUF_396
    );
  c_y_12_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(12),
      I1 => f_0_IBUF_357,
      O => c_y(12)
    );
  c_y_12_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_12_IBUF_228,
      I2 => f_2_IBUF_359,
      I3 => a_12_IBUF_164,
      O => c_y_12_1_264
    );
  c_y_12_f5 : MUXF5
    port map (
      I0 => c_y_12_1_264,
      I1 => c_y(12),
      S => f_1_IBUF_358,
      O => y_12_OBUF_395
    );
  c_y_14_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(14),
      I1 => f_0_IBUF_357,
      O => c_y(14)
    );
  c_y_14_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_14_IBUF_230,
      I2 => f_2_IBUF_359,
      I3 => a_14_IBUF_166,
      O => c_y_14_1_268
    );
  c_y_14_f5 : MUXF5
    port map (
      I0 => c_y_14_1_268,
      I1 => c_y(14),
      S => f_1_IBUF_358,
      O => y_14_OBUF_397
    );
  c_y_16_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(16),
      I1 => f_0_IBUF_357,
      O => c_y(16)
    );
  c_y_16_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_16_IBUF_232,
      I2 => f_2_IBUF_359,
      I3 => a_16_IBUF_168,
      O => c_y_16_1_272
    );
  c_y_16_f5 : MUXF5
    port map (
      I0 => c_y_16_1_272,
      I1 => c_y(16),
      S => f_1_IBUF_358,
      O => y_16_OBUF_399
    );
  c_y_15_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(15),
      I1 => f_0_IBUF_357,
      O => c_y(15)
    );
  c_y_15_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_15_IBUF_231,
      I2 => f_2_IBUF_359,
      I3 => a_15_IBUF_167,
      O => c_y_15_1_270
    );
  c_y_15_f5 : MUXF5
    port map (
      I0 => c_y_15_1_270,
      I1 => c_y(15),
      S => f_1_IBUF_358,
      O => y_15_OBUF_398
    );
  c_y_17_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(17),
      I1 => f_0_IBUF_357,
      O => c_y(17)
    );
  c_y_17_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_17_IBUF_233,
      I2 => f_2_IBUF_359,
      I3 => a_17_IBUF_169,
      O => c_y_17_1_274
    );
  c_y_17_f5 : MUXF5
    port map (
      I0 => c_y_17_1_274,
      I1 => c_y(17),
      S => f_1_IBUF_358,
      O => y_17_OBUF_400
    );
  c_y_19_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(19),
      I1 => f_0_IBUF_357,
      O => c_y(19)
    );
  c_y_19_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_19_IBUF_235,
      I2 => f_2_IBUF_359,
      I3 => a_19_IBUF_171,
      O => c_y_19_1_278
    );
  c_y_19_f5 : MUXF5
    port map (
      I0 => c_y_19_1_278,
      I1 => c_y(19),
      S => f_1_IBUF_358,
      O => y_19_OBUF_402
    );
  c_y_18_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(18),
      I1 => f_0_IBUF_357,
      O => c_y(18)
    );
  c_y_18_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_18_IBUF_234,
      I2 => f_2_IBUF_359,
      I3 => a_18_IBUF_170,
      O => c_y_18_1_276
    );
  c_y_18_f5 : MUXF5
    port map (
      I0 => c_y_18_1_276,
      I1 => c_y(18),
      S => f_1_IBUF_358,
      O => y_18_OBUF_401
    );
  c_y_20_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(20),
      I1 => f_0_IBUF_357,
      O => c_y(20)
    );
  c_y_20_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_20_IBUF_237,
      I2 => f_2_IBUF_359,
      I3 => a_20_IBUF_173,
      O => c_y_20_1_282
    );
  c_y_20_f5 : MUXF5
    port map (
      I0 => c_y_20_1_282,
      I1 => c_y(20),
      S => f_1_IBUF_358,
      O => y_20_OBUF_404
    );
  c_y_22_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(22),
      I1 => f_0_IBUF_357,
      O => c_y(22)
    );
  c_y_22_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_22_IBUF_239,
      I2 => f_2_IBUF_359,
      I3 => a_22_IBUF_175,
      O => c_y_22_1_286
    );
  c_y_22_f5 : MUXF5
    port map (
      I0 => c_y_22_1_286,
      I1 => c_y(22),
      S => f_1_IBUF_358,
      O => y_22_OBUF_406
    );
  c_y_21_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(21),
      I1 => f_0_IBUF_357,
      O => c_y(21)
    );
  c_y_21_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_21_IBUF_238,
      I2 => f_2_IBUF_359,
      I3 => a_21_IBUF_174,
      O => c_y_21_1_284
    );
  c_y_21_f5 : MUXF5
    port map (
      I0 => c_y_21_1_284,
      I1 => c_y(21),
      S => f_1_IBUF_358,
      O => y_21_OBUF_405
    );
  c_y_23_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(23),
      I1 => f_0_IBUF_357,
      O => c_y(23)
    );
  c_y_23_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_23_IBUF_240,
      I2 => f_2_IBUF_359,
      I3 => a_23_IBUF_176,
      O => c_y_23_1_288
    );
  c_y_23_f5 : MUXF5
    port map (
      I0 => c_y_23_1_288,
      I1 => c_y(23),
      S => f_1_IBUF_358,
      O => y_23_OBUF_407
    );
  c_y_31_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(31),
      I1 => f_0_IBUF_357,
      O => c_y(31)
    );
  c_y_31_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_31_IBUF_249,
      I2 => f_2_IBUF_359,
      I3 => a_31_IBUF_185,
      O => c_y_31_1_306
    );
  c_y_31_f5 : MUXF5
    port map (
      I0 => c_y_31_1_306,
      I1 => c_y(31),
      S => f_1_IBUF_358,
      O => y_31_OBUF_416
    );
  c_y_30_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(30),
      I1 => f_0_IBUF_357,
      O => c_y(30)
    );
  c_y_30_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_30_IBUF_248,
      I2 => f_2_IBUF_359,
      I3 => a_30_IBUF_184,
      O => c_y_30_1_304
    );
  c_y_30_f5 : MUXF5
    port map (
      I0 => c_y_30_1_304,
      I1 => c_y(30),
      S => f_1_IBUF_358,
      O => y_30_OBUF_415
    );
  c_y_29_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(29),
      I1 => f_0_IBUF_357,
      O => c_y(29)
    );
  c_y_29_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_29_IBUF_246,
      I2 => f_2_IBUF_359,
      I3 => a_29_IBUF_182,
      O => c_y_29_1_300
    );
  c_y_29_f5 : MUXF5
    port map (
      I0 => c_y_29_1_300,
      I1 => c_y(29),
      S => f_1_IBUF_358,
      O => y_29_OBUF_413
    );
  c_y_28_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(28),
      I1 => f_0_IBUF_357,
      O => c_y(28)
    );
  c_y_28_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_28_IBUF_245,
      I2 => f_2_IBUF_359,
      I3 => a_28_IBUF_181,
      O => c_y_28_1_298
    );
  c_y_28_f5 : MUXF5
    port map (
      I0 => c_y_28_1_298,
      I1 => c_y(28),
      S => f_1_IBUF_358,
      O => y_28_OBUF_412
    );
  c_y_27_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(27),
      I1 => f_0_IBUF_357,
      O => c_y(27)
    );
  c_y_27_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_27_IBUF_244,
      I2 => f_2_IBUF_359,
      I3 => a_27_IBUF_180,
      O => c_y_27_1_296
    );
  c_y_27_f5 : MUXF5
    port map (
      I0 => c_y_27_1_296,
      I1 => c_y(27),
      S => f_1_IBUF_358,
      O => y_27_OBUF_411
    );
  c_y_26_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(26),
      I1 => f_0_IBUF_357,
      O => c_y(26)
    );
  c_y_26_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_26_IBUF_243,
      I2 => f_2_IBUF_359,
      I3 => a_26_IBUF_179,
      O => c_y_26_1_294
    );
  c_y_26_f5 : MUXF5
    port map (
      I0 => c_y_26_1_294,
      I1 => c_y(26),
      S => f_1_IBUF_358,
      O => y_26_OBUF_410
    );
  c_y_25_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(25),
      I1 => f_0_IBUF_357,
      O => c_y(25)
    );
  c_y_25_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_25_IBUF_242,
      I2 => f_2_IBUF_359,
      I3 => a_25_IBUF_178,
      O => c_y_25_1_292
    );
  c_y_25_f5 : MUXF5
    port map (
      I0 => c_y_25_1_292,
      I1 => c_y(25),
      S => f_1_IBUF_358,
      O => y_25_OBUF_409
    );
  c_y_24_1 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => c_y_share0000(24),
      I1 => f_0_IBUF_357,
      O => c_y(24)
    );
  c_y_24_2 : LUT4
    generic map(
      INIT => X"BE28"
    )
    port map (
      I0 => f_0_IBUF_357,
      I1 => b_24_IBUF_241,
      I2 => f_2_IBUF_359,
      I3 => a_24_IBUF_177,
      O => c_y_24_1_290
    );
  c_y_24_f5 : MUXF5
    port map (
      I0 => c_y_24_1_290,
      I1 => c_y(24),
      S => f_1_IBUF_358,
      O => y_24_OBUF_408
    );

end Structure;

