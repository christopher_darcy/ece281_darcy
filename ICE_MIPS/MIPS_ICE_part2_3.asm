#-------------------------------------------------------------------------------
# MIPS ICE - Part 2
# C3C Christopher Darcy, USAF / 11 Apr 2017 (Start Date) / 12 Apr 2017 (Completion Date)
#
# Purpose:  This file implements a program that determines if a user input
#	    integer is even or odd. Output 1 is odd, 0 for even.
# 
# Documentation: none.
#		
#-------------------------------------------------------------------------------


.data
load_string: .asciiz "\nEnter a number > "


.text
	# print input string
	li $v0, 4
	la $a0, load_string
	syscall
	
	# read integer input
	li $v0, 5
	syscall
	add $t0, $v0, $0
	
	# with integer division, multiplying then dividing by 2 will
	# equal to original number if even, if it doesn't, then
	# it's odd
	srl $t1, $t0, 1	# divide by 2
	sll $t1, $t1, 1	# multiply by 2
	
	# if/else statement
	bne $t0, $t1, odd	# if they aren't equal, branch to odd
	
	# print 0 for even
	li $v0, 1
	la $a0, 0
	syscall
	j end	# unconditional jump to end, skips else (odd) from executing
	


odd:
	# print 1 for odd
	li $v0, 1
	la $a0, 1
	syscall


end:
	j end
