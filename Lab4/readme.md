# Lab 4

## By C3C Christopher Darcy

## Table of Contents 
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
3. [Debugging](#debugging)
4. [Testing methodology or results](#testing-methodology-or-results)
5. [Answers to Lab Questions](#answers-to-lab-questions)
6. [Observations and Conclusions](#observations-and-conclusions)
7. [Feedback](#feedback)
8. [Documentation](#documentation)
 
### Objectives or Purpose 
The first objective of this lab is to create a single-cycle MIPS processor using VHDL. It will combine the ALU created previously in the course with the code provided and modify the code as needed to run the test program. After, the second objective is to code two new instructions, ORI and BNE, and create a mips file to test those.

The purpose of this lab is to demonstrate understanding of the ALU, MIPS processor, and VHDL. We created the ALU in a previous lab, and it was easy to recall that code and its functionality. Additionally, we have been working with VHDL all semester, so the actual coding was not difficult. However, the MIPS processor was a new concept, and this was the first lab tackling that issue. So, the purpose of this was to learn the internal operations of a single-cycle MIPS processor through dropping an ALU in it and modifying it for added instructions. 



### Preliminary design
Before running the simulation, I created expected signal values and output tables for each test in order to debug the program. 

![Table 1](images/table.PNG)
#####Figure 1: First 16 instructions for the original mipstest.asm file.

![Table 2](images/table_part2.PNG)
#####Figure 2: Expected values for the second mipstest2.asm file

These tables will be used to verify the correctness of the waveform simulation output later in the lab.



### Hardware schematic

The original hardware schematic, shown below, without any modifications was provided to us several times.

![Original Schematic](images/orig_schem.PNG)
#####Figure 3: Original schematic for a single-cycle MIPS processor.

![Part 2 Schematic](images/part2_schem.PNG)
#####Figure 4: Updated schematic to include added hardware for ori and bne functionality of part 2. All added hardware is in red for ease of visibility.

For the bne instruction, I had to add a mux to choose whether we were looking at the zero or not the zero. Along with that I needed a select line for it, and a not gate for the not_zero signal.

For the ori instruction I had to add a Zero Extend to make the immediate signal the correct width to go into the ALU, a mux to choose whether we want the normal input, or the ori input into the the ALU, and a select line to choose on that mux.

### Debugging
The main problem I ran into during this lab was signals being undefined. On several attempts, my code failed with the error that it could not find one signal or the other. 

![Syntax Errors](images/errors.PNG)
#####Figure 5: Errors thrown during the Behavioral Check Syntax. The main error is that a signal (ori_op in this case) was not declared.

Usually, these errors occurred because I did not carry the signal through into the datapath or controller, or other entity. I fixed the error shown above simply by poking a hole through the datapath and coding a signal down through it to carry the ori_op line. I was able to debug all these undeclared signal errors simply by tracking the signal through the VHDL entities and making sure everything was connected.




### Testing methodology or results 

####PART 1

Part 1 was using the provided code form the book to ensure our basic MIPS processor was working. It implemented the following assembly program.

![Part 1 Code](images/part1code.PNG)
#####Figure 6: Assembly code, with comments, for the mipstest.asm file implemented in this simulation.

Below is the waveform simulation results from the part 1 simulation. The required signals are colored purple for ease of sight, while the additional signals are green and used to verification of functionality. The waveform is split into 3 segments, and each is described below.

![Simulation 1.1](images/sim_1.PNG)
#####Figure 7: First segment of part 1 simulation.
Here we see the first 6 instructions. We can easily track these values and map them into the table we created as a part of the prelab to see that all signals are working properly.

![Simulation 1.2](images/sim_2.PNG)
#####Figure 8: Second segment of part 1 simulation.
This segment contains the next 9 instructions. Here we can see that the first branch is not taken and the second beq is taken. Again, the simulation matches the predicted values in the table.

![Simulation 1.3](images/sim_3.PNG)
#####Figure 9: Last instruction for part 1 simulation.
This shows the waveform simulation output for the last instruction, which is not present in our prediction table but I verified it as the correct output through stepping through the assembly program. 


####PART 2

The completed prediction table can be found above, under the Preliminary Design section. This part implemented the following assembly program.

![Part 2 Code](images/part2code.PNG) 
#####Figure 10: The assembly program (lab4_test2.asm) implemented in part, with comments describing its functionality.

In the waveform simulations below, the purple signals are the machine code instructions, srca, srcb, and results which can be used to quickly verify the functionality. The green signals below are the signals used in the prediction tables and were used to debug when instructions were not executing properly. 

The quick checks I did to verify the correctness of this output was looking at the instr[31:0] signal and the final output value. The instr signal has all the machine code instructions, in the correct order (same as in the memfile2.dat file). The final output is 0xffff7f54, which is the value of the final, sw, instruction.

![Simulation 2.1](images/sim2_1.PNG)
#####Figure 11: The first 40 ns of the part 2 simulation.
Here, we see the first 40 nanoseconds of the part 2 simulation. It starts with a reset for the first 22.5 ns for setup. We see our first machine code instruction 34088000, an ori, a 0 is loaded into srca and 0x00008000 is loaded into srcb, which results in the correct value of 0x00008000. Following that instruction is the addi instruction, which puts -32768, or 0xffff8000, into register $t1. 


![Simulation 2.2](images/sim2_2.PNG)
####Figure 12: Part 2 simulation 40 ns to 100 ns, next 6 instructions.
In this segment we see another ori instruction correctly place 0x8001 into its register. Then, we encounter the first branch instruction, beq, at 50 ns. The values aren't equal, so our zero signal stays at 0, and prevents the branch from occurring. Following that is the slt instruction at 60 ns, which correctly results in a 1. Then, we have another of the added instructions: bne at 70 ns. Here the registers are not equal, so the branch is taken, we can see that the bne-op is high to choose the not-zero signal in that mux, allowing the branch to occur. It branches to 'here' which is a sub instruction that works correctly to result in 1. The last instruction in this segment is another ori, which correctly implements a logical or between the register value and immediate value 0xFF. 



![Simulation 2.3](images/sim2_3.PNG)
####Figure 13: Part 2 simulation, 100 ns to 130 ns, the final 3 instructions.
The first instruction in this final segment is the ori from the last instruction of the previous segment. Following that, at 100 ns is the add instruction beginning the 'there' section of code, which correctly adds 1+1 to result in 2. At 110 ns is the next instruction which subtracts 0x80ff from 1, resulting in 0xffff7f02. Finally, the final instruction attempts to sw, or store word, at address 82($t0); which is the 82nd element of the address at $t0. $t0 contains 0xffff7f02, and adding 82 (0x52 in hexadecimal) results in 0xffff7f54. The last result is this value and this is also the address that the lab4_test2.asm file tries to store into, which indicates that the simulation ran correctly.


### Answers to Lab Questions
3.) What address will the final sw instruction write to and what value will it write?

It will write the value of $2, which is 7, to the the 84th byte of %0, which is just 0x00000084.


(Part 2) What address and data value are written by the sw instruction?

It will try to write to 0xFFFF7F54, which is 82 bytes plus the address stored in $t0.It the data value it will try to write is what is in $t3, which is 2.

### Feedback

Number of hours spent on Lab 4: ____13+____ (no points associated with this unless you leave it blank)

Suggestions to improve Lab 4 in future years: (use blank space below)

While the Lab took a long time to complete, it actually wasn't that difficult. What took a lot of time was understanding all the code we were given, and figuring out how to add the functionality. I think if we split some of the VHDL code up into separate files, it would have been a lot easier to tackle.



### Documentation

C3C Ryer described to me what exactly Table 1 was, and I was able to use his description to understand the meaning of each column in the table during the Prelab.