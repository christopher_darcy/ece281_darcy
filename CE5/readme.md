# Computer Exercise #5

## By C3C Christopher Darcy

## Table of Contents 
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
 * [Code](#code)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
 * [Pseudocode](#pseudocode)
4. [Hardware schematic](#hardware-schematic)
5. [Well-formatted code](#well-formatted-code)
6. [Debugging](#debugging)
7. [Testing methodology or results](#testing-methodology-or-results)
8. [Answers to Lab Questions](#answers-to-lab-questions)
9. [Observations and Conclusions](#observations-and-conclusions)
10. [Feedback](#feedback)
11. [Documentation](#documentation)
 
### Objectives or Purpose 
The objective of this Computer Exercise is to create a high-low guessing game using a MIPS simulator. This will be completed by designing, writing, and testing the high-low guessing game created in MIPS assembly.The purpose is to teach MIPS assembly and how the basic instructions work, as well as familiarize ourselves with this level of computer development.


### Preliminary design
The high-low guessing game works by having a user try to guess the number the computer has. The numbers will be between 0-9. If the guess is too high or too low, the computer will print "H" or "L" respectively and waits for more input. If the player guesses the correct number, then the program prints "W" and exits. The pseudocode for this program can be seen within the Pseudocode section below.


#####Code
All code for this project is included in this repository under the **code/ file**. Key points are also included in the well formatted code section of this markdown file.





#####VHDL Header:
	--------------------------------------------------------------------
	-- Name:C3C Christopher Darcy
	-- Date: 14 April 2017
	-- Course: ECE 281
	-- File:CE5/readme.md
	-- HW:	CE5
	--
	-- Purp: 
	-- 
	--
	-- Doc:	None.
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	------------------------------------------------------------------------- 

	
### Software flow chart or algorithms
No flow chart or algorithm was needed for this CE,



#### Pseudocode:
The following is pseudocode for the implementation of the high-low game. 

	--------------------------------------------------------------------
	-- integer lucky_number = some number between 0-9
	-- integer guess = user input
	--
	--
	-- if lucky_number = guess
	--		print "w"
	--		terminate program
	--	else
	-- 		if lucky_number > guess
	--			print "l"
	--			return to user input
	--		else
	--			print "h"
	--			return to user input
	------------------------------------------------------------------------- 

### Hardware schematic
None.
 




### Well-formatted code
All code for this project is included in this repository under the **code/ file**.


### Debugging
I only came across one issue in this exercise. It occurred when I tried to load all 32 bytes of an address using lui. It resulted in the following error:

![Error](images/error.PNG)
#####Figure 1: Operand out of range error.

I solved it by only putting the first 16 bytes (0xFFFF) into the register using lui.




### Testing methodology or results 
I was able to design, program, and test this game with relative ease. As was noted earlier, I only encountered one bug, which was easily solved. In testing my program, it works as outlined and in the guide (and articulated in the preliminary design section of this file). 

![Game 1](images/game1.PNG)
#####Figure 2: First test game

This test shows 5 example inputs both high and low, and finally the winning guess. This demonstrates that the program correctly branches to all sections and has the proper output.  It also tests the high bound of 9. 

![Game 2](images/game2.PNG)
#####Figure 3: Second test game

The second test game again shows proper functionality, and also how the program terminates after the correct answer in entered, any additional input  is ignored. The repetition of incorrect guesses show that the program is able to continually take more input and give proper output. Both these tests are good as the test a variety of values, including both the high and low bounds.


### Answers to Lab Questions
1.) What addressing modes did you use in your program?  Give an example line for each type used.
	
I used Register-only, immediate, base, PC-relative, and pseudo-direct addressing. Example lines for each are provided below.

Register-only:  slt $t3, $t0, $t2

Immediate:		addi $t0, $0, 0x36

Base:			lw $t1, 0($s0)

PC-relative:	beq $t2, $t0, won

Pseudo-relative: j end


2.) Were there any other instructions or pseudo instructions that would have been useful for your program?  Which ones and why?

This program is relatively simple and does not really require any other instructions or pseudo-instructions. However, it would have been convenient to use syscalls to read and print the integers. 


3.) Would subroutines have been useful in your program?  Why or why not?

Yes subroutines would have been useful as lot of this code executes repetitively and subroutines act as sort of a method that chunks this code together and allows for easy jumps to it.


### Observations and Conclusions
Overall, I was successful in designed, programming, and testing the high-low guessing game using the MIPS assembly language and the lightweight MIPS simulator. As the debugging and testing sections describe, I was able to go through this process with relatively few bugs and errors. In the end, I was able to create the high-low game with all the required functionality.


### Feedback
Number of hours spent on CE5: ______2________ (no points associated with this unless you leave it blank)


Suggestions to improve CE5 in future years: (use blank space below)

None, this CE was very straightforward and I enjoyed it. 


### Documentation
C3C Ryer explained how I could use the slt instruction to find determine if the number was greater or less than another number, which I implemented into my program. 