#-------------------------------------------------------------------------------
# Ce5
# C3C Christopher Darcy, USAF / 13 Apr 2017 (Start Date) / 14 Apr 2017 (Completion Date)
#
# Purpose:  This file implements the high-low guessing game.
#	    
# 
# Documentation: none.
#-------------------------------------------------------------------------------

start:
	addi $t0, $0, 0x36		# load lucky number as 6 
	lui $s0, 0xFFFF			# saves address 0xFFFF0000 into s0
	lw $t1, 0($s0)			# puts whats in address s0 into t1
	beq $t1, $0, start 		# if first bit of address is 0, restart because no input was given
	lw $t2, 4($s0)			# put input into t2
	beq $t2, $t0, won		# branch if input equals number
	slt $t3, $t0, $t2		# put 1 in t3 if lucky number is less than guess, else 0
	beq $t3, $0, low		# if 0 in t3, branch to low
	addi $t9, $0, 0x48		# else, its high so load H ascii value
	sw $t9, 12($s0)			# print H
	j start				# jump back to start for more input



	
low:
	addi $t9, $0, 0x4C		# load L into t9
	sw $t9, 12($s0)			# print L
	j start				# jump back to start for more input

won:
	addi $t9, $0, 0x57		# load W into t9
	sw $t9, 12($s0)			# print W
	j end				# exit program
	
end:
	j end

	