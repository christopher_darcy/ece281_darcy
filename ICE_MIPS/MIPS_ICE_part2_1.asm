#-------------------------------------------------------------------------------
# MIPS ICE - Part 2
# C3C Christopher Darcy, USAF / 11 Apr 2017 (Start Date) / 12 Apr 2017 (Completion Date)
#
# Purpose:  This file implements a program that gives the 2's complement
#	    of a user input integer.
# 
# Documentation: none.
#	
#-------------------------------------------------------------------------------


.data
	load_string: .asciiz "\nEnter a number > "
	print_string: .asciiz "\n2's Complement is: "

.text
	# print input prompt
	li $v0, 4
	la $a0, load_string
	syscall
	
	# read integer input
	li $v0, 5
	syscall
	add $t0, $v0, $0	# move integer to $t0 register
	
	# calculate 2's complement and put in $t1
	xori $t1, $t0, 0xffffffff # flip the bits
	addi $t1, $t1, 1	# add one
	
	# print output message
	li $v0, 4
	la $a0, print_string
	syscall
	
	# print 2's complement
	li $v0, 1
	la $a0, ($t1)
	syscall


end:
	j end
