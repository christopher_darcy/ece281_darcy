--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : top_nexys2.vhd
--| AUTHOR(S)     : C3C Chris Darcy
--| CREATED       : 03/2017
--| DESCRIPTION   : This file implements the Lab 3 top level module for a NEXYS2 in 
--|					order to drive an elevator controller and I/O.
--|
--|					Inputs: clk 	 --> 50 MHz clock from FPGA
--|							BTN(3:0) --> Rst Master, Rst FSM, Rst Clk, GO (request floor)
--|							SW(7:4)  --> Passenger location (floor select bits)
--| 						SW(3:0)  --> Desired location (floor select bits)
--| 						 - REQUIRED FUNCTIONALITY ONLY: SW(1) --> up_down, SW(2) --> stop
--|							 
--|					Outputs: LED(7:0) --> indicates elevator movement with sweeping pattern
--|							   - LED(0) --> LED(7) = MOVING UP
--|							   - LED(7) --> LED(0) = MOVING DOWN
--|							   - ALL OFF		   = NOT MOVING
--|							 AN(3:0)    --> seven-segment display anode active-low enable (AN3 ... AN0)
--|							 SSEG(6:0)	--> seven-segment display cathodes (CA ... CG.  DP unused)
--|
--| DOCUMENTATION : None
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES : clock_divider.vhd
--|						synchronizer.vhd
--|						MooreElevatorController_Shell.vhd
--|						sevenSegDecoder.vhd
--|						TDM4.vhd
--|						thunderbird_fsm.vhd
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : clock_divider.vhd, YOUR_LAB1_MODULE, YOUR_MOORE_FSM_MODULE
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    b_<port name>            = bidirectional port
--|    i_<port name>            = input port
--|    o_<port name>            = output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  
entity top_nexys2 is
	port(
		i_clk_50MHz : in std_logic; -- native 50MHz FPGA clock
		i_BTN		: in std_logic_vector(3 downto 0);  -- Rst Master, Rst FSM, Rst Clk, GO
		i_SW  		: in std_logic_vector(7 downto 0);  -- SW(7:4) Passenger location (floor select bits)
													    -- SW(3:0) Desired location (floor select bits)
													    --    OR
													    -- SW(1) --> up_down, SW(2) --> stop
		o_LED 		: out std_logic_vector(7 downto 0); -- lights to indicate elevator movement
														--   LED(0) --> LED(7) = MOVING UP
														--   LED(7) --> LED(0) = MOVING DOWN
														--   ALL OFF		   = NOT MOVING		
		o_AN_n		: out std_logic_vector(3 downto 0); -- 7SD anode enable (active-low)
		o_SSEG_n	: out std_logic_vector(6 downto 0) -- 7SD segment cathode (active-low)
	);
end top_nexys2;

architecture top_nexys2_arch of top_nexys2 is 
	
  -- declare components
	component clock_divider is
		generic ( constant k_DIV : natural := 3	); -- how much you divide clock
		port(
			i_clk    : in std_logic;
			i_reset  : in std_logic;		   -- asynchronous
			o_clk    : out std_logic		   -- divided (slow) clock
		);
	end component;
	
	component clock_divider_two is
		generic ( constant k_DIV : natural := 3	); -- how much you divide clock
		port(
			i_clk    : in std_logic;
			i_reset  : in std_logic;		   -- asynchronous
			o_clk    : out std_logic		   -- divided (slow) clock
		);
	end component;
	
	component TDM4 is
		generic ( constant k_WIDTH : natural  := 4); -- bits in input and output
		Port ( i_CLK     	: in  STD_LOGIC;
           i_RESET	 	: in  STD_LOGIC; -- asynchronous
           i_D3 		: in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
			  i_D2 		: in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
		     i_D1 		: in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
		     i_D0 		: in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
		     o_DATA		: out STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
		     o_SEL		: out STD_LOGIC_VECTOR (3 downto 0)	-- selected data line (one-cold)
		);
	end component;
	
	component synchronizer is
		generic ( constant k_WIDTH : natural := 2	); -- # bits in registers
		port ( 	
			i_clk    : in  std_logic;
			i_reset  : in  std_logic;		   -- asynchronous
			i_D		 : in  std_logic_vector(k_WIDTH - 1 downto 0);
			o_Q      : out std_logic_vector(k_WIDTH - 1 downto 0)
		);
	end component;
	
	
	component MooreElevatorController_Shell is
		port(
			i_clk : in  STD_LOGIC;
         i_reset : in  STD_LOGIC;
			i_go : in STD_LOGIC;
         i_starting : in  STD_LOGIC_VECTOR (3 downto 0);
         i_desired : in  STD_LOGIC_VECTOR (3 downto 0);
			o_floor_ones : out STD_LOGIC_VECTOR (3 downto 0);
         o_floor_tens : out  STD_LOGIC_VECTOR (3 downto 0);
			o_right : out std_logic;
			o_left : out std_logic
		);
	end component;
	
	component sevenSegDecoder is
	port(
		i_D  : in  std_logic_vector(3 downto 0); 
		o_S  : out std_logic_vector(6 downto 0)  
	);
	end component;
	
	component thunderbird_fsm is
	port(
		i_clk, i_reset : in std_logic;
		i_left, i_right : in std_logic;
		o_lights_L : out std_logic_vector(2 downto 0);
		o_lights_R : out std_logic_vector(2 downto 0)
	);
	end component;
	
	
  -- create wires to connect components
  signal c_clk_OR : std_logic; -- carries signal from OR gate to clock_divider reset
  signal c_clk_any : std_logic; -- carries reset from any btn to clock two
  signal c_BTN_sync : std_logic_vector(3 downto 0); -- carries output from synchronizer to OR gates
  signal c_Moore_OR : std_logic; -- carries signal from OR gate to MooreElevator reset
  signal c_clk : std_logic; -- carries signal from clock_divider to MooreElevator
  signal c_clk_two : std_logic; -- carries signal from faster clock to TDM4
  signal c_clk_three : std_logic; -- carries signal from third clock to thunderbird fsm
  signal c_floortens_out : std_logic_vector(3 downto 0); --carries floor output from MooreElevator into TDM
  signal c_floorones_out : std_logic_vector(3 downto 0); --carries floor output from MooreElevator into TDM
  signal c_left_out : std_logic; -- carries signal from MooreElevator to thunderbird_fsm
  signal c_right_out : std_logic; -- carries signal from MooreElevator to thunderbird_fsm
  signal c_left_lights : std_logic_vector(2 downto 0); -- carries left lights from thunderbird to leds
  signal c_right_lights : std_logic_vector(2 downto 0); -- carries right lights from thunderbird to leds
  signal c_data_out : std_logic_vector(3 downto 0); -- carries floor output from TDM into sevenSeg
  signal c_anode : std_logic_vector(3 downto 0); -- carries anode select from TDM to o_AN_n

  
begin
	-- PORT MAPS / COMPONENT INSTANTIATION --------------
	clock_divider_inst : clock_divider 
	generic map( k_DIV => 12500000)
	port map(
		i_clk => i_clk_50MHz, -- fast clock input
		i_reset => c_clk_OR,
		o_clk => c_clk  --outputs slow clock into signal c_clk
	);
	
	clock_divider_two_inst : clock_divider 
	generic map( k_DIV => 10000)
	port map(
		i_clk => i_clk_50MHz, -- fast clock input
		i_reset => c_BTN_sync(3),
		o_clk => c_clk_two  --outputs slow clock into signal c_clk_two
	);
	
	clock_divider_three_inst : clock_divider 
	generic map( k_DIV => 3125000)
	port map(
		i_clk => i_clk_50MHz, -- fast clock input
		i_reset => c_clk_any,
		o_clk => c_clk_three  --outputs slow clock into signal c_clk_three
	);
	
	MooreElevatorController_inst : MooreElevatorController_Shell 
	port map(
		i_clk => c_clk,
		i_reset => c_Moore_OR,
		i_go => c_BTN_sync(0),
		i_starting => i_SW(7 downto 4),
		i_desired => i_SW(3 downto 0),
		o_floor_ones => c_floorones_out,
		o_floor_tens => c_floortens_out,
		o_left => c_left_out,
		o_right => c_right_out
	);
	
	TDM4_inst : TDM4
	port map(
		i_CLK => c_clk_two, 
      i_RESET => c_BTN_sync(3),
      i_D3 => 	c_floortens_out,	
		i_D2 => c_floorones_out,
		i_D1 => "0000",	
		i_D0 => "0000",
		o_DATA => c_data_out,
		o_SEL => c_anode
	);
	
	
	synchronizer_inst : synchronizer 
	generic map (k_WIDTH => 4)
	port map(	
		i_clk => i_clk_50MHz,
		i_reset => '0',
		i_D => i_BTN,
		o_Q => c_BTN_sync
	);
	
	thunderbird_fsm_inst : thunderbird_fsm
	port map(
		i_clk => c_clk_three,
		i_reset => c_clk_OR,
		i_left => c_left_out,
		i_right => c_right_out,
		o_lights_L => c_left_lights,
		o_lights_R => c_right_lights
	);
	

	
	sevenSegDecoder_inst : sevenSegDecoder port map(
		i_D => c_data_out,
		o_S => o_SSEG_n
	);
	

	
	-- CONCURRENT STATEMENTS ----------------------------
	c_clk_OR <= c_BTN_sync(3) or c_BTN_sync(1);
	c_clk_any <= c_BTN_sync(3) or c_BTN_sync(1) or c_BTN_sync(2);
	c_Moore_OR <= c_BTN_sync(3) or c_BTN_sync(2);
	
	
	o_AN_n <= c_anode;
	
	
	
	
	
	-- PROCESSES ----------------------------------------
	light_process : process(c_right_out)
	begin
		if(c_right_out = '1') then
			o_LED <= c_right_lights(0) & c_right_lights(0) & c_right_lights(0) & c_right_lights(1) & c_right_lights(1) &
						c_right_lights(2) & c_right_lights(2) & c_right_lights(2);
		else
			o_LED <= c_left_lights(2) & c_left_lights(2) & c_left_lights(2) & c_left_lights(1) & c_left_lights(1) &
						c_left_lights(0) & c_left_lights(0) & c_left_lights(0);
		end if;
	
	end process;


	
end top_nexys2_arch;
