library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clock_divider is
	port(
		i_clk    : in std_logic;
		i_reset  : in std_logic;
		o_clk    : out std_logic
	);
end clock_divider;

architecture horrible_method of clock_divider is
	signal clk_bus : unsigned(23 downto 0);
begin

	process (i_clk, i_reset) is
	begin
		if i_reset = '1' then
			clk_bus <= (others => '0');
		elsif rising_edge(i_clk) then
			clk_bus <= clk_bus + 1;
		end if;
	end process;
	
	o_clk <= clk_bus(23);
end horrible_method;