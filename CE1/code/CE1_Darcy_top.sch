<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_4" />
        <signal name="XLXN_5" />
        <signal name="sw7" />
        <signal name="led0" />
        <signal name="XLXN_15" />
        <signal name="sw5" />
        <signal name="sw6" />
        <signal name="XLXN_22" />
        <signal name="XLXN_23" />
        <port polarity="Input" name="sw7" />
        <port polarity="Output" name="led0" />
        <port polarity="Input" name="sw5" />
        <port polarity="Input" name="sw6" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="and2" name="XLXI_1">
            <blockpin signalname="XLXN_15" name="I0" />
            <blockpin signalname="sw7" name="I1" />
            <blockpin signalname="XLXN_4" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_2">
            <blockpin signalname="sw5" name="I0" />
            <blockpin signalname="sw6" name="I1" />
            <blockpin signalname="XLXN_5" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_3">
            <blockpin signalname="XLXN_5" name="I0" />
            <blockpin signalname="XLXN_4" name="I1" />
            <blockpin signalname="led0" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_5">
            <blockpin signalname="sw6" name="I" />
            <blockpin signalname="XLXN_15" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="sw7">
            <wire x2="1152" y1="400" y2="400" x1="800" />
        </branch>
        <branch name="led0">
            <wire x2="1728" y1="544" y2="544" x1="1712" />
        </branch>
        <instance x="1152" y="528" name="XLXI_1" orien="R0" />
        <branch name="XLXN_15">
            <wire x2="1152" y1="464" y2="464" x1="1136" />
        </branch>
        <branch name="sw5">
            <wire x2="1152" y1="656" y2="656" x1="800" />
        </branch>
        <branch name="sw6">
            <wire x2="880" y1="592" y2="592" x1="800" />
            <wire x2="1152" y1="592" y2="592" x1="880" />
            <wire x2="912" y1="464" y2="464" x1="880" />
            <wire x2="880" y1="464" y2="592" x1="880" />
        </branch>
        <instance x="912" y="496" name="XLXI_5" orien="R0" />
        <iomarker fontsize="28" x="800" y="400" name="sw7" orien="R180" />
        <iomarker fontsize="28" x="800" y="656" name="sw5" orien="R180" />
        <iomarker fontsize="28" x="800" y="592" name="sw6" orien="R180" />
        <instance x="1152" y="720" name="XLXI_2" orien="R0" />
        <branch name="XLXN_4">
            <wire x2="1424" y1="432" y2="432" x1="1408" />
            <wire x2="1424" y1="432" y2="512" x1="1424" />
            <wire x2="1456" y1="512" y2="512" x1="1424" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="1424" y1="624" y2="624" x1="1408" />
            <wire x2="1424" y1="576" y2="624" x1="1424" />
            <wire x2="1456" y1="576" y2="576" x1="1424" />
        </branch>
        <instance x="1456" y="640" name="XLXI_3" orien="R0" />
        <iomarker fontsize="28" x="1728" y="544" name="led0" orien="R0" />
    </sheet>
</drawing>