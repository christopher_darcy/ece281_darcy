--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : alu_tb.vhd (TEST BENCH)
--| AUTHOR(S)     : C9C John Doe, C8C Jane Day
--| CREATED       : 01/01/2017
--| DESCRIPTION   : This file simply provides a template for all VHDL assignments
--| 				- Be sure to include your Documentation Statement below!
--|
--| DOCUMENTATION : 
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES : alu.vhd
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim, std_logic_signed
--|    Files     : LIST ANY DEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use ieee.std_logic_signed.all;

library unisim;
  use UNISIM.Vcomponents.ALL;
  
entity alu_tb is
end alu_tb;

architecture test_bench of alu_tb is 
	
  -- declare the component of your top-level design unit under test (UUT)
  component alu is
		port(
			a, b : in std_logic_vector(31 downto 0);
			f    : in std_logic_vector(2 downto 0);
			y    : out std_logic_vector(31 downto 0);
			zero : out std_logic
		);
	
  end component;

  -- declare any additional components required
  
  -- declare signals needed to stimulate the UUT inputs
  signal c_f : std_logic_vector(2 downto 0) := "000"; 
  signal c_a : std_logic_vector(31 downto 0) := x"00000000";
  signal c_b : std_logic_vector(31 downto 0) := x"00000000";
  -- also need signals for the outputs of the UUT
  signal c_y : std_logic_vector(31 downto 0) := x"00000000";
  signal c_zero : std_logic := '0';
  
begin
	-- PORT MAPS ----------------------------------------

	-- map ports for any component instances (port mapping is like wiring hardware)
	uut_inst : alu port map(
		a => c_a,
		b => c_b, 
		f => c_f,
		y => c_y,
		zero => c_zero
	);

	-- CONCURRENT STATEMENTS ----------------------------

	
	-- PROCESSES ----------------------------------------
	
	-- Provide a comment that describes each process
	-- block them off like the modules above and separate with SPACE
	-- You will at least have a test process
	
	
	-- Test Plan Process --------------------------------
	-- Implement the test plan here.  Body of process is continuously from time = 0  
	test_process : process 
	begin
		-- ADD
		c_f <= "010"; c_a <= x"00000000"; c_b <= x"00000000"; wait for 10 ns; assert c_y <= x"00000000" and c_zero <= '1' report "Test 1 failed";
		c_f <= "010"; c_a <= x"00000000"; c_b <= x"FFFFFFFF"; wait for 10 ns; assert c_y <= x"FFFFFFFF" and c_zero <= '0' report "Test 2 failed";
		c_f <= "010"; c_a <= x"00000001"; c_b <= x"FFFFFFFF"; wait for 10 ns; assert c_y <= x"00000000" and c_zero <= '1' report "Test 3 failed";
		c_f <= "010"; c_a <= x"000000FF"; c_b <= x"00000001"; wait for 10 ns; assert c_y <= x"00000100" and c_zero <= '0' report "Test 4 failed";
		
		-- SUB
		c_f <= "110"; c_a <= x"00000000"; c_b <= x"00000000"; wait for 10 ns; assert c_y <= x"00000000" and c_zero <= '1' report "Test 5 failed";
		c_f <= "110"; c_a <= x"00000000"; c_b <= x"FFFFFFFF"; wait for 10 ns; assert c_y <= x"00000001" and c_zero <= '0' report "Test 6 failed";
		c_f <= "110"; c_a <= x"00000001"; c_b <= x"00000001"; wait for 10 ns; assert c_y <= x"00000000" and c_zero <= '1' report "Test 7 failed";
		c_f <= "110"; c_a <= x"00000100"; c_b <= x"00000001"; wait for 10 ns; assert c_y <= x"000000FF" and c_zero <= '0' report "Test 8 failed";
		
		-- SLT
		c_f <= "111"; c_a <= x"00000000"; c_b <= x"00000000"; wait for 10 ns; assert c_y <= x"00000000" and c_zero <= '1' report "Test 9 failed";
		c_f <= "111"; c_a <= x"00000000"; c_b <= x"00000001"; wait for 10 ns; assert c_y <= x"00000001" and c_zero <= '0' report "Test 10 failed";
		c_f <= "111"; c_a <= x"00000000"; c_b <= x"FFFFFFFF"; wait for 10 ns; assert c_y <= x"00000000" and c_zero <= '1' report "Test 11 failed";
		c_f <= "111"; c_a <= x"00000001"; c_b <= x"00000000"; wait for 10 ns; assert c_y <= x"00000000" and c_zero <= '1' report "Test 12 failed";
		c_f <= "111"; c_a <= x"FFFFFFFF"; c_b <= x"00000000"; wait for 10 ns; assert c_y <= x"00000001" and c_zero <= '0' report "Test 13 failed";
		
		-- AND
		c_f <= "000"; c_a <= x"FFFFFFFF"; c_b <= x"FFFFFFFF"; wait for 10 ns; assert c_y <= x"FFFFFFFF" and c_zero <= '0' report "Test 14 failed";
		c_f <= "000"; c_a <= x"FFFFFFFF"; c_b <= x"12345678"; wait for 10 ns; assert c_y <= x"12345678" and c_zero <= '0' report "Test 15 failed";
		c_f <= "100"; c_a <= x"12345678"; c_b <= x"02040608"; wait for 10 ns; assert c_y <= x"10305070" and c_zero <= '0' report "Test 16 failed";
		c_f <= "100"; c_a <= x"AAAAAAAA"; c_b <= x"FFFF0000"; wait for 10 ns; assert c_y <= x"0000AAAA" and c_zero <= '0' report "Test 17 failed";
		
		-- OR
		c_f <= "001"; c_a <= x"FFFFFFFF"; c_b <= x"FFFFFFFF"; wait for 10 ns; assert c_y <= x"FFFFFFFF" and c_zero <= '0' report "Test 18 failed";
		c_f <= "001"; c_a <= x"12345678"; c_b <= x"87654321"; wait for 10 ns; assert c_y <= x"97755779" and c_zero <= '0' report "Test 19 failed";
		c_f <= "001"; c_a <= x"00000000"; c_b <= x"FFFFFFFF"; wait for 10 ns; assert c_y <= x"FFFFFFFF" and c_zero <= '0' report "Test 20 failed";
		c_f <= "001"; c_a <= x"00000000"; c_b <= x"00000000"; wait for 10 ns; assert c_y <= x"00000000" and c_zero <= '1' report "Test 21 failed";
		c_f <= "101"; c_a <= x"00000000"; c_b <= x"00000000"; wait for 10 ns; assert c_y <= x"FFFFFFFF" and c_zero <= '0' report "Test 22 failed";
		
		-- Extra Cases
		c_f <= "010"; c_a <= x"80000000"; c_b <= x"80000000"; wait for 10 ns; assert c_y <= x"00000000" and c_zero <= '1' report "Test 23 failed";
		c_f <= "111"; c_a <= x"000000FF"; c_b <= x"FFFFFFFF"; wait for 10 ns; assert c_y <= x"00000001" and c_zero <= '1' report "Test 24 failed";
		c_f <= "110"; c_a <= x"FFFFFFFF"; c_b <= x"FFFFFFFF"; wait for 10 ns; assert c_y <= x"00000000" and c_zero <= '1' report "Test 25 failed";
		
		-- end on all 0's
		c_f <= "000"; c_a <= x"00000000"; c_b <= x"00000000"; wait for 10 ns; 
		
		
		wait;
	end process;	
	-----------------------------------------------------	
	
end test_bench;
