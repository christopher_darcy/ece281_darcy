# Lab 3

## By C3C Christopher Darcy

## Table of Contents 
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
 * [Code](#code)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
 * [Pseudocode](#pseudocode)
4. [Hardware schematic](#hardware-schematic)
5. [Well-formatted code](#well-formatted-code)
6. [Debugging](#debugging)
7. [Testing methodology or results](#testing-methodology-or-results)
8. [Answers to Lab Questions](#answers-to-lab-questions)
9. [Observations and Conclusions](#observations-and-conclusions)
10. [Feedback](#feedback)
11. [Documentation](#documentation)
 
### Objectives or Purpose 
The objective of this lab is to build an elevator controller and implement it onto the Nexys2 Board. The required functionality is that one elevator goes up and down between 4 floors, displays the floor on seven segment display number 2, and is controlled by two switches (up-down and stop) and 3 buttons (master-reset, fsm-reset, and clock-reset). Additional functionality that I included was the ability to specify a starting and desired floor using 8 switches, 16 floors instead of only 4, and moving LEDs (using the thunderbird_fsm) to indicate the direction the elevator is moving.  The purpose of this lab is to gain further understanding in FSMs and also to be able to combine sequential and combinatorial blocks as well as integrate modules from previous exercises.



### Preliminary design
The Prelab required a top-level schematic detailing how the elevator controller would be implemented. Below is the schematic I drew for this.

![Top Level Schematic](images/top_level_schematic.jpg)
#####Figure 1: Top-level schematic drawing of the architecture for the required functionality of Lab 3.

This is the Prelab schematic for the required functionality of the elevator controller. It includes the stop and up_down switches and 3 different reset buttons. It routes the buttons through a synchronizer to ensure they don't bounce and put the FSM in an invalid state. The output is simple, one anode is manual powered on, the LEDs are all powered off, and the output from the sevenSegDecoder goes straight out to the board.

I added additional functionality as described below after completing this required functionality. The top level schematic for the additional functionality can be scene in the schematic section below.

![Additional Functionality](images/functionality.jpg)
#####Figure 2: Description of the additional functionality. Includes 16 floors, LEDs showing movement, and switches for starting and desired passenger locations.


#####Code
All code for this project is included in this repository under the **code/ file**. Key points are also included in the well formatted code section of this markdown file.





#####VHDL Header:
	--------------------------------------------------------------------
	-- Name:C3C Christopher Darcy
	-- Date: 20 March 2017
	-- Course: ECE 281
	-- File:Lab3/readme.md
	-- HW:	Lab3
	--
	-- Purp: Implement a driver for an elevator, using switches, buttons, and FSMs.
	-- 
	--
	-- Doc:	None.
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	------------------------------------------------------------------------- 

	
### Software flow chart or algorithms
Neither a software flow chart or any type of algorithm was necessary to complete this lab.



#### Pseudocode:
No pseudocode was needed for this Lab.




### Hardware schematic
After adding all the additional functionality, the top level schematic ended up looking very different then the one I drew in the Prelab. 

![Final Top Level](images/final_top_level.PNG)
#####Figure 3: RTL schematic for Lab 3 with all the additional functionality.

The schematic is a lot simpler then it looks, most the components are the same as the initial one I drew. This one adds the TDM4, thunderbird-fsm, and two additional clocks. Also, because I implemented the moving LEDs with a single, unmodified thunderbird-fsm, I had to concatenate a lot of signals together, which is what created all those o_LED<##> components.


As can be seen, this schematic has three different clocks, one for the ElevatorController, the TDM4, and the thunderbird_fsm. Also, the TDM4 comes right before the sevenSegDecoder, it could also be placed behind it; however my implementation puts it first.

### Well-formatted code
Please see the **Lab3/code** folder to view the commented code for this Lab.




### Debugging
The only real issue I had, aside from trying to figure out how to implement the functionality, was with my testbench for the changing inputs. I used xilinx to create a testbench file for the MooreElevator Controller, and when the simulation ran all the inputs were undefined. I fixed this by holding the elevator in a reset state before trying to apply signals. This solved the undefined issues, except for the right and left signals, which were undefined until they were actually used as the elevator started moving. 




### Testing methodology or results 
First, I completed the required functionality and demonstrated that during the Lab days. After that, I began working on the additional functionality. I implemented the switches for starting and desired floor first, then added more floors. From there, I used a testbench to ensure that my elevator was working with more floors and the inputs.

![Waveform Going up](images/waveform_up.PNG)
#####Figure 4: The simulation of the elevator going up. It starts in a reset state, then a high signal is applied to i-go and taken out, which starts the elevator going. The floors can be scene in o-floor-ones and o-floor-tens, which go all the way up to 15 (the 16th floor).

![Waveform Going Down](images/waveform_down.PNG)
#####Figure 5: The second half of the simulation, the elevator picked up the passengers at the starting location of floor 16, and takes them to the desired floor 1. 

This waveform demonstrates the elevators ability to go to all floors, how it will not change the starting or desired floors until i-go is pressed, and how the floors can be chosen using the switches input.

I also have included a state transition diagram for the added functionality.

![STD](images/std.PNG)
#####Figure 6:State Transition Diagram for additional functionality. X represents the desired floor input.

Basically, the elevator goes up if the desired floor is greater than the current, and goes down if its less than. The lights output a ones values and a tens value to display the current floor.


The last thing I added was the moving lights to indicate the direction of the elevator. I did this using only one, unmodified thunderbird-fsm for extra credit. I got it to work by creating a process to power the LEDs in the top level file. The process uses the output from the MooreElevator to see if its going up or down, then powers the thunderbird-fsm left or right accordingly. Also, because the thunderbird-fsm outputs a 3-bit signal, and I needed to power all 8 lights (4-bits), I had each output power two or three LEDs at a time. This is easily seen in the snippet of code below. 

![Code Snippet](images/clever_design.PNG)
#####Figure 7: Snippet of code showing how I used one thunderbird-fsm to power all the lights by concatenating signals.

### Answers to Lab Questions
1)	What do you need the synchronizer for (See Sections 3.5.4-3.5.5 of your text book)?  How many bits will you require to be “synchronized” (Hint:  See Figures 1-2)?

The synchronizer takes the asynchronous inputs from the human input switches and buttons (which is inherently asynchronous) and outputs a stable signal. Essentially, the synchronizer prevents the system from reaching an metastable state which would cause the system to crash. In this system, I must synchronize 5 bits for the required functionality, which is the total number of user input bits.6

2) What value for k_DIV do you need to produce a 2 Hz clock?

k_DIV = 12,500,000 (12.5e6) because that turns a 50 MHz clock into a 2 Hz clock by dividing the half of the clock cycle by 2.


3) Notice the Master Reset does not reset both the FSM and clock like it should. Why not? Hint: think about how the other resets work.

Because it holds the clock process, so the state of the Moore machine does not actually change.

4) Not applicable.

5) What did you learn?

I learned how to put several components into a top level file and chain inputs and outputs for each together. I also learned that I can assigned an output to a wider bit-width signal by concatenating the same bits over and over. 

### Feedback


Number of hours spent on Lab 3: ___11_____ (no points associated with this unless you leave it blank)

Suggestions to improve Lab 3 in future years: (use blank space below, or write in README)

I thought this was a good lab, I wasn't confused or lost at really any point through this. I would suggest making some of the additional functionality part of the extra credit, as there was a lot required for this lab and it took awhile to complete. 



### Documentation

C3C Ryer showed me how to use signals in the ElevatorController to track current floor and the desired location floor in order to compare them within the processes. He also told me that I could use two output signals from the ElevatorController to the thunderbird-fsm to indicate up/down.