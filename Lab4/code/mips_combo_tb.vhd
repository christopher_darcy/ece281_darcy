-- HDL Example 7.12 (MIPS TESTBENCH) on pg 438 of 
-- Digital Design and Computer Architecture, 2nd Edition
-- by Harris & Harris, 2013
-- 
-- This simple testbench tests a single-cycle MIPS processor.
-- The processor is reset and then run on a clock in order to execute
-- a desired machine code program loaded into instruction memory 
-- via a .dat file.
--
-- Last modified 4/2017 by Capt Phillip Warner to work with 
-- mips_combo.vhd file for ECE 281 Lab 4.

--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : mips_combo_tb.vhd
--| AUTHOR(S)     : C3C Christopher Darcy
--| CREATED       : 04/2017
--| DESCRIPTION   : This file implements a testbench for the mips_combo.vhd
--|						single cycle MIPS processor. 
--|  
--| DOCUMENTATION : None.
--|
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES : mips_combo.vhd
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, std_logic_arith, std_logic_signed,
--|						std_logic_unsigned, numeric_std
--|    Files     : None
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.all; use IEEE.NUMERIC_STD.all;

entity testbench is
end;

architecture test of testbench is
 component top
  port(clk, reset:     	 in STD_LOGIC;
     writedata, dataadr: inout STD_LOGIC_VECTOR(31 downto 0);
     memwrite:      	 inout STD_LOGIC);
 end component;
 signal writedata, dataadr:  STD_LOGIC_VECTOR(31 downto 0);
 signal clk, reset, memwrite: STD_LOGIC;
begin

 -- instantiate device to be tested
 dut: top port map(clk, reset, writedata, dataadr, memwrite);
 -- Generate clock with 10 ns period
 process begin
   clk <= '1';
   wait for 5 ns;
   clk <= '0';
   wait for 5 ns;
 end process;

 -- Generate reset for first two clock cycles
 process begin
   reset <= '1';
   wait for 22 ns;
   reset <= '0';
   wait;
	
	
 end process;

 
 -- Self check not required.  Must be edited to use.
 
 -- check that 7 gets written to address 84 at end of program
 -- process(clk) begin
   -- if (rising_edge(clk) and memwrite = '1') then
    -- if (to_integer(dataadr) = 84 and to_integer (writedata) = 7) then
     -- report "NO ERRORS: Simulation succeeded" severity failure;
    -- elsif (dataadr /= 80) then
     -- report "Simulation failed" severity failure;
    -- end if;
  -- end if;
 -- end process;

 end;