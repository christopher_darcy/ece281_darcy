#-------------------------------------------------------------------------------
# MIPS ICE - Part 1
# C3C Christopher Darcy, USAF / 10 Apr 2017 (Start Date) / 10 Apr 2017 (Completion Date)
#
# Purpose:  This program is counts up by 2 from 0-10, then down by 2 from 10-0.
# 
# Documentation: none 
#-------------------------------------------------------------------------------

	addi $t0, $0, 0		# x = 0
	addi $t1, $0, 10 	# $t1 = 10

loop_up:	
	addi $t0, $t0, 2		# x = x + 2
	beq $t0, $t1, loop_down	# if x =  $t1, jump to loop_down
	j loop_up
loop_down:
	subi $t0, $t0, 2	# x = x-2
	beq $t0, $0, loop_up	# if x = 0, jump to loop_up
	j loop_down
	
	
