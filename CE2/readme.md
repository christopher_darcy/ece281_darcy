# Computer Exercise #2

## By C3C Christopher Darcy

## Table of Contents 
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
 * [Code](#code)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
 * [Pseudocode](#pseudocode)
4. [Hardware schematic](#hardware-schematic)
5. [Well-formatted code](#well-formatted-code)
6. [Debugging](#debugging)
7. [Testing methodology or results](#testing-methodology-or-results)
8. [Answers to Lab Questions](#answers-to-lab-questions)
9. [Observations and Conclusions](#observations-and-conclusions)
10. [Feedback](#feedback)
11. [Documentation](#documentation)
 
### Objectives or Purpose 
The objective of Computer Exercise 2 is to design and implement a "Half Adder" circuit using Very High Speed Integrated Circuits (VHSIC) Hardware Description Language (VHDL). The learning objectives are to implement and test the logic design using VHDL and to gain additional experience with Git, Markdown, and Xilinx. These goals are accomplished through the design, implementation, and testing of the Half Adder circuit. Basically, the Half Adder is a circuit with two switches and two LEDs. When either of the switches were on (but not both), one LED would turn on; if both switches were on, then the other LED would be on.


### Preliminary design
The purpose of this Computer Exercise will be accomplished by following the instructional document provided. First, I had to create the project and add the required source code. Next, I had to add edit the implementation and architecture using VHDL. Examples of code were provided with the source and I had to add additional code modeling the example but for different ports and signals. I used VHDL to make the circuit described in the Objectives and Purpose section above. After coding the logic for the circuit, I had to test it to ensure it was correct. I relied upon what I remembered from Computer Exercise 1 to simulate the results. After i verified that the simulation results were correct, I implemented the design onto the Nexys2 Board and tested its correctness. 




#### Code:

There was a log of VHDL coding involved in this Computer Exercise. First, the code below creates the entities, basically the inputs and outputs.

![Entities Code](images/code_entity.PNG)
#####Figure 1: Code segment declaring entities.

Next, in the same file I had to code for the two gates. I wrote the code below that assigns the carry signal to o_Cout (an LED).
![And gate](images/code_assignments.PNG)
#####Figure 2: Segment of code assigning the AND gate.

The rest of the coding was done for the simulation testbench. First, I had to define the ports, which was the same syntax as the entities above, but instead they were called components. Then I had to declare signals, which consisted of assigning '0' to the components, but most of that code was already provided. Next, I had to map ports with the following code:
![Port Mapping](images/code_map_ports.PNG)
#####Figure 3: Code segment mapping ports.

The last step for the simulation is to implement a test plan of the four possible input combinations. I had to implement this test plan for CE1, thus the coding was pretty easy:

![Test Plan](images/code_test_plan.PNG)
#####Figure 4: Coding all the test cases for the simulation.

#####VHDL Header:
	--------------------------------------------------------------------
	-- Name:C3C Christopher Darcy
	-- Date: 22 Jan 2017
	-- Course:	ECE 281
	-- File:CE2/readme.md
	-- HW:	Computer Exercise 2
	--
	-- Purp: Design logic to illuminate an LED on a Nexys2 Board based on certain inputs.
	-- Solved by drawing a schematic and using Xilinx to run a simulation and download it on the board.
	--
	-- Doc:	None.
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	------------------------------------------------------------------------- 

	
### Software flow chart or algorithms
The logic for this circuit is pretty simple. An **AND** gate and a **XOR** gate operate concurrently to output the sum of two inputs. This is described by: **S** = **A ⊕ B**, **Cout** = **AB**.  The image below shows the schematic symbol and truth table for the Half Adder.

![Half Adder Flow](images/half_adder_flow.PNG)
#####Figure 5: The Half Adder schematic symbol and truth table. 



#### Pseudocode:
**A** and **B** turn on led1, **A** or **B** but not both turns on led2.




### Hardware schematic
Computer Exercise 2 using VHDL to implement the logic instead of creating a hardware schematic like Computer Exercise 1. However, the a hardware schematic was provided.

![Logic](images/logic.PNG)
##### Figure 6: The logic for the Half Adder circuit.




### Well-formatted code
Code is provided in the code folder in this repository. Additionally, important sections of code are included in the Code section above. 



### Debugging
My first problem was a syntax error when I used the Behavioral Check Syntax tool. it produced the following syntax errors.
![Syntax Errors](images/syntax_errors.PNG)
#####Figure 7: Syntax errors in Xilinx

The issue was really simple, I had a typo that referred to the o_Cout entity as i_Cout, which was easily fixed. 

Another error occurred when I tried to run the simulation. The Xilinx ISE produced the following error having to with ports:

![Port Errors](images/port_errors.PNG)
#####Figure 8: Errors with the ports when attempting to run the simulation

I had to go back and check all the code to try to find this issue, which took a few minutes but I was able to find the problem in this line on the simulation file:

![Port Error Cause](images/cause_of_port_error.PNG)
#####Figure 9: Typo that caused the error with ports.

As can be seen, the o_Cout component was declared as an input instead of an output. This was the typo which caused these errors and was easily fixed by declaring it as "out" instead of "in".  



### Testing methodology or results 

The first step was coding the circuit in VHDL. This code can be seen in the code folder and important sections are included above. 

![Simulation results](images/sim_output.PNG)
##### Figure 10: Shows the results of the simulation are correct.

The simulation output was correct, so the next step was to actually implement the logic onto the FPGA.I used Xilinx to generate a format that could actually be interpreted by the Nexys2 Board. I followed the process from Computer Exercise 1 to download the .bit file onto the Board and test it on there. After downloading this onto my board, I tested the results and the output was correct. The inputs on the board were from the switches H18 and G18 and the LEDs being used were LD0 and LD1. This all pictured in the demonstration of functionality below.
 

#####Figure 11: The pictures below depict both off on the left, and both switches up with the correct LD1 illuminated on the right.
![All Off](images/all_off.jpg)![Both On](images/both.jpg)

![Left](images/left.jpg)![Right](images/right.jpg)
#####Figure 12: The above pictures demonstrate how H18 (on the left) or G18 (on the right) are switched on, but not both, LD0 illuminates.



### Answers to Lab Questions
The only question asked in the Computer Exercise referred to the testbench and how coding for the test plan.

-How many test cases will you need?

There are only four test cases in this problem. 





### Observations and Conclusions
I was very successful in creating the Half Adder circuit. Aside from a few syntax errors and typos I had no major problems in creating this circuit. I relied heavily on my knowledge from Computer Exercise 1, and referred back to the instructional document from that exercise a few times. I was successful implementing this circuit using VHDL and have gained valuable experience with this programming language. Additionally, I gained more experience and familiarity with Git, Markdown, and Xilinx. 

### Feedback
Number of hours spent on CE2: 3.5    

Suggestions to improve CE2 in future years: (use blank space below)   
I thought CE2 was really good. I struggled a lot less than I did with CE1. My only suggestion is to provide less of the example code. Only having to complete one line in each section was trivial, and I think it would be beneficial to have to code more.




### Documentation
None.