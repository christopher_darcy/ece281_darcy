--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: top_nexys2_synthesis.vhd
-- /___/   /\     Timestamp: Sun Mar 19 22:15:51 2017
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm top_nexys2 -w -dir netgen/synthesis -ofmt vhdl -sim top_nexys2.ngc top_nexys2_synthesis.vhd 
-- Device	: xc3s500e-4-fg320
-- Input file	: top_nexys2.ngc
-- Output file	: C:\Users\C19Christopher.Darcy\Documents\Spring2017\ECE281\Lab3\netgen\synthesis\top_nexys2_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: top_nexys2
-- Xilinx	: C:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity top_nexys2 is
  port (
    i_clk_50MHz : in STD_LOGIC := 'X'; 
    o_SSEG_n : out STD_LOGIC_VECTOR ( 6 downto 0 ); 
    o_LED : out STD_LOGIC_VECTOR ( 7 downto 0 ); 
    o_AN_n : out STD_LOGIC_VECTOR ( 3 downto 0 ); 
    i_BTN : in STD_LOGIC_VECTOR ( 3 downto 0 ); 
    i_SW : in STD_LOGIC_VECTOR ( 7 downto 0 ) 
  );
end top_nexys2;

architecture Structure of top_nexys2 is
  signal MooreElevatorController_inst_floor_state_FSM_FFd1_0 : STD_LOGIC; 
  signal MooreElevatorController_inst_floor_state_FSM_FFd1_In : STD_LOGIC; 
  signal MooreElevatorController_inst_floor_state_FSM_FFd2_2 : STD_LOGIC; 
  signal MooreElevatorController_inst_floor_state_FSM_FFd2_In : STD_LOGIC; 
  signal N1 : STD_LOGIC; 
  signal c_Moore_OR : STD_LOGIC; 
  signal c_clk_OR : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_10_rt_40 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_11_rt_42 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_12_rt_44 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_13_rt_46 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_14_rt_48 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_15_rt_50 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_16_rt_52 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_17_rt_54 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_18_rt_56 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_19_rt_58 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_1_rt_60 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_20_rt_62 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_21_rt_64 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_22_rt_66 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_23_rt_68 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_24_rt_70 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_25_rt_72 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_26_rt_74 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_27_rt_76 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_28_rt_78 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_29_rt_80 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_2_rt_82 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_3_rt_84 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_4_rt_86 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_5_rt_88 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_6_rt_90 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_7_rt_92 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_8_rt_94 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_cy_9_rt_96 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_0 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_1 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_10 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_11 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_12 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_13 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_14 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_15 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_16 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_17 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_18 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_19 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_2 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_20 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_21 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_22 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_23 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_24 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_25 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_26 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_27 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_28 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_29 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_3 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_30 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_4 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_5 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_6 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_7 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_8 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_eqn_9 : STD_LOGIC; 
  signal clock_divider_inst_Mcount_f_count_xor_30_rt_129 : STD_LOGIC; 
  signal clock_divider_inst_f_clk_130 : STD_LOGIC; 
  signal clock_divider_inst_f_clk_cmp_eq0000 : STD_LOGIC; 
  signal clock_divider_inst_f_clk_not0001 : STD_LOGIC; 
  signal i_BTN_1_IBUF_182 : STD_LOGIC; 
  signal i_BTN_2_IBUF_183 : STD_LOGIC; 
  signal i_BTN_3_IBUF_184 : STD_LOGIC; 
  signal i_SW_1_IBUF_187 : STD_LOGIC; 
  signal i_SW_2_IBUF_188 : STD_LOGIC; 
  signal i_clk_50MHz_BUFGP_190 : STD_LOGIC; 
  signal o_SSEG_n_0_OBUF_207 : STD_LOGIC; 
  signal o_SSEG_n_1_OBUF_208 : STD_LOGIC; 
  signal o_SSEG_n_2_OBUF_209 : STD_LOGIC; 
  signal o_SSEG_n_3_OBUF_210 : STD_LOGIC; 
  signal o_SSEG_n_4_OBUF_211 : STD_LOGIC; 
  signal o_SSEG_n_5_OBUF_212 : STD_LOGIC; 
  signal synchronizer_inst_Mshreg_ff_d_1_213 : STD_LOGIC; 
  signal synchronizer_inst_Mshreg_ff_d_2_214 : STD_LOGIC; 
  signal synchronizer_inst_Mshreg_ff_d_3_215 : STD_LOGIC; 
  signal Result : STD_LOGIC_VECTOR ( 30 downto 0 ); 
  signal clock_divider_inst_Mcount_f_count_cy : STD_LOGIC_VECTOR ( 29 downto 0 ); 
  signal clock_divider_inst_Mcount_f_count_lut : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal clock_divider_inst_f_clk_cmp_eq0000_wg_cy : STD_LOGIC_VECTOR ( 6 downto 0 ); 
  signal clock_divider_inst_f_clk_cmp_eq0000_wg_lut : STD_LOGIC_VECTOR ( 7 downto 0 ); 
  signal clock_divider_inst_f_count : STD_LOGIC_VECTOR ( 30 downto 0 ); 
  signal synchronizer_inst_ff_d : STD_LOGIC_VECTOR ( 3 downto 1 ); 
begin
  XST_GND : GND
    port map (
      G => o_SSEG_n_5_OBUF_212
    );
  XST_VCC : VCC
    port map (
      P => N1
    );
  clock_divider_inst_f_clk : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CE => clock_divider_inst_f_clk_cmp_eq0000,
      CLR => c_clk_OR,
      D => clock_divider_inst_f_clk_not0001,
      Q => clock_divider_inst_f_clk_130
    );
  clock_divider_inst_f_count_0 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_0,
      Q => clock_divider_inst_f_count(0)
    );
  clock_divider_inst_f_count_1 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_1,
      Q => clock_divider_inst_f_count(1)
    );
  clock_divider_inst_f_count_2 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_2,
      Q => clock_divider_inst_f_count(2)
    );
  clock_divider_inst_f_count_3 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_3,
      Q => clock_divider_inst_f_count(3)
    );
  clock_divider_inst_f_count_4 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_4,
      Q => clock_divider_inst_f_count(4)
    );
  clock_divider_inst_f_count_5 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_5,
      Q => clock_divider_inst_f_count(5)
    );
  clock_divider_inst_f_count_6 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_6,
      Q => clock_divider_inst_f_count(6)
    );
  clock_divider_inst_f_count_7 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_7,
      Q => clock_divider_inst_f_count(7)
    );
  clock_divider_inst_f_count_8 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_8,
      Q => clock_divider_inst_f_count(8)
    );
  clock_divider_inst_f_count_9 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_9,
      Q => clock_divider_inst_f_count(9)
    );
  clock_divider_inst_f_count_10 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_10,
      Q => clock_divider_inst_f_count(10)
    );
  clock_divider_inst_f_count_11 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_11,
      Q => clock_divider_inst_f_count(11)
    );
  clock_divider_inst_f_count_12 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_12,
      Q => clock_divider_inst_f_count(12)
    );
  clock_divider_inst_f_count_13 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_13,
      Q => clock_divider_inst_f_count(13)
    );
  clock_divider_inst_f_count_14 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_14,
      Q => clock_divider_inst_f_count(14)
    );
  clock_divider_inst_f_count_15 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_15,
      Q => clock_divider_inst_f_count(15)
    );
  clock_divider_inst_f_count_16 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_16,
      Q => clock_divider_inst_f_count(16)
    );
  clock_divider_inst_f_count_17 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_17,
      Q => clock_divider_inst_f_count(17)
    );
  clock_divider_inst_f_count_18 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_18,
      Q => clock_divider_inst_f_count(18)
    );
  clock_divider_inst_f_count_19 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_19,
      Q => clock_divider_inst_f_count(19)
    );
  clock_divider_inst_f_count_20 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_20,
      Q => clock_divider_inst_f_count(20)
    );
  clock_divider_inst_f_count_21 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_21,
      Q => clock_divider_inst_f_count(21)
    );
  clock_divider_inst_f_count_22 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_22,
      Q => clock_divider_inst_f_count(22)
    );
  clock_divider_inst_f_count_23 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_23,
      Q => clock_divider_inst_f_count(23)
    );
  clock_divider_inst_f_count_24 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_24,
      Q => clock_divider_inst_f_count(24)
    );
  clock_divider_inst_f_count_25 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_25,
      Q => clock_divider_inst_f_count(25)
    );
  clock_divider_inst_f_count_26 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_26,
      Q => clock_divider_inst_f_count(26)
    );
  clock_divider_inst_f_count_27 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_27,
      Q => clock_divider_inst_f_count(27)
    );
  clock_divider_inst_f_count_28 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_28,
      Q => clock_divider_inst_f_count(28)
    );
  clock_divider_inst_f_count_29 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_29,
      Q => clock_divider_inst_f_count(29)
    );
  clock_divider_inst_f_count_30 : FDC
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      CLR => c_clk_OR,
      D => clock_divider_inst_Mcount_f_count_eqn_30,
      Q => clock_divider_inst_f_count(30)
    );
  clock_divider_inst_Mcount_f_count_cy_0_Q : MUXCY
    port map (
      CI => o_SSEG_n_5_OBUF_212,
      DI => N1,
      S => clock_divider_inst_Mcount_f_count_lut(0),
      O => clock_divider_inst_Mcount_f_count_cy(0)
    );
  clock_divider_inst_Mcount_f_count_xor_0_Q : XORCY
    port map (
      CI => o_SSEG_n_5_OBUF_212,
      LI => clock_divider_inst_Mcount_f_count_lut(0),
      O => Result(0)
    );
  clock_divider_inst_Mcount_f_count_cy_1_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(0),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_1_rt_60,
      O => clock_divider_inst_Mcount_f_count_cy(1)
    );
  clock_divider_inst_Mcount_f_count_xor_1_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(0),
      LI => clock_divider_inst_Mcount_f_count_cy_1_rt_60,
      O => Result(1)
    );
  clock_divider_inst_Mcount_f_count_cy_2_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(1),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_2_rt_82,
      O => clock_divider_inst_Mcount_f_count_cy(2)
    );
  clock_divider_inst_Mcount_f_count_xor_2_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(1),
      LI => clock_divider_inst_Mcount_f_count_cy_2_rt_82,
      O => Result(2)
    );
  clock_divider_inst_Mcount_f_count_cy_3_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(2),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_3_rt_84,
      O => clock_divider_inst_Mcount_f_count_cy(3)
    );
  clock_divider_inst_Mcount_f_count_xor_3_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(2),
      LI => clock_divider_inst_Mcount_f_count_cy_3_rt_84,
      O => Result(3)
    );
  clock_divider_inst_Mcount_f_count_cy_4_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(3),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_4_rt_86,
      O => clock_divider_inst_Mcount_f_count_cy(4)
    );
  clock_divider_inst_Mcount_f_count_xor_4_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(3),
      LI => clock_divider_inst_Mcount_f_count_cy_4_rt_86,
      O => Result(4)
    );
  clock_divider_inst_Mcount_f_count_cy_5_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(4),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_5_rt_88,
      O => clock_divider_inst_Mcount_f_count_cy(5)
    );
  clock_divider_inst_Mcount_f_count_xor_5_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(4),
      LI => clock_divider_inst_Mcount_f_count_cy_5_rt_88,
      O => Result(5)
    );
  clock_divider_inst_Mcount_f_count_cy_6_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(5),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_6_rt_90,
      O => clock_divider_inst_Mcount_f_count_cy(6)
    );
  clock_divider_inst_Mcount_f_count_xor_6_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(5),
      LI => clock_divider_inst_Mcount_f_count_cy_6_rt_90,
      O => Result(6)
    );
  clock_divider_inst_Mcount_f_count_cy_7_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(6),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_7_rt_92,
      O => clock_divider_inst_Mcount_f_count_cy(7)
    );
  clock_divider_inst_Mcount_f_count_xor_7_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(6),
      LI => clock_divider_inst_Mcount_f_count_cy_7_rt_92,
      O => Result(7)
    );
  clock_divider_inst_Mcount_f_count_cy_8_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(7),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_8_rt_94,
      O => clock_divider_inst_Mcount_f_count_cy(8)
    );
  clock_divider_inst_Mcount_f_count_xor_8_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(7),
      LI => clock_divider_inst_Mcount_f_count_cy_8_rt_94,
      O => Result(8)
    );
  clock_divider_inst_Mcount_f_count_cy_9_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(8),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_9_rt_96,
      O => clock_divider_inst_Mcount_f_count_cy(9)
    );
  clock_divider_inst_Mcount_f_count_xor_9_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(8),
      LI => clock_divider_inst_Mcount_f_count_cy_9_rt_96,
      O => Result(9)
    );
  clock_divider_inst_Mcount_f_count_cy_10_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(9),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_10_rt_40,
      O => clock_divider_inst_Mcount_f_count_cy(10)
    );
  clock_divider_inst_Mcount_f_count_xor_10_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(9),
      LI => clock_divider_inst_Mcount_f_count_cy_10_rt_40,
      O => Result(10)
    );
  clock_divider_inst_Mcount_f_count_cy_11_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(10),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_11_rt_42,
      O => clock_divider_inst_Mcount_f_count_cy(11)
    );
  clock_divider_inst_Mcount_f_count_xor_11_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(10),
      LI => clock_divider_inst_Mcount_f_count_cy_11_rt_42,
      O => Result(11)
    );
  clock_divider_inst_Mcount_f_count_cy_12_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(11),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_12_rt_44,
      O => clock_divider_inst_Mcount_f_count_cy(12)
    );
  clock_divider_inst_Mcount_f_count_xor_12_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(11),
      LI => clock_divider_inst_Mcount_f_count_cy_12_rt_44,
      O => Result(12)
    );
  clock_divider_inst_Mcount_f_count_cy_13_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(12),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_13_rt_46,
      O => clock_divider_inst_Mcount_f_count_cy(13)
    );
  clock_divider_inst_Mcount_f_count_xor_13_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(12),
      LI => clock_divider_inst_Mcount_f_count_cy_13_rt_46,
      O => Result(13)
    );
  clock_divider_inst_Mcount_f_count_cy_14_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(13),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_14_rt_48,
      O => clock_divider_inst_Mcount_f_count_cy(14)
    );
  clock_divider_inst_Mcount_f_count_xor_14_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(13),
      LI => clock_divider_inst_Mcount_f_count_cy_14_rt_48,
      O => Result(14)
    );
  clock_divider_inst_Mcount_f_count_cy_15_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(14),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_15_rt_50,
      O => clock_divider_inst_Mcount_f_count_cy(15)
    );
  clock_divider_inst_Mcount_f_count_xor_15_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(14),
      LI => clock_divider_inst_Mcount_f_count_cy_15_rt_50,
      O => Result(15)
    );
  clock_divider_inst_Mcount_f_count_cy_16_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(15),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_16_rt_52,
      O => clock_divider_inst_Mcount_f_count_cy(16)
    );
  clock_divider_inst_Mcount_f_count_xor_16_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(15),
      LI => clock_divider_inst_Mcount_f_count_cy_16_rt_52,
      O => Result(16)
    );
  clock_divider_inst_Mcount_f_count_cy_17_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(16),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_17_rt_54,
      O => clock_divider_inst_Mcount_f_count_cy(17)
    );
  clock_divider_inst_Mcount_f_count_xor_17_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(16),
      LI => clock_divider_inst_Mcount_f_count_cy_17_rt_54,
      O => Result(17)
    );
  clock_divider_inst_Mcount_f_count_cy_18_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(17),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_18_rt_56,
      O => clock_divider_inst_Mcount_f_count_cy(18)
    );
  clock_divider_inst_Mcount_f_count_xor_18_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(17),
      LI => clock_divider_inst_Mcount_f_count_cy_18_rt_56,
      O => Result(18)
    );
  clock_divider_inst_Mcount_f_count_cy_19_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(18),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_19_rt_58,
      O => clock_divider_inst_Mcount_f_count_cy(19)
    );
  clock_divider_inst_Mcount_f_count_xor_19_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(18),
      LI => clock_divider_inst_Mcount_f_count_cy_19_rt_58,
      O => Result(19)
    );
  clock_divider_inst_Mcount_f_count_cy_20_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(19),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_20_rt_62,
      O => clock_divider_inst_Mcount_f_count_cy(20)
    );
  clock_divider_inst_Mcount_f_count_xor_20_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(19),
      LI => clock_divider_inst_Mcount_f_count_cy_20_rt_62,
      O => Result(20)
    );
  clock_divider_inst_Mcount_f_count_cy_21_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(20),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_21_rt_64,
      O => clock_divider_inst_Mcount_f_count_cy(21)
    );
  clock_divider_inst_Mcount_f_count_xor_21_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(20),
      LI => clock_divider_inst_Mcount_f_count_cy_21_rt_64,
      O => Result(21)
    );
  clock_divider_inst_Mcount_f_count_cy_22_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(21),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_22_rt_66,
      O => clock_divider_inst_Mcount_f_count_cy(22)
    );
  clock_divider_inst_Mcount_f_count_xor_22_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(21),
      LI => clock_divider_inst_Mcount_f_count_cy_22_rt_66,
      O => Result(22)
    );
  clock_divider_inst_Mcount_f_count_cy_23_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(22),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_23_rt_68,
      O => clock_divider_inst_Mcount_f_count_cy(23)
    );
  clock_divider_inst_Mcount_f_count_xor_23_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(22),
      LI => clock_divider_inst_Mcount_f_count_cy_23_rt_68,
      O => Result(23)
    );
  clock_divider_inst_Mcount_f_count_cy_24_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(23),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_24_rt_70,
      O => clock_divider_inst_Mcount_f_count_cy(24)
    );
  clock_divider_inst_Mcount_f_count_xor_24_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(23),
      LI => clock_divider_inst_Mcount_f_count_cy_24_rt_70,
      O => Result(24)
    );
  clock_divider_inst_Mcount_f_count_cy_25_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(24),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_25_rt_72,
      O => clock_divider_inst_Mcount_f_count_cy(25)
    );
  clock_divider_inst_Mcount_f_count_xor_25_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(24),
      LI => clock_divider_inst_Mcount_f_count_cy_25_rt_72,
      O => Result(25)
    );
  clock_divider_inst_Mcount_f_count_cy_26_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(25),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_26_rt_74,
      O => clock_divider_inst_Mcount_f_count_cy(26)
    );
  clock_divider_inst_Mcount_f_count_xor_26_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(25),
      LI => clock_divider_inst_Mcount_f_count_cy_26_rt_74,
      O => Result(26)
    );
  clock_divider_inst_Mcount_f_count_cy_27_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(26),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_27_rt_76,
      O => clock_divider_inst_Mcount_f_count_cy(27)
    );
  clock_divider_inst_Mcount_f_count_xor_27_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(26),
      LI => clock_divider_inst_Mcount_f_count_cy_27_rt_76,
      O => Result(27)
    );
  clock_divider_inst_Mcount_f_count_cy_28_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(27),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_28_rt_78,
      O => clock_divider_inst_Mcount_f_count_cy(28)
    );
  clock_divider_inst_Mcount_f_count_xor_28_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(27),
      LI => clock_divider_inst_Mcount_f_count_cy_28_rt_78,
      O => Result(28)
    );
  clock_divider_inst_Mcount_f_count_cy_29_Q : MUXCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(28),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_Mcount_f_count_cy_29_rt_80,
      O => clock_divider_inst_Mcount_f_count_cy(29)
    );
  clock_divider_inst_Mcount_f_count_xor_29_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(28),
      LI => clock_divider_inst_Mcount_f_count_cy_29_rt_80,
      O => Result(29)
    );
  clock_divider_inst_Mcount_f_count_xor_30_Q : XORCY
    port map (
      CI => clock_divider_inst_Mcount_f_count_cy(29),
      LI => clock_divider_inst_Mcount_f_count_xor_30_rt_129,
      O => Result(30)
    );
  MooreElevatorController_inst_floor_state_FSM_FFd1 : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => clock_divider_inst_f_clk_130,
      D => MooreElevatorController_inst_floor_state_FSM_FFd1_In,
      R => c_Moore_OR,
      Q => MooreElevatorController_inst_floor_state_FSM_FFd1_0
    );
  MooreElevatorController_inst_floor_state_FSM_FFd2 : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => clock_divider_inst_f_clk_130,
      D => MooreElevatorController_inst_floor_state_FSM_FFd2_In,
      R => c_Moore_OR,
      Q => MooreElevatorController_inst_floor_state_FSM_FFd2_2
    );
  clock_divider_inst_f_clk_cmp_eq0000_wg_lut_0_Q : LUT3
    generic map(
      INIT => X"01"
    )
    port map (
      I0 => clock_divider_inst_f_count(8),
      I1 => clock_divider_inst_f_count(7),
      I2 => clock_divider_inst_f_count(9),
      O => clock_divider_inst_f_clk_cmp_eq0000_wg_lut(0)
    );
  clock_divider_inst_f_clk_cmp_eq0000_wg_cy_0_Q : MUXCY
    port map (
      CI => N1,
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_f_clk_cmp_eq0000_wg_lut(0),
      O => clock_divider_inst_f_clk_cmp_eq0000_wg_cy(0)
    );
  clock_divider_inst_f_clk_cmp_eq0000_wg_lut_1_Q : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => clock_divider_inst_f_count(10),
      I1 => clock_divider_inst_f_count(11),
      I2 => clock_divider_inst_f_count(6),
      I3 => clock_divider_inst_f_count(12),
      O => clock_divider_inst_f_clk_cmp_eq0000_wg_lut(1)
    );
  clock_divider_inst_f_clk_cmp_eq0000_wg_cy_1_Q : MUXCY
    port map (
      CI => clock_divider_inst_f_clk_cmp_eq0000_wg_cy(0),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_f_clk_cmp_eq0000_wg_lut(1),
      O => clock_divider_inst_f_clk_cmp_eq0000_wg_cy(1)
    );
  clock_divider_inst_f_clk_cmp_eq0000_wg_lut_2_Q : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => clock_divider_inst_f_count(13),
      I1 => clock_divider_inst_f_count(14),
      I2 => clock_divider_inst_f_count(5),
      I3 => clock_divider_inst_f_count(15),
      O => clock_divider_inst_f_clk_cmp_eq0000_wg_lut(2)
    );
  clock_divider_inst_f_clk_cmp_eq0000_wg_cy_2_Q : MUXCY
    port map (
      CI => clock_divider_inst_f_clk_cmp_eq0000_wg_cy(1),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_f_clk_cmp_eq0000_wg_lut(2),
      O => clock_divider_inst_f_clk_cmp_eq0000_wg_cy(2)
    );
  clock_divider_inst_f_clk_cmp_eq0000_wg_lut_3_Q : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => clock_divider_inst_f_count(16),
      I1 => clock_divider_inst_f_count(17),
      I2 => clock_divider_inst_f_count(4),
      I3 => clock_divider_inst_f_count(18),
      O => clock_divider_inst_f_clk_cmp_eq0000_wg_lut(3)
    );
  clock_divider_inst_f_clk_cmp_eq0000_wg_cy_3_Q : MUXCY
    port map (
      CI => clock_divider_inst_f_clk_cmp_eq0000_wg_cy(2),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_f_clk_cmp_eq0000_wg_lut(3),
      O => clock_divider_inst_f_clk_cmp_eq0000_wg_cy(3)
    );
  clock_divider_inst_f_clk_cmp_eq0000_wg_lut_4_Q : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => clock_divider_inst_f_count(19),
      I1 => clock_divider_inst_f_count(20),
      I2 => clock_divider_inst_f_count(3),
      I3 => clock_divider_inst_f_count(21),
      O => clock_divider_inst_f_clk_cmp_eq0000_wg_lut(4)
    );
  clock_divider_inst_f_clk_cmp_eq0000_wg_cy_4_Q : MUXCY
    port map (
      CI => clock_divider_inst_f_clk_cmp_eq0000_wg_cy(3),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_f_clk_cmp_eq0000_wg_lut(4),
      O => clock_divider_inst_f_clk_cmp_eq0000_wg_cy(4)
    );
  clock_divider_inst_f_clk_cmp_eq0000_wg_lut_5_Q : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => clock_divider_inst_f_count(22),
      I1 => clock_divider_inst_f_count(23),
      I2 => clock_divider_inst_f_count(2),
      I3 => clock_divider_inst_f_count(24),
      O => clock_divider_inst_f_clk_cmp_eq0000_wg_lut(5)
    );
  clock_divider_inst_f_clk_cmp_eq0000_wg_cy_5_Q : MUXCY
    port map (
      CI => clock_divider_inst_f_clk_cmp_eq0000_wg_cy(4),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_f_clk_cmp_eq0000_wg_lut(5),
      O => clock_divider_inst_f_clk_cmp_eq0000_wg_cy(5)
    );
  clock_divider_inst_f_clk_cmp_eq0000_wg_lut_6_Q : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => clock_divider_inst_f_count(25),
      I1 => clock_divider_inst_f_count(26),
      I2 => clock_divider_inst_f_count(1),
      I3 => clock_divider_inst_f_count(27),
      O => clock_divider_inst_f_clk_cmp_eq0000_wg_lut(6)
    );
  clock_divider_inst_f_clk_cmp_eq0000_wg_cy_6_Q : MUXCY
    port map (
      CI => clock_divider_inst_f_clk_cmp_eq0000_wg_cy(5),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_f_clk_cmp_eq0000_wg_lut(6),
      O => clock_divider_inst_f_clk_cmp_eq0000_wg_cy(6)
    );
  clock_divider_inst_f_clk_cmp_eq0000_wg_lut_7_Q : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => clock_divider_inst_f_count(30),
      I1 => clock_divider_inst_f_count(28),
      I2 => clock_divider_inst_f_count(0),
      I3 => clock_divider_inst_f_count(29),
      O => clock_divider_inst_f_clk_cmp_eq0000_wg_lut(7)
    );
  clock_divider_inst_f_clk_cmp_eq0000_wg_cy_7_Q : MUXCY
    port map (
      CI => clock_divider_inst_f_clk_cmp_eq0000_wg_cy(6),
      DI => o_SSEG_n_5_OBUF_212,
      S => clock_divider_inst_f_clk_cmp_eq0000_wg_lut(7),
      O => clock_divider_inst_f_clk_cmp_eq0000
    );
  c_clk_OR1 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => synchronizer_inst_ff_d(3),
      I1 => synchronizer_inst_ff_d(1),
      O => c_clk_OR
    );
  c_Moore_OR1 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => synchronizer_inst_ff_d(3),
      I1 => synchronizer_inst_ff_d(2),
      O => c_Moore_OR
    );
  sevenSegDecoder_inst_c_Sg_or00001 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => MooreElevatorController_inst_floor_state_FSM_FFd2_2,
      I1 => MooreElevatorController_inst_floor_state_FSM_FFd1_0,
      O => o_SSEG_n_0_OBUF_207
    );
  sevenSegDecoder_inst_c_Sf_or00001 : LUT2
    generic map(
      INIT => X"B"
    )
    port map (
      I0 => MooreElevatorController_inst_floor_state_FSM_FFd2_2,
      I1 => MooreElevatorController_inst_floor_state_FSM_FFd1_0,
      O => o_SSEG_n_1_OBUF_208
    );
  sevenSegDecoder_inst_c_Se_or00001 : LUT2
    generic map(
      INIT => X"B"
    )
    port map (
      I0 => MooreElevatorController_inst_floor_state_FSM_FFd1_0,
      I1 => MooreElevatorController_inst_floor_state_FSM_FFd2_2,
      O => o_SSEG_n_2_OBUF_209
    );
  sevenSegDecoder_inst_c_Sc_cmp_eq00001 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => MooreElevatorController_inst_floor_state_FSM_FFd2_2,
      I1 => MooreElevatorController_inst_floor_state_FSM_FFd1_0,
      O => o_SSEG_n_4_OBUF_211
    );
  MooreElevatorController_inst_floor_state_FSM_FFd1_In1 : LUT4
    generic map(
      INIT => X"DC8C"
    )
    port map (
      I0 => i_SW_2_IBUF_188,
      I1 => MooreElevatorController_inst_floor_state_FSM_FFd1_0,
      I2 => MooreElevatorController_inst_floor_state_FSM_FFd2_2,
      I3 => i_SW_1_IBUF_187,
      O => MooreElevatorController_inst_floor_state_FSM_FFd1_In
    );
  MooreElevatorController_inst_floor_state_FSM_FFd2_In1 : LUT4
    generic map(
      INIT => X"BE14"
    )
    port map (
      I0 => i_SW_2_IBUF_188,
      I1 => i_SW_1_IBUF_187,
      I2 => MooreElevatorController_inst_floor_state_FSM_FFd1_0,
      I3 => MooreElevatorController_inst_floor_state_FSM_FFd2_2,
      O => MooreElevatorController_inst_floor_state_FSM_FFd2_In
    );
  clock_divider_inst_Mcount_f_count_eqn_91 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(9),
      O => clock_divider_inst_Mcount_f_count_eqn_9
    );
  clock_divider_inst_Mcount_f_count_eqn_81 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(8),
      I1 => clock_divider_inst_f_clk_cmp_eq0000,
      O => clock_divider_inst_Mcount_f_count_eqn_8
    );
  clock_divider_inst_Mcount_f_count_eqn_71 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(7),
      I1 => clock_divider_inst_f_clk_cmp_eq0000,
      O => clock_divider_inst_Mcount_f_count_eqn_7
    );
  clock_divider_inst_Mcount_f_count_eqn_61 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(6),
      I1 => clock_divider_inst_f_clk_cmp_eq0000,
      O => clock_divider_inst_Mcount_f_count_eqn_6
    );
  clock_divider_inst_Mcount_f_count_eqn_51 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(5),
      I1 => clock_divider_inst_f_clk_cmp_eq0000,
      O => clock_divider_inst_Mcount_f_count_eqn_5
    );
  clock_divider_inst_Mcount_f_count_eqn_41 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(4),
      I1 => clock_divider_inst_f_clk_cmp_eq0000,
      O => clock_divider_inst_Mcount_f_count_eqn_4
    );
  clock_divider_inst_Mcount_f_count_eqn_31 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(3),
      I1 => clock_divider_inst_f_clk_cmp_eq0000,
      O => clock_divider_inst_Mcount_f_count_eqn_3
    );
  clock_divider_inst_Mcount_f_count_eqn_210 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(2),
      I1 => clock_divider_inst_f_clk_cmp_eq0000,
      O => clock_divider_inst_Mcount_f_count_eqn_2
    );
  clock_divider_inst_Mcount_f_count_eqn_110 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(1),
      I1 => clock_divider_inst_f_clk_cmp_eq0000,
      O => clock_divider_inst_Mcount_f_count_eqn_1
    );
  clock_divider_inst_Mcount_f_count_eqn_01 : LUT2
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => Result(0),
      I1 => clock_divider_inst_f_clk_cmp_eq0000,
      O => clock_divider_inst_Mcount_f_count_eqn_0
    );
  clock_divider_inst_Mcount_f_count_eqn_101 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(10),
      O => clock_divider_inst_Mcount_f_count_eqn_10
    );
  clock_divider_inst_Mcount_f_count_eqn_111 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(11),
      O => clock_divider_inst_Mcount_f_count_eqn_11
    );
  clock_divider_inst_Mcount_f_count_eqn_121 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(12),
      O => clock_divider_inst_Mcount_f_count_eqn_12
    );
  clock_divider_inst_Mcount_f_count_eqn_131 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(13),
      O => clock_divider_inst_Mcount_f_count_eqn_13
    );
  clock_divider_inst_Mcount_f_count_eqn_141 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(14),
      O => clock_divider_inst_Mcount_f_count_eqn_14
    );
  clock_divider_inst_Mcount_f_count_eqn_151 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(15),
      O => clock_divider_inst_Mcount_f_count_eqn_15
    );
  clock_divider_inst_Mcount_f_count_eqn_161 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(16),
      O => clock_divider_inst_Mcount_f_count_eqn_16
    );
  clock_divider_inst_Mcount_f_count_eqn_171 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(17),
      O => clock_divider_inst_Mcount_f_count_eqn_17
    );
  clock_divider_inst_Mcount_f_count_eqn_181 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(18),
      O => clock_divider_inst_Mcount_f_count_eqn_18
    );
  clock_divider_inst_Mcount_f_count_eqn_191 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(19),
      O => clock_divider_inst_Mcount_f_count_eqn_19
    );
  clock_divider_inst_Mcount_f_count_eqn_201 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(20),
      O => clock_divider_inst_Mcount_f_count_eqn_20
    );
  clock_divider_inst_Mcount_f_count_eqn_211 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(21),
      O => clock_divider_inst_Mcount_f_count_eqn_21
    );
  clock_divider_inst_Mcount_f_count_eqn_221 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(22),
      O => clock_divider_inst_Mcount_f_count_eqn_22
    );
  clock_divider_inst_Mcount_f_count_eqn_231 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(23),
      O => clock_divider_inst_Mcount_f_count_eqn_23
    );
  clock_divider_inst_Mcount_f_count_eqn_241 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(24),
      O => clock_divider_inst_Mcount_f_count_eqn_24
    );
  clock_divider_inst_Mcount_f_count_eqn_251 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(25),
      O => clock_divider_inst_Mcount_f_count_eqn_25
    );
  clock_divider_inst_Mcount_f_count_eqn_261 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(26),
      O => clock_divider_inst_Mcount_f_count_eqn_26
    );
  clock_divider_inst_Mcount_f_count_eqn_271 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(27),
      O => clock_divider_inst_Mcount_f_count_eqn_27
    );
  clock_divider_inst_Mcount_f_count_eqn_281 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(28),
      O => clock_divider_inst_Mcount_f_count_eqn_28
    );
  clock_divider_inst_Mcount_f_count_eqn_291 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(29),
      O => clock_divider_inst_Mcount_f_count_eqn_29
    );
  clock_divider_inst_Mcount_f_count_eqn_301 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => clock_divider_inst_f_clk_cmp_eq0000,
      I1 => Result(30),
      O => clock_divider_inst_Mcount_f_count_eqn_30
    );
  i_BTN_3_IBUF : IBUF
    port map (
      I => i_BTN(3),
      O => i_BTN_3_IBUF_184
    );
  i_BTN_2_IBUF : IBUF
    port map (
      I => i_BTN(2),
      O => i_BTN_2_IBUF_183
    );
  i_BTN_1_IBUF : IBUF
    port map (
      I => i_BTN(1),
      O => i_BTN_1_IBUF_182
    );
  i_SW_2_IBUF : IBUF
    port map (
      I => i_SW(2),
      O => i_SW_2_IBUF_188
    );
  i_SW_1_IBUF : IBUF
    port map (
      I => i_SW(1),
      O => i_SW_1_IBUF_187
    );
  o_SSEG_n_6_OBUF : OBUF
    port map (
      I => o_SSEG_n_3_OBUF_210,
      O => o_SSEG_n(6)
    );
  o_SSEG_n_5_OBUF : OBUF
    port map (
      I => o_SSEG_n_5_OBUF_212,
      O => o_SSEG_n(5)
    );
  o_SSEG_n_4_OBUF : OBUF
    port map (
      I => o_SSEG_n_4_OBUF_211,
      O => o_SSEG_n(4)
    );
  o_SSEG_n_3_OBUF : OBUF
    port map (
      I => o_SSEG_n_3_OBUF_210,
      O => o_SSEG_n(3)
    );
  o_SSEG_n_2_OBUF : OBUF
    port map (
      I => o_SSEG_n_2_OBUF_209,
      O => o_SSEG_n(2)
    );
  o_SSEG_n_1_OBUF : OBUF
    port map (
      I => o_SSEG_n_1_OBUF_208,
      O => o_SSEG_n(1)
    );
  o_SSEG_n_0_OBUF : OBUF
    port map (
      I => o_SSEG_n_0_OBUF_207,
      O => o_SSEG_n(0)
    );
  o_LED_7_OBUF : OBUF
    port map (
      I => o_SSEG_n_5_OBUF_212,
      O => o_LED(7)
    );
  o_LED_6_OBUF : OBUF
    port map (
      I => o_SSEG_n_5_OBUF_212,
      O => o_LED(6)
    );
  o_LED_5_OBUF : OBUF
    port map (
      I => o_SSEG_n_5_OBUF_212,
      O => o_LED(5)
    );
  o_LED_4_OBUF : OBUF
    port map (
      I => o_SSEG_n_5_OBUF_212,
      O => o_LED(4)
    );
  o_LED_3_OBUF : OBUF
    port map (
      I => o_SSEG_n_5_OBUF_212,
      O => o_LED(3)
    );
  o_LED_2_OBUF : OBUF
    port map (
      I => o_SSEG_n_5_OBUF_212,
      O => o_LED(2)
    );
  o_LED_1_OBUF : OBUF
    port map (
      I => o_SSEG_n_5_OBUF_212,
      O => o_LED(1)
    );
  o_LED_0_OBUF : OBUF
    port map (
      I => o_SSEG_n_5_OBUF_212,
      O => o_LED(0)
    );
  o_AN_n_1_OBUF : OBUF
    port map (
      I => o_SSEG_n_5_OBUF_212,
      O => o_AN_n(1)
    );
  clock_divider_inst_Mcount_f_count_cy_1_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(1),
      O => clock_divider_inst_Mcount_f_count_cy_1_rt_60
    );
  clock_divider_inst_Mcount_f_count_cy_2_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(2),
      O => clock_divider_inst_Mcount_f_count_cy_2_rt_82
    );
  clock_divider_inst_Mcount_f_count_cy_3_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(3),
      O => clock_divider_inst_Mcount_f_count_cy_3_rt_84
    );
  clock_divider_inst_Mcount_f_count_cy_4_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(4),
      O => clock_divider_inst_Mcount_f_count_cy_4_rt_86
    );
  clock_divider_inst_Mcount_f_count_cy_5_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(5),
      O => clock_divider_inst_Mcount_f_count_cy_5_rt_88
    );
  clock_divider_inst_Mcount_f_count_cy_6_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(6),
      O => clock_divider_inst_Mcount_f_count_cy_6_rt_90
    );
  clock_divider_inst_Mcount_f_count_cy_7_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(7),
      O => clock_divider_inst_Mcount_f_count_cy_7_rt_92
    );
  clock_divider_inst_Mcount_f_count_cy_8_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(8),
      O => clock_divider_inst_Mcount_f_count_cy_8_rt_94
    );
  clock_divider_inst_Mcount_f_count_cy_9_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(9),
      O => clock_divider_inst_Mcount_f_count_cy_9_rt_96
    );
  clock_divider_inst_Mcount_f_count_cy_10_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(10),
      O => clock_divider_inst_Mcount_f_count_cy_10_rt_40
    );
  clock_divider_inst_Mcount_f_count_cy_11_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(11),
      O => clock_divider_inst_Mcount_f_count_cy_11_rt_42
    );
  clock_divider_inst_Mcount_f_count_cy_12_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(12),
      O => clock_divider_inst_Mcount_f_count_cy_12_rt_44
    );
  clock_divider_inst_Mcount_f_count_cy_13_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(13),
      O => clock_divider_inst_Mcount_f_count_cy_13_rt_46
    );
  clock_divider_inst_Mcount_f_count_cy_14_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(14),
      O => clock_divider_inst_Mcount_f_count_cy_14_rt_48
    );
  clock_divider_inst_Mcount_f_count_cy_15_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(15),
      O => clock_divider_inst_Mcount_f_count_cy_15_rt_50
    );
  clock_divider_inst_Mcount_f_count_cy_16_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(16),
      O => clock_divider_inst_Mcount_f_count_cy_16_rt_52
    );
  clock_divider_inst_Mcount_f_count_cy_17_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(17),
      O => clock_divider_inst_Mcount_f_count_cy_17_rt_54
    );
  clock_divider_inst_Mcount_f_count_cy_18_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(18),
      O => clock_divider_inst_Mcount_f_count_cy_18_rt_56
    );
  clock_divider_inst_Mcount_f_count_cy_19_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(19),
      O => clock_divider_inst_Mcount_f_count_cy_19_rt_58
    );
  clock_divider_inst_Mcount_f_count_cy_20_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(20),
      O => clock_divider_inst_Mcount_f_count_cy_20_rt_62
    );
  clock_divider_inst_Mcount_f_count_cy_21_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(21),
      O => clock_divider_inst_Mcount_f_count_cy_21_rt_64
    );
  clock_divider_inst_Mcount_f_count_cy_22_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(22),
      O => clock_divider_inst_Mcount_f_count_cy_22_rt_66
    );
  clock_divider_inst_Mcount_f_count_cy_23_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(23),
      O => clock_divider_inst_Mcount_f_count_cy_23_rt_68
    );
  clock_divider_inst_Mcount_f_count_cy_24_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(24),
      O => clock_divider_inst_Mcount_f_count_cy_24_rt_70
    );
  clock_divider_inst_Mcount_f_count_cy_25_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(25),
      O => clock_divider_inst_Mcount_f_count_cy_25_rt_72
    );
  clock_divider_inst_Mcount_f_count_cy_26_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(26),
      O => clock_divider_inst_Mcount_f_count_cy_26_rt_74
    );
  clock_divider_inst_Mcount_f_count_cy_27_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(27),
      O => clock_divider_inst_Mcount_f_count_cy_27_rt_76
    );
  clock_divider_inst_Mcount_f_count_cy_28_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(28),
      O => clock_divider_inst_Mcount_f_count_cy_28_rt_78
    );
  clock_divider_inst_Mcount_f_count_cy_29_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(29),
      O => clock_divider_inst_Mcount_f_count_cy_29_rt_80
    );
  clock_divider_inst_Mcount_f_count_xor_30_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => clock_divider_inst_f_count(30),
      O => clock_divider_inst_Mcount_f_count_xor_30_rt_129
    );
  i_clk_50MHz_BUFGP : BUFGP
    port map (
      I => i_clk_50MHz,
      O => i_clk_50MHz_BUFGP_190
    );
  clock_divider_inst_Mcount_f_count_lut_0_INV_0 : INV
    port map (
      I => clock_divider_inst_f_count(0),
      O => clock_divider_inst_Mcount_f_count_lut(0)
    );
  o_SSEG_n_3_1_INV_0 : INV
    port map (
      I => MooreElevatorController_inst_floor_state_FSM_FFd2_2,
      O => o_SSEG_n_3_OBUF_210
    );
  clock_divider_inst_f_clk_not00011_INV_0 : INV
    port map (
      I => clock_divider_inst_f_clk_130,
      O => clock_divider_inst_f_clk_not0001
    );
  synchronizer_inst_Mshreg_ff_d_1 : SRL16
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => o_SSEG_n_5_OBUF_212,
      A1 => o_SSEG_n_5_OBUF_212,
      A2 => o_SSEG_n_5_OBUF_212,
      A3 => o_SSEG_n_5_OBUF_212,
      CLK => i_clk_50MHz_BUFGP_190,
      D => i_BTN_1_IBUF_182,
      Q => synchronizer_inst_Mshreg_ff_d_1_213
    );
  synchronizer_inst_ff_d_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      D => synchronizer_inst_Mshreg_ff_d_1_213,
      Q => synchronizer_inst_ff_d(1)
    );
  synchronizer_inst_Mshreg_ff_d_3 : SRL16
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => o_SSEG_n_5_OBUF_212,
      A1 => o_SSEG_n_5_OBUF_212,
      A2 => o_SSEG_n_5_OBUF_212,
      A3 => o_SSEG_n_5_OBUF_212,
      CLK => i_clk_50MHz_BUFGP_190,
      D => i_BTN_3_IBUF_184,
      Q => synchronizer_inst_Mshreg_ff_d_3_215
    );
  synchronizer_inst_ff_d_3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      D => synchronizer_inst_Mshreg_ff_d_3_215,
      Q => synchronizer_inst_ff_d(3)
    );
  synchronizer_inst_Mshreg_ff_d_2 : SRL16
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => o_SSEG_n_5_OBUF_212,
      A1 => o_SSEG_n_5_OBUF_212,
      A2 => o_SSEG_n_5_OBUF_212,
      A3 => o_SSEG_n_5_OBUF_212,
      CLK => i_clk_50MHz_BUFGP_190,
      D => i_BTN_2_IBUF_183,
      Q => synchronizer_inst_Mshreg_ff_d_2_214
    );
  synchronizer_inst_ff_d_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => i_clk_50MHz_BUFGP_190,
      D => synchronizer_inst_Mshreg_ff_d_2_214,
      Q => synchronizer_inst_ff_d(2)
    );

end Structure;

