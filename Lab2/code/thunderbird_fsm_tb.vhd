--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : thunderbird_fsm_tb.vhd (TEST BENCH)
--| AUTHOR(S)     : C3c Chris Darcy
--| CREATED       : 03/01/2017
--| DESCRIPTION   : Testbench file for Lab2 Thunderbird taillights FSM simulation.
--|
--| DOCUMENTATION : 
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES : thunderbird_fsm.vhd
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : LIST ANY DEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library unisim;
  use UNISIM.Vcomponents.ALL;
  
entity thunderbird_fsm_tb is
end thunderbird_fsm_tb;

architecture test_bench of thunderbird_fsm_tb is 
	
  -- declaration of the thunderbird_fsm (top level entity)
  component thunderbird_fsm is
    port(
         i_clk, i_reset : in std_logic;
			i_left, i_right : in std_logic;
			o_lights_L : out std_logic_vector(2 downto 0);
			o_lights_R : out std_logic_vector(2 downto 0)
    );	
  end component;

  --inputs
  signal c_clk : std_logic := '0';
  signal c_reset : std_logic := '0';
  signal c_left : std_logic := '0';
  signal c_right : std_logic := '0';
  
  -- outputs
  signal c_lights_L : std_logic_vector(2 downto 0);
  signal c_lights_R : std_logic_vector(2 downto 0);
  
  
  -- clock period
  constant clk_period : time := 10 ns;
  
  
begin
	-- PORT MAPS ----------------------------------------

	-- map ports for any component instances (port mapping is like wiring hardware)
	-- maps inputs and outputs to signals
	uut_inst : thunderbird_fsm port map (
		i_clk => c_clk,
		i_reset => c_reset,
		i_left => c_left,
		i_right => c_right,
		o_lights_L => c_lights_L,
		o_lights_R => c_lights_R
	);


	-- clock process
	clk_process : process
	begin
		c_clk <= '0';
		wait for clk_period/2;
		c_clk <= '1';
		wait for clk_period/2;
	end process;
	
	
	-- simulation process
	sim_process : process
	begin 
		-- start in known reset state
		c_reset <= '1';
			wait for clk_period;
		-- left blinker
		c_left <= '1'; c_right <= '0'; c_reset <= '0'; 
			wait for clk_period*3;  -- wait 3 clk cycles so it goes through each left blinker state
		-- same thing for right blinker
		c_left <= '0'; c_right <= '1'; c_reset <= '0'; 
			wait for clk_period*3;
		-- hazard test
		c_left <= '1'; c_right <= '1'; c_reset <= '0'; 
			wait for clk_period*3;
		-- reset during the hazards
		c_left <= '1'; c_right <= '1'; c_reset <= '1'; 
			wait for clk_period*3;
		-- reset during a blinker (all lights should remain off from previous test)
		c_left <= '1'; c_right <= '0'; c_reset <= '1'; 
			wait for clk_period*3;
		wait;
	end process;

	-----------------------------------------------------	
	
end test_bench;
