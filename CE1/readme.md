# Computer Exercise #1 

## By C3C Christopher Darcy

## Table of Contents 
1. [Objectives or Purpose](#objectives-or-purpose)
2. [Preliminary Design](#preliminary-design)
 * [Code](#code)
3. [Software flow chart or algorithms](#software-flow-chart-or-algorithms)
 * [Pseudocode](#pseudocode)
4. [Hardware schematic](#hardware-schematic)
5. [Well-formatted code](#well-formatted-code)
6. [Debugging](#debugging)
7. [Testing methodology or results](#testing-methodology-or-results)
8. [Answers to Lab Questions](#answers-to-lab-questions)
9. [Observations and Conclusions](#observations-and-conclusions)
10. [Feedback](#feedback)
11. [Documentation](#documentation)
 
### Objectives or Purpose 
The goal of Computer Exercise 1 is to set up and use the required software, learn the functionality of Git, and to understand how to create and use a lab notebook. Specifically, the task is to create a design that uses simple logic to illuminate an LED on the NEXYS2 Development Board. Creating this project develops an understanding of how to use Xilinx, Git with Bitbucket, MarkdownPad 2 to edit markdown files, as well as how to connect and operate the Development Board.


### Preliminary design
The plan of attack for this project is to follow the guidelines provided in the Computer Exercise 1 document from the Sharepoint. I skipped the portion regarding setting up a Bitbucket account and repository because that was covered in class the first two lessons. Otherwise, the instruction document provides step by step instructions for downloading, licensing, and installing software, and creating a schematic and workbench in Xilinx.

#### Code:

The only code we have to do was to code all the possible inputs for the simulation in Xilinx. There are 8 different possible combinations of input, which are all accounted for in the input below.

![Sim Code](images/code.PNG)

#####VHDL Header:
	--------------------------------------------------------------------
	-- Name:C3C Christopher Darcy
	-- Date: 10 Jan 2017
	-- Course:	ECE 281
	-- File:CE1/readme.md
	-- HW:	Computer Exercise 1
	--
	-- Purp: Design logic to illuminate an LED on a Nexys2 Board based on certain inputs.
	-- Solved by drawing a schematic and using Xilinx to run a simulation and download it on the board.
	--
	-- Doc:	None.
	--
	-- Academic Integrity Statement: I certify that, while others may have 
	-- assisted me in brain storming, debugging and validating this program, 
	-- the program itself is my own work. I understand that submitting code 
	-- which is the work of other individuals is a violation of the honor   
	-- code.  I also understand that if I knowingly give my original work to 
	-- another individual is also a violation of the honor code. 
	------------------------------------------------------------------------- 

	
### Software flow chart or algorithms
The logic for the design is AB + BC = F, where A, B, and C, are input switches and F is an output. Below is a truth table for this logic:

| A | B | C | Expected F |  Sim F  |  actual F  |
|:-:|:-:|:-:|:----------:|:-------:|:----------:|
| 0 | 0 | 0 |     0      |    0    |      0     |
| 0 | 0 | 1 |     0      |    0    |      0     |
| 0 | 1 | 0 |     0      |    0    |      0     |
| 0 | 1 | 1 |     1      |    1    |      1     |
| 1 | 0 | 0 |     1      |    1    |      1     |
| 1 | 0 | 1 |     1      |    1    |      1     |
| 1 | 1 | 0 |     0      |    0    |      0     |
| 1 | 1 | 1 |     1      |    1    |      1     |

#### Pseudocode:
***IF*** **A** is on and **B** is not ***OR*** **B** and **C** are on; ***THEN*** the led turns on. 



### Hardware schematic
I created the below schematic to implement the **AB'** + **BC** = **F** logic.The pseudocode and truth table above provide a guide for how this schematic should be implemented.

![Schematic Drawing](images/schematic.PNG)
##### Figure 1: The schematic drawing for the logic  **AB'** + **BC** = **F**, created in Xilinx.

### Well-formatted code
All your code will be in the code folder and contain headers, comments, and good coding practices.  Please reference the Lab Notebook requirements page.

### Debugging
The first problem I encountered was being unable to install Xilinx because downloaded a corrupt version from the hard drives in class. It was suggested that we download the files instead from the M drive, and this remedied the problem.

Another issue came in my misunderstanding of how the .gitignore worked. I copied this and the initial readme.md into my repository before I had added my CE1 folder. The CE1 folder and it's contents would not show. I fixed this by removing the .gitignore file from the repo until it was needed for blocking the Xilinx files. 

I also encountered an issue while trying to download the Xilinx files onto the Development Board. I encountered the error in the picture below stating that my PATH variable to TEMP was not working properly. The following image shows the error that was thrown:

![Error](images/error.PNG)
##### Figure 2: Error that was thrown by Xilinx while trying to download code onto the Board.

I don't understand how this occurred yet, but closing and reopening Xilinx and then trying to run Synthesize - XST again was successful.

### Testing methodology or results 

I began by creating the schematic for the desired logic, which can be viewed in the Hardware schematic section above. Next step was to run a simulation to see if the logic was correct. I created Xilinx VHDL testbench and coded all possible combinations of input. Running the simulation provided the following results, consistent with what was expected.

![Simulation results](images/sim.PNG)
##### Figure 3: Shows the results of the simulation are correct.

The simulation output was correct, so the next step was to actually implement the logic onto the FPGA.I used Xilinx to generate a format that could actually be interpreted by the Nexys2 Board. Once I created the .bit file for the board, I connected the board to my computer. I had to go through a short process to configure the board and associate the correct bit file with it, which was successful as can be seen in the picture below.

![Config Success](images/succeeded.PNG)
##### Figure 4: Graphics from the iMPACT window showing the successful configuration and download of code onto the Nexys2 Board.

Now that the code was downloaded onto the board it was operational. The LED turned on for all the correct input combinations, as can be demonstrated in the .gif below.
![Board](images/final.gif)
##### Figure 5: This .gif demonstrates the correct functionality on the board. From left to right, the first three switches are **A**, **B**, and **C**.

### Answers to Lab Questions
We will need to write the test vectors for all possible combinations of switch positions.  
	a.	How many test vectors will this be?  
8 tests for every possible combination of inputs.



Click on the “Design” tab and open up the imported file.  
a.	Make sure you are in “Implementation” mode not “Simulation” mode  
b.	Look at the contents of this file.  What is each line doing?  
Each line links the pins on the board to the correct switches from the schematic.




### Observations and Conclusions
The purpose of this Computer Exercise was to become familiar with the software and development process for this course. I definitely achieved that purpose. I had no experience with any software of this kind, so I learned something new at every step. I understand that this was an extremely basic problem, but I feel that I have a solid base to solve harder problems in the future. I will most likely be using the who process, from creating a truth table to the schematic to running simulations to actually implementing the code.

### Feedback
Number of hours spent on CE1: 8   

Suggestions to improve CE1 in future years: (use blank space below)   
My suggestion would be better explanation and instruction as to what exactly to put into the readme.md. I felt it was very vague and I'm not sure if I've included everything that is required. Or if I have put the correct information in the correct sections. Other than that, I thought the instructions were very clear and easy to follow.




### Documentation
C3C Ryer helped me with the readme.md file. We talked about what exactly the writeup is and what should be included in it. 

Capt Warner sent out an email regarding issues installing Xilinx and his suggestion to download it from the M drive instead of the hard drives from class helped me solve that issue.